package com.gep.smartdoctor.adapters;

import android.app.Activity;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.gep.smartdoctor.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import com.gep.smartdoctor.models.ReviewsModel;
import com.gep.smartdoctor.util.CommonClass;

/**
 * Created by LENOVO on 4/19/2016.
 */
public class ReviewsAdapter extends RecyclerView.Adapter<ReviewsAdapter.ProductHolder> {

    private Activity context;
    private CommonClass common;
    private ArrayList<ReviewsModel> postItems;

    public ReviewsAdapter(Activity con, ArrayList<ReviewsModel> array) {
        context = con;
        postItems = array;
        common = new CommonClass(con);
    }

    @Override
    public ProductHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;

        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_reviews, parent, false);

        return new ProductHolder(view);
    }

    @Override
    public void onBindViewHolder(final ProductHolder holder, final int position) {
        final ReviewsModel categoryModel = postItems.get(position);
        String path = categoryModel.getUser_image();

        holder.textReview.setText(categoryModel.getReviews());
        holder.textUser.setText(categoryModel.getUser_fullname());
        holder.textDate.setText(categoryModel.getOn_date());
        holder.ratingBar1.setRating(Float.valueOf(categoryModel.getRatings()));
        holder.datetime.setText(timeDifference(categoryModel.getOn_date()));

        Float rate = Float.valueOf(categoryModel.getRatings());
        if(rate > 3){
            holder.imageIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.happy_icon));
        }
        if(rate == 3 /*&& rate < 4*/){
            holder.imageIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.sad_icon));
        }
        if(rate >= 0 && rate < 3){
            holder.imageIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.very_sad_icon));
        }
    }


    @Override
    public int getItemCount() {
        return postItems.size();
    }

    class ProductHolder extends RecyclerView.ViewHolder {
        TextView textReview;
        ImageView imageIcon;
        TextView textUser;
        TextView textDate;
        RatingBar ratingBar1;
        TextView datetime;

        public ProductHolder(View itemView) {
            super(itemView);
            imageIcon = (ImageView) itemView.findViewById(R.id.imgIcon);
            textReview = (TextView) itemView.findViewById(R.id.txtReview);
            textUser = (TextView) itemView.findViewById(R.id.txtUser);
            textDate = (TextView) itemView.findViewById(R.id.txtDate);
            ratingBar1 = (RatingBar) itemView.findViewById(R.id.ratingBar1);
            datetime = (TextView) itemView.findViewById(R.id.datetime);
        }
    }


    // count total difference current datetime to given datetime
    public String timeDifference(String posteddate) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar calender = Calendar.getInstance(Locale.getDefault());

        int c_day = calender.get(Calendar.DAY_OF_MONTH);
        int c_month = calender.get(Calendar.MONTH) + 1;
        int c_year = calender.get(Calendar.YEAR);
        int c_hour = calender.get(Calendar.HOUR_OF_DAY) - 8;
        int c_min = calender.get(Calendar.MINUTE);
        int c_sec = calender.get(Calendar.SECOND) + 3;

        String currentdate = c_year + "-" + c_month + "-" + c_day + " " + c_hour + ":" + c_min + ":" + c_sec;

        Date d1 = null;
        Date d2 = null;

        try {
            d1 = format.parse(posteddate);
            d2 = format.parse(currentdate);

            //in milliseconds
        } catch (Exception e) {
            e.printStackTrace();
        }

        long diff = d2.getTime() - d1.getTime();
        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;
        long weeksInMilli = daysInMilli * 7;
        long monthInMilli = daysInMilli * 30;
        long yearInMilli = monthInMilli * 12;

        long elapsedYear = diff / yearInMilli;
        diff = diff % yearInMilli;

        long elapsedMonths = diff / monthInMilli;
        diff = diff % monthInMilli;

        long elapsedWeeks = diff / weeksInMilli;
        diff = diff % weeksInMilli;

        long elapsedDays = diff / daysInMilli;
        diff = diff % daysInMilli;

        long elapsedHours = diff / hoursInMilli;
        diff = diff % hoursInMilli;

        long elapsedMinutes = diff / minutesInMilli;
        diff = diff % minutesInMilli;

        long elapsedSeconds = diff / secondsInMilli;


        if (elapsedYear != 0) {
            return String.format(Locale.ENGLISH, "%d yrs %d days", elapsedYear, elapsedDays);
        } else if (elapsedMonths != 0) {
            return String.format(Locale.ENGLISH, "%d mon %d days", elapsedMonths, elapsedDays);
        } else if (elapsedWeeks != 0) {
            return String.format(Locale.ENGLISH, "%d wk %d days", elapsedWeeks, elapsedDays);
        } else if (elapsedDays == 0 && elapsedHours == 0) {
            return String.format(Locale.ENGLISH, "%d min %d sec", elapsedMinutes, elapsedSeconds);
        } else if (elapsedDays == 0) {
            return String.format(Locale.ENGLISH, "%d hrs %d min", elapsedHours, elapsedMinutes);
        } else if (elapsedDays != 0) {
            return String.format(Locale.ENGLISH, "%d days %d hrs", elapsedDays, elapsedHours);
        }
        return String.format(Locale.ENGLISH, "%d days, %d hrs, %d min, %d sec%n",
                elapsedDays,
                elapsedHours, elapsedMinutes, elapsedSeconds);

    }
}
