package com.gep.smartdoctor.adapters;

import android.app.Activity;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gep.smartdoctor.R;
import com.gep.smartdoctor.models.SlotsModel;
import com.gep.smartdoctor.util.CommonClass;

import java.util.ArrayList;

public class TimeSlotMorningAdapter extends RecyclerView.Adapter<TimeSlotMorningAdapter.ProductHolder> {

    private int limit = 0;
    private ArrayList<SlotsModel> morningTimeSlotArray;
    private CommonClass common;
    private Activity activity;

    public TimeSlotMorningAdapter(Activity activity, ArrayList<SlotsModel> list) {
        this.morningTimeSlotArray = list;
        this.activity = activity;
        this.limit = list.size();
        common = new CommonClass(activity);

    }

    //with limit data
    public TimeSlotMorningAdapter(Activity activity, ArrayList<SlotsModel> list, int limit) {
        this.morningTimeSlotArray = list;
        this.activity = activity;
        this.limit = limit;
        common = new CommonClass(activity);

    }

    @Override
    public ProductHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;

        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_time_slot, parent, false);

        return new ProductHolder(view);
    }

    @Override
    public void onBindViewHolder(final TimeSlotMorningAdapter.ProductHolder holder, final int position) {
        final SlotsModel slotsModel = morningTimeSlotArray.get(position);

        if (slotsModel.getIs_booked()) {
            holder.itemView.setBackground(activity.getResources().getDrawable(R.drawable.timeslot_selected));
        } else {
            holder.itemView.setBackground(activity.getResources().getDrawable(R.drawable.timeslot_unselected));
        }

        holder.timeslot.setText(CommonClass.parseTime(slotsModel.getSlot()));
    }

    @Override
    public int getItemCount() {
        return limit;
    }

    class ProductHolder extends RecyclerView.ViewHolder {
        TextView timeslot;

        public ProductHolder(View itemView) {
            super(itemView);

            timeslot = (TextView) itemView.findViewById(R.id.timeslot);
        }
    }
}
