package com.gep.smartdoctor.adapters;

import android.app.Activity;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.gep.smartdoctor.Config.ConstValue;
import com.gep.smartdoctor.R;
import com.gep.smartdoctor.activities.ActivityClinicList;
import com.gep.smartdoctor.models.ActiveModels;
import com.gep.smartdoctor.models.CategoryModel;
import com.gep.smartdoctor.util.CircleTransform;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class CategoryListAdapter extends RecyclerView.Adapter<CategoryListAdapter.ProductHolder> {

    private ArrayList<CategoryModel> list;
    private Activity activity;

    public CategoryListAdapter(Activity activity, ArrayList<CategoryModel> list) {
        this.list = list;
        this.activity = activity;

    }

    @Override
    public CategoryListAdapter.ProductHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_category_list, parent, false);

        return new CategoryListAdapter.ProductHolder(view);
    }

    @Override
    public void onBindViewHolder(final CategoryListAdapter.ProductHolder holder, final int position) {
        final CategoryModel categoryModel = list.get(position);
        String path = categoryModel.getImage();

        Picasso.with(activity)
                .load(ConstValue.BASE_URL + "/uploads/admin/category/" + path)
                .placeholder(R.drawable.circle_bg_replacer)
                .resize(200, 200)
                .transform(new CircleTransform())
                .into(holder.icon_image);

        holder.lbl_title.setText(categoryModel.getTitle());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActiveModels.CATEGORY_MODEL = list.get(position);
                Intent intent = new Intent(activity, ActivityClinicList.class);
                intent.putExtra("cat_id", ActiveModels.CATEGORY_MODEL.getId());
                activity.startActivity(intent);
            }
        });
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    class ProductHolder extends RecyclerView.ViewHolder {
        ImageView icon_image;
        TextView lbl_title;

        public ProductHolder(View itemView) {
            super(itemView);
            icon_image = (ImageView) itemView.findViewById(R.id.imageView);
            lbl_title = (TextView) itemView.findViewById(R.id.title);
        }
    }


}