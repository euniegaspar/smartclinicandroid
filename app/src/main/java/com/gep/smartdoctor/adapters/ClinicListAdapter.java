package com.gep.smartdoctor.adapters;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.gep.smartdoctor.activities.ActivityClinicList;
import com.gep.smartdoctor.activities.ActivityDetailsClinics;
import com.gep.smartdoctor.activities.CommonActivity;

import com.gep.smartdoctor.R;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.gep.smartdoctor.Config.ConstValue;
import com.gep.smartdoctor.models.ActiveModels;
import com.gep.smartdoctor.models.BusinessModel;
import com.gep.smartdoctor.util.CircleTransform;
import com.gep.smartdoctor.util.CommonClass;
import com.squareup.picasso.Picasso;

/**
 * Created by LENOVO on 4/19/2016.
 */
public class ClinicListAdapter extends RecyclerView.Adapter<ClinicListAdapter.ProductHolder> {

    private Activity activity;
    private CommonClass common;
    private ArrayList<BusinessModel> postItems;
    private List<String> favoriteList = new ArrayList<>();

    public ClinicListAdapter(Activity con, ArrayList<BusinessModel> array) {
        activity = con;
        postItems = array;
        common = new CommonClass(con);
        updatelist();
    }

    public void updatelist() {
        favoriteList.clear();
        favoriteList.addAll(Arrays.asList(CommonActivity.getFavorite(activity).split(",")));
        Log.e(activity.toString(), "favoriteList:" + favoriteList.toString());
    }

    @Override
    public ProductHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_business_list_new, parent, false);

        return new ProductHolder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(final ProductHolder holder, final int position) {
        final BusinessModel categoryModel = postItems.get(position);
        String path = categoryModel.getBus_logo();
        holder.setIsRecyclable(false);

        Picasso.with(activity).load(ConstValue.BASE_URL + "/uploads/business/" + path)
                .placeholder(R.color.colorTextWhite)
                .error(R.color.colorTextWhite)
                .transform(new CircleTransform())
                .into(holder.imgBusiness);

        holder.textName.setText(categoryModel.getBus_title());
        holder.textSubTitle.setText(categoryModel.getBus_google_street());
        categoryModel.setFavorite(favoriteList.contains(categoryModel.getBus_id()));

        if (categoryModel.isFavorite()) {
            holder.iv_favorite.setBackgroundResource(R.mipmap.ic_favorite);
        } else {
            holder.iv_favorite.setBackgroundResource(R.mipmap.ic_unfavorite);
        }

        float rate = Float.parseFloat(categoryModel.getAvg_rating());
        DecimalFormat df = new DecimalFormat("#.#");
        df.setRoundingMode(RoundingMode.CEILING);
        holder.tvRating.setText(df.format(rate));

    }


    @Override
    public int getItemCount() {
        return postItems.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    class ProductHolder extends RecyclerView.ViewHolder {
        ImageView imgBusiness, iv_favorite;
        TextView textName;
        TextView textSubTitle;
        TextView tvRating;

        public ProductHolder(View itemView) {
            super(itemView);
            imgBusiness = (ImageView) itemView.findViewById(R.id.img_business);
            textName = (TextView) itemView.findViewById(R.id.txt_clinicname);
            textSubTitle = (TextView) itemView.findViewById(R.id.txt_clinicdesc);
            iv_favorite = (ImageView) itemView.findViewById(R.id.iv_business_list_favorite);
            tvRating = (TextView) itemView.findViewById(R.id.tx_rate);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getAdapterPosition();
                    ActiveModels.BUSINESS_MODEL = postItems.get(position);
                    Intent intent = new Intent(activity, ActivityDetailsClinics.class);
                    Bundle b = new Bundle();
                    b.putString("header", ((ActivityClinicList) activity).header);
                    intent.putExtras(b);
                    activity.startActivity(intent);
                }
            });

            iv_favorite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    BusinessModel businessModel = postItems.get(position);

                    CommonActivity.setFavorite(activity, businessModel.getBus_id());

                    if (businessModel.isFavorite()) {
                        businessModel.setFavorite(false);
                    } else {
                        businessModel.setFavorite(true);
                    }

                    updatelist();

                    notifyItemChanged(position);
                }
            });

        }
    }


}
