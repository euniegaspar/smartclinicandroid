package com.gep.smartdoctor.adapters;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Paint;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.gep.smartdoctor.R;

import java.util.ArrayList;

import com.gep.smartdoctor.activities.ActivityDetailsClinics;
import com.gep.smartdoctor.fragments.PriceFragment;
import com.gep.smartdoctor.models.ActiveModels;
import com.gep.smartdoctor.models.ServicesModel;

import static android.text.Spanned.SPAN_INCLUSIVE_INCLUSIVE;

public class ServiceChargeAdapter extends RecyclerView.Adapter<ServiceChargeAdapter.ProductHolder> {

    private Activity activity;
    private PriceFragment priceFragment;
    private ArrayList<ServicesModel> postItems;
    int count = 0;
    private int selectedPosition = -1;
    private CheckBox lastCheckedRadio;

    public ServiceChargeAdapter(Activity context, ArrayList<ServicesModel> arraylist, PriceFragment priceFragment) {
        this.activity = context;
        postItems = arraylist;
        this.priceFragment = priceFragment;
    }

    @Override
    public ProductHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_service_charge, parent, false);

        return new ProductHolder(view);
    }

    @Override
    public void onBindViewHolder(final ProductHolder holder, final int position) {
        final ServicesModel categoryModel = postItems.get(position);

        String stitleOff = "";

        if (!categoryModel.getService_discount().equalsIgnoreCase("0")) {
            stitleOff = "(" + categoryModel.getService_discount() + "% off)";
            holder.txtPriceAct.setText("P. " + String.format("%.2f", Double.parseDouble(categoryModel.getService_price())));
            holder.txtPriceAct.setTextColor(activity.getResources().getColor(R.color.colorRed));
            holder.txtPriceAct.setPaintFlags(holder.txtPriceAct.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            holder.txtPriceAct.setVisibility(View.VISIBLE);
            Double getAmt = Double.parseDouble(categoryModel.getService_price());
            Double detDisc = Double.parseDouble(categoryModel.getService_discount());
            Double finalAmt = getAmt - (detDisc * getAmt / 100);
            holder.txtPrice.setText("P. " + String.format("%.2f", finalAmt));
        } else {
            stitleOff = "";
            holder.txtPriceAct.setVisibility(View.GONE);
            holder.txtPrice.setText("P. " + String.format("%.2f", Double.parseDouble(categoryModel.getService_price())));
        }
        holder.itemView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.chbox.performClick();
            }
        });

        //needed to check for duplicates
        if(position == 0 && holder.chbox.isChecked())
        {
            lastCheckedRadio = holder.chbox;
            selectedPosition = 0;
        }


        holder.chbox.setOnCheckedChangeListener(null);
        holder.chbox.setChecked(categoryModel.isChecked());

        holder.chbox.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                int clickedPos = holder.getAdapterPosition();
                if(selectedPosition != position) {
                    if(lastCheckedRadio != null) {
                        lastCheckedRadio.setChecked(false);
                    }

                    lastCheckedRadio = holder.chbox;
                    lastCheckedRadio.setChecked(true);
                    selectedPosition = clickedPos;

                } else{
                    lastCheckedRadio = holder.chbox;
                }

                String[] timesplit = categoryModel.getBusiness_approxtime().split(":");
                Double businessfee = Double.parseDouble(ActiveModels.BUSINESS_MODEL.getBus_fee());
                Double amount = Double.parseDouble(categoryModel.getService_price());
                Double discountAmt = Double.parseDouble(categoryModel.getService_discount()) * amount / 100;
                Double finalAmount = (businessfee + amount) - discountAmt;


                ((ActivityDetailsClinics) activity).updatePrice(categoryModel.getService_title(),
                        timesplit,
                        ActiveModels.BUSINESS_MODEL.getBus_fee(),
                        String.valueOf(amount),
                        String.valueOf(discountAmt),
                        String.valueOf(finalAmount));
                priceFragment.checkBookBtn(holder.chbox.isChecked(), categoryModel.getId());
                priceFragment.getServiceId = Integer.parseInt(categoryModel.getId());

            }
        });

        if (selectedPosition == position){
            holder.chbox.setChecked(true);
        } else {
            holder.chbox.setChecked(false);
        }

        //span color and size
        int textSize1 = activity.getResources().getDimensionPixelSize(R.dimen.text_normal);
        int textSize2 = activity.getResources().getDimensionPixelSize(R.dimen.text_tiny);

        String text1 = categoryModel.getService_title();
        String text2 = stitleOff;

        SpannableString span1 = new SpannableString(text1);
        span1.setSpan(new AbsoluteSizeSpan(textSize1), 0, text1.length(), SPAN_INCLUSIVE_INCLUSIVE);
        span1.setSpan(new ForegroundColorSpan(activity.getResources().getColor(R.color.colorTextDarkGray)), 0, text1.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        SpannableString span2 = new SpannableString(text2);
        span2.setSpan(new AbsoluteSizeSpan(textSize2), 0, text2.length(), SPAN_INCLUSIVE_INCLUSIVE);
        span2.setSpan(new ForegroundColorSpan(Color.RED), 0, text2.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        CharSequence finalText = TextUtils.concat(span1, " ", span2);
        holder.txtService.setText(finalText);
    }


    @Override
    public int getItemCount() {
        return postItems.size();
    }

    class ProductHolder extends RecyclerView.ViewHolder {
        TextView txtService;
        TextView txtPrice;
        TextView txtPriceAct;
        TextView txtPriceOff;
        CheckBox chbox;

        public ProductHolder(View itemView) {
            super(itemView);
            txtService = (TextView) itemView.findViewById(R.id.tx_service);
            txtPrice = (TextView) itemView.findViewById(R.id.textView1);
            txtPriceAct = (TextView) itemView.findViewById(R.id.textView);
            chbox = (CheckBox) itemView.findViewById(R.id.checkBox1);
        }
    }


    public interface OnDataChangeListener {
        public void onDataChanged();
    }

    OnDataChangeListener mOnDataChangeListener;

    public void setOnDataChangeListener(OnDataChangeListener onDataChangeListener) {
        mOnDataChangeListener = onDataChangeListener;
    }

}

