package com.gep.smartdoctor.adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.gep.smartdoctor.Config.ApiParams;
import com.gep.smartdoctor.R;
import com.gep.smartdoctor.activities.ActivityMain;
import com.gep.smartdoctor.fragments.FragmentAppointment;
import com.gep.smartdoctor.fragments.FragmentSchedule;
import com.gep.smartdoctor.models.AppointmentModel;
import com.gep.smartdoctor.models.ScheduleItemModel;
import com.gep.smartdoctor.util.CircleTransform;
import com.gep.smartdoctor.util.CommonClass;
import com.gep.smartdoctor.util.VJsonRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;

public class MyApptSchedAdapter extends RecyclerSwipeAdapter<MyApptSchedAdapter.ProductHolder> {

    ArrayList<AppointmentModel> arrayList;
    Activity activity;
    CommonClass common;

    public MyApptSchedAdapter(Activity activity, ArrayList<AppointmentModel> arrayList) {
        this.arrayList = arrayList;
        this.activity = activity;
        common = new CommonClass(activity);
    }

    @Override
    public MyApptSchedAdapter.ProductHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_appointment, parent, false);

        return new ProductHolder(view);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(final MyApptSchedAdapter.ProductHolder holder, final int position) {
        final AppointmentModel appointment = arrayList.get(position);
        holder.setIsRecyclable(false);
        String imagepath = appointment.getImage()/*.replace(ConstValue.BASE_URL + "/uploads/admin/category/", "")*/;

        Picasso.with(activity)
                .load(/*ConstValue.BASE_URL + "/uploads/admin/category/" +*/ imagepath)
                .placeholder(R.drawable.circle_bg_replacer)
                .resize(200, 200)
                .transform(new CircleTransform())
                .into(holder.imgCategory);


        holder.swipeLayout.setSwipeEnabled(true);
        holder.dividerView.setVisibility(View.VISIBLE);
        holder.titleSchedLabel.setVisibility(View.GONE);
        holder.tvTimeMainLabel.setVisibility(View.VISIBLE);
        holder.statusLabel.setVisibility(View.GONE);

        holder.tvTimeMainLabel.setText(parsePMAM(appointment.getStart_time()));
        holder.tvDoctorLabel.setText(appointment.getDoct_speciality() + " - " + appointment.getDoct_name());
        holder.tvHospitalLabel.setText(appointment.getBus_title());
        holder.tvTimeLabel.setText(parsePMAM(appointment.getStart_time()) + " - " +
                addTimeBooking(appointment.getStart_time(), appointment.getBus_con_time())); //for edit
        holder.tvSheduleTitle.setText(CommonClass.getDateText(appointment.getAppointment_date()));

        holder.swipeLayout.setShowMode(SwipeLayout.ShowMode.PullOut);
        holder.iv_Delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mItemManger.removeShownLayouts(holder.swipeLayout);
                mItemManger.closeAllItems();
                holder.swipeLayout.close();
                openReasonDialog(view, position, appointment.getId());
            }
        });

        mItemManger.bindView(holder.itemView, position);

    }

    @Override
    public int getItemCount() {
        if(arrayList.size() == 0){
            return  0;
        } else {
            return arrayList.size();
        }
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }


    class ProductHolder extends RecyclerView.ViewHolder {

        LinearLayout mainItemLayout;
        SwipeLayout swipeLayout;
        View dividerView;
        LinearLayout titleSchedLabel, statusLabel;
        ImageView iv_Delete, imgCategory;
        TextView tvTimeMainLabel, tvDoctorLabel, tvHospitalLabel, tvTimeLabel, tvSheduleTitle,
                tvPayment, tvCancelStatus;

        public ProductHolder(View view) {
            super(view);

            mainItemLayout = (LinearLayout) view.findViewById(R.id.appt_layout_main);
            swipeLayout = (SwipeLayout) view.findViewById(R.id.swipe);
            iv_Delete = (ImageView) view.findViewById(R.id.iv_appointment_delete);
            dividerView = (View) view.findViewById(R.id.divider_view);
            statusLabel = (LinearLayout) view.findViewById(R.id.status_view);
            titleSchedLabel = (LinearLayout) view.findViewById(R.id.ll_schedule);
            imgCategory = (ImageView) view.findViewById(R.id.img_category);
            tvTimeMainLabel = (TextView) view.findViewById(R.id.tv_time_main_label);
            tvDoctorLabel = (TextView) view.findViewById(R.id.tv_doctor_label);
            tvHospitalLabel = (TextView) view.findViewById(R.id.tv_hospital_label);
            tvTimeLabel = (TextView) view.findViewById(R.id.tv_timeslot_label);
            tvSheduleTitle = (TextView) view.findViewById(R.id.tv_schedule_label);
            tvPayment = (TextView) view.findViewById(R.id.txt_payment);
            tvCancelStatus = (TextView) view.findViewById(R.id.txt_cancel_status);
        }
    }

    private String addTimeBooking(String starttime, String contime){
        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
        SimpleDateFormat newdf = new SimpleDateFormat("hh:mm aa");
        Date d = null;
        try {
            d = df.parse(starttime);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String[] splitcontime = contime.split(":");
        int hours = Integer.parseInt(splitcontime[0]);
        int minutes = Integer.parseInt(splitcontime[1]);
        int seconds = Integer.parseInt(splitcontime[2]) ;

        Calendar cal = Calendar.getInstance();
        cal.setTime(d);
        cal.add(Calendar.SECOND, seconds);
        cal.add(Calendar.MINUTE, minutes);
        cal.add(Calendar.HOUR_OF_DAY, hours);
        String newTime = df.format(cal.getTime());

        Date d2 = null;
        try {
            d2 = df.parse(newTime);
            newTime = newdf.format(d2);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return newTime;
    }

    private String parsePMAM (String time) {
        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
        SimpleDateFormat newdf = new SimpleDateFormat("hh:mm aa");
        Date d = null;
        try {
            d = df.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        Calendar cal = Calendar.getInstance();
        cal.setTime(d);
        String newTime = df.format(cal.getTime());

        Date d2 = null;
        try {
            d2 = df.parse(newTime);
            newTime = newdf.format(d2);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return newTime;
    }

    private String parseDateToddMM(String time) {
        String inputPattern = "yyyy-MM-dd";
        String outputPattern = "dd - MMM";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern, Locale.getDefault());
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern, Locale.getDefault());

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    // show review dialog
    public void openReasonDialog(final View view, final int position, final String mapId) {
        final Dialog dialog = new Dialog(view.getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_cancel_appointment);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        ImageView btnClose = (ImageView) dialog.findViewById(R.id.btn_close);
        final Button btnSubmit = (Button) dialog.findViewById(R.id.btn_submit);
        final EditText etCancellationReason = (EditText) dialog.findViewById(R.id.editText1);
        etCancellationReason.setFilters(CommonClass.getFilterWithoutSpecialChars(250));
        btnSubmit.setEnabled(false);
        etCancellationReason.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                if(!etCancellationReason.getText().toString().trim().isEmpty()) {
                    if(etCancellationReason.getText().length() > 0) {
                        btnSubmit.setEnabled(true);
                        btnSubmit.setBackground(view.getResources().getDrawable(R.drawable.rounded_button));
                    } else{
                        btnSubmit.setEnabled(false);
                        btnSubmit.setBackground(view.getResources().getDrawable(R.drawable.rounded_btn_gray));
                    }
                } else{
                    btnSubmit.setEnabled(false);
                    btnSubmit.setBackground(view.getResources().getDrawable(R.drawable.rounded_btn_gray));
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }
        });

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(TextUtils.isEmpty(etCancellationReason.getText().toString())){
                    etCancellationReason.setError("Please specify reason for cancellation");
                } else{
                    dialog.dismiss();
                    deleteConfirm(v, position, etCancellationReason.getText().toString(), mapId);
                }
            }
        });

        dialog.show();
    }

    public void deleteConfirm(final View view, final int position, final String cancelreason, final String mapId) {
        final Dialog dialog = new Dialog(view.getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_confirm_cancel);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView btnConfirm = (TextView) dialog.findViewById(R.id.btn_confirm);
        TextView btnCancel = (TextView) dialog.findViewById(R.id.btn_cancel);

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteRow(view, position, cancelreason, mapId);
                dialog.dismiss();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });

        dialog.show();
    }


    //UPDATE THIS
    private void deleteRow(View view, final int position, String cancelreason, String mapId) {
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", common.get_user_id());
        params.put("app_id", mapId);
//            params.put("app_id", map.getId());
        params.put("cancel_reason", cancelreason);

        VJsonRequest vJsonRequest = new VJsonRequest(activity, ApiParams.CANCELAPPOINTMENTS_URL, params,
                new VJsonRequest.VJsonResponce() {
                    @Override
                    public void VResponce(String responce) {

                        arrayList.remove(position);
                        notifyItemRemoved(position);
                        notifyItemRangeChanged(position, arrayList.size());
                    }

                    @Override
                    public void VError(String responce) {
                        //common.setToastMessage(responce);
                        common.setToastMessage(responce);
                    }
                });


        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
//                Intent intent = new Intent(activity, ActivityMain.class);
//                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                activity.startActivity(intent);
//                activity.finish();
//                activity.overridePendingTransition(0,0);

                FragmentSchedule.fetchData(activity, common.get_user_id());
                notifyDataSetChanged();
                ActivityMain.tabPosition = 0;
            }
        }, 3000);
    }
}