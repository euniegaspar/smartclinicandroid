package com.gep.smartdoctor.adapters;

import android.app.Activity;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gep.smartdoctor.R;
import com.gep.smartdoctor.activities.ActivityClinicList;
import com.gep.smartdoctor.models.ActiveModels;
import com.gep.smartdoctor.models.CategoryModel;

import java.util.ArrayList;

public class MoreListSearchAdapter extends RecyclerView.Adapter<MoreListSearchAdapter.ProductHolder> {

    private ArrayList<CategoryModel> list;
    private Activity activity;

    public MoreListSearchAdapter(Activity activity, ArrayList<CategoryModel> list) {
        this.list = list;
        this.activity = activity;

    }

    @Override
    public ProductHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_locality, parent, false);

        return new ProductHolder(view);
    }

    @Override
    public void onBindViewHolder(final ProductHolder holder, final int position) {
        final CategoryModel categoryModel = list.get(position);
        holder.setIsRecyclable(false);

        holder.lbl_title.setText(categoryModel.getTitle());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActiveModels.CATEGORY_MODEL = list.get(position);
                Intent intent = new Intent(activity, ActivityClinicList.class);
                intent.putExtra("cat_id", ActiveModels.CATEGORY_MODEL.getId());
                activity.startActivity(intent);
            }
        });
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    class ProductHolder extends RecyclerView.ViewHolder {
        TextView lbl_title;

        public ProductHolder(View itemView) {
            super(itemView);
            lbl_title = (TextView) itemView.findViewById(R.id.title);
        }
    }


}