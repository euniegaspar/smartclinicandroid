package com.gep.smartdoctor.adapters;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.gep.smartdoctor.Config.ConstValue;
import com.gep.smartdoctor.R;
import com.gep.smartdoctor.activities.ActivityMain;
import com.gep.smartdoctor.activities.SearchActivity;
import com.gep.smartdoctor.models.AmbulanceModel;
import com.gep.smartdoctor.models.LocalityModel;

import java.util.ArrayList;

public class AmbulanceAdapter extends RecyclerView.Adapter<AmbulanceAdapter.AmbulanceHolder> {

    private ArrayList<AmbulanceModel> list;
    private Activity activity;

    //temporary
    private int tabIcons[] = {R.drawable.ambulance1, R.drawable.ambulance2, R.drawable.ambulance3, R.drawable.ambulance4, R.drawable.ambulance5};


    public AmbulanceAdapter(Activity activity, ArrayList<AmbulanceModel> list) {
        this.list = list;
        this.activity = activity;
    }

    @Override
    public AmbulanceAdapter.AmbulanceHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_ambulance, parent, false);

        return new AmbulanceHolder(view);
    }

    @Override
    public void onBindViewHolder(final AmbulanceAdapter.AmbulanceHolder holder, final int position) {
        AmbulanceModel ambulanceModel = list.get(position);
        holder.setIsRecyclable(false);

        holder.tvTitle.setText(ambulanceModel.getTitle());
        holder.tvAddress.setText(activity.getResources().getString(R.string.address) + " " + ambulanceModel.getAddress());
        holder.tvPhone.setText(activity.getResources().getString(R.string.phone) + " " + ambulanceModel.getPhone());

        RequestOptions requestOptions = new RequestOptions();
        requestOptions = requestOptions.transforms(new CenterCrop(), new RoundedCorners(10));
        Glide.with(activity)
                .load(tabIcons[position])
                .apply(requestOptions)
                .into(holder.imgStation);


        holder.tvPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class AmbulanceHolder extends RecyclerView.ViewHolder {
        TextView tvTitle, tvAddress, tvPhone;
        ImageView imgStation;

        public AmbulanceHolder(View itemView) {
            super(itemView);
            tvTitle = (TextView) itemView.findViewById(R.id.tv_title);
            tvAddress = (TextView) itemView.findViewById(R.id.tv_address);
            tvPhone = (TextView) itemView.findViewById(R.id.tv_phone);
            imgStation = (ImageView) itemView.findViewById(R.id.img_station);
        }
    }

    private void callAmbulance(String number){
        number = number.replace(activity.getResources().getString(R.string.phone), "")
                .trim();

        if (Build.VERSION.SDK_INT > 22) {
            if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CALL_PHONE}, 101);
                return;
            }
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:" + number));
            activity.startActivity(callIntent);
        } else {

            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:" + number));
            activity.startActivity(callIntent);
        }

        ActivityMain.tabPosition = 1;
    }
}
