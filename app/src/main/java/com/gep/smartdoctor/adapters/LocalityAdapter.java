package com.gep.smartdoctor.adapters;

import android.app.Activity;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gep.smartdoctor.R;

import java.util.ArrayList;

import com.gep.smartdoctor.activities.SearchActivity;
import com.gep.smartdoctor.models.LocalityModel;


public class LocalityAdapter extends RecyclerView.Adapter<LocalityAdapter.ProductHolder> {

    private ArrayList<LocalityModel> list;
    private Activity activity;

    public LocalityAdapter(Activity activity, ArrayList<LocalityModel> list) {
        this.list = list;
        this.activity = activity;
    }

    @Override
    public ProductHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_locality, parent, false);

        return new ProductHolder(view);
    }

    @Override
    public void onBindViewHolder(final ProductHolder holder, final int position) {
        final LocalityModel categoryModel = list.get(position);
        holder.setIsRecyclable(false);

        holder.lbl_title.setText(categoryModel.getLocality() + "," + categoryModel.getCity_name() + "," + categoryModel.getCountry_name());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((SearchActivity) activity).etLocality.setText(categoryModel.getLocality() + "," + categoryModel.getCity_name() + "," + categoryModel.getCountry_name());
                ((SearchActivity) activity).localityRecycler.setVisibility(View.GONE);

                ((SearchActivity) activity).locality = categoryModel.getLocality();
                ((SearchActivity) activity).latitude = categoryModel.getLocality_lat();
                ((SearchActivity) activity).longitude = categoryModel.getLocality_lon();
                ((SearchActivity) activity).locality_id = categoryModel.getLocality_id();
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ProductHolder extends RecyclerView.ViewHolder {
        TextView lbl_title;

        public ProductHolder(View itemView) {
            super(itemView);
            lbl_title = (TextView) itemView.findViewById(R.id.title);
        }
    }


}
