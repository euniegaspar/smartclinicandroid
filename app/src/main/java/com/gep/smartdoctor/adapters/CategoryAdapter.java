package com.gep.smartdoctor.adapters;

import android.app.Activity;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gep.smartdoctor.activities.ActivityCategoryMoreList;
import com.gep.smartdoctor.activities.ActivityClinicList;

import com.gep.smartdoctor.R;
import com.gep.smartdoctor.util.CircleTransform;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import com.gep.smartdoctor.Config.ConstValue;
import com.gep.smartdoctor.models.ActiveModels;
import com.gep.smartdoctor.models.CategoryModel;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ProductHolder> {

    private ArrayList<CategoryModel> list;
    private Activity activity;

    public CategoryAdapter(Activity activity, ArrayList<CategoryModel> list) {
        this.list = list;
        this.activity = activity;
    }

    @Override
    public ProductHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_category, parent, false);

        return new ProductHolder(view);
    }

    @Override
    public void onBindViewHolder(final ProductHolder holder, final int position) {
        final CategoryModel categoryModel = list.get(position);
        String path = categoryModel.getImage();

        if(position < 8){
            Picasso.with(activity)
                    .load(ConstValue.BASE_URL + "/uploads/admin/category/" + path)
                    .placeholder(R.drawable.circle_bg_replacer)
                    .resize(200, 200)
                    .transform(new CircleTransform())
                    .into(holder.icon_image);
            holder.circle.setBackgroundDrawable(activity.getResources().getDrawable(R.drawable.circle_bg_replacer));

            holder.lbl_title.setText(categoryModel.getTitle());

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ActiveModels.CATEGORY_MODEL = list.get(position);
                    Intent intent = new Intent(activity, ActivityClinicList.class);
                    intent.putExtra("cat_id", ActiveModels.CATEGORY_MODEL.getId());
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    activity.startActivity(intent);
                }
            });
        } else{
            Picasso.with(activity)
                    .load(R.drawable.ellipsis)
                    .resize(200, 200)
                    .into(holder.icon_image);
            holder.circle.setBackgroundColor(activity.getResources().getColor(R.color.colorTextWhite));

            holder.lbl_title.setText("More");
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(activity.getApplication(), ActivityCategoryMoreList.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    activity.getApplicationContext().startActivity(intent);
                }
            });
        }


    }


    @Override
    public int getItemCount() {
        if(list.size() > 9){
            return 9;
        }
        else
        {
            return list.size();
        }
    }

    class ProductHolder extends RecyclerView.ViewHolder {
        LinearLayout circle;
        ImageView icon_image;
        TextView lbl_title;

        public ProductHolder(View itemView) {
            super(itemView);
            circle = (LinearLayout) itemView.findViewById(R.id.layout);
            icon_image = (ImageView) itemView.findViewById(R.id.imageView);
            lbl_title = (TextView) itemView.findViewById(R.id.title);
        }
    }


}
