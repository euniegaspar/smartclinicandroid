package com.gep.smartdoctor.adapters;

import android.app.Activity;

import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.gep.smartdoctor.Config.ConstValue;
import com.gep.smartdoctor.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import com.gep.smartdoctor.models.AppointmentModel;
import com.gep.smartdoctor.util.CircleTransform;
import com.gep.smartdoctor.util.CommonClass;
import com.squareup.picasso.Picasso;

public class MyAppointmentAdapter extends RecyclerSwipeAdapter<MyAppointmentAdapter.ProductHolder> {

    ArrayList<AppointmentModel> arrayList;
    Activity activity;
    CommonClass common;

    public MyAppointmentAdapter(Activity activity, ArrayList<AppointmentModel> arrayList) {
        this.arrayList = arrayList;
        this.activity = activity;
        common = new CommonClass(activity);
    }

    @Override
    public ProductHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;

        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_appointment, parent, false);

        return new ProductHolder(view);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(final ProductHolder holder, final int position) {
        final AppointmentModel appointment = arrayList.get(position);
        holder.setIsRecyclable(false);
        String imagepath = appointment.getImage()/*.replace(ConstValue.BASE_URL + "/uploads/admin/category/", "")*/;

        Picasso.with(activity)
                .load(imagepath)
                .placeholder(R.drawable.circle_bg_replacer)
                .resize(200, 200)
                .transform(new CircleTransform())
                .into(holder.imgCategory);

        holder.swipeLayout.setSwipeEnabled(false);
        holder.dividerView.setVisibility(View.GONE);
        holder.titleSchedLabel.setVisibility(View.VISIBLE);
        holder.tvTimeMainLabel.setVisibility(View.GONE);
        holder.statusLabel.setVisibility(View.VISIBLE);

        //Labels
        holder.tvTimeMainLabel.setText(parsePMAM(appointment.getStart_time()));
        holder.tvDoctorLabel.setText(appointment.getDoct_speciality() + " - " + appointment.getDoct_name());
        holder.tvHospitalLabel.setText(appointment.getBus_title());
        holder.tvTimeLabel.setText(parsePMAM(appointment.getStart_time()) + " - " +
                addTimeBooking(appointment.getStart_time(), appointment.getBus_con_time())); //for edit
        holder.tvSheduleTitle.setText(CommonClass.getDateText(appointment.getAppointment_date()));

        if (appointment.getStatus() != null && !appointment.getStatus().equalsIgnoreCase("0")) {
            holder.tvPayment.setText(R.string.paid);
            holder.tvPayment.setBackground(activity.getResources().getDrawable(R.drawable.paid));
            holder.tvPayment.setTextColor(activity.getResources().getColor(R.color.colorTextWhite));
            holder.tvPayment.setPadding(10, 0, 10, 0);
        } else {
            holder.tvPayment.setText(R.string.unpaid);
            holder.tvPayment.setBackground(activity.getResources().getDrawable(R.drawable.unpaid));
            holder.tvPayment.setTextColor(activity.getResources().getColor(R.color.colorTextWhite));
            holder.tvPayment.setPadding(2, 0, 2, 0);
        }

        //check cancelled status
        if(appointment.getCancel_status() != null && !appointment.getCancel_status().equalsIgnoreCase("1")){
            holder.tvCancelStatus.setVisibility(View.GONE);
        } else{
            holder.tvCancelStatus.setVisibility(View.VISIBLE);
            holder.tvCancelStatus.setBackground(activity.getResources().getDrawable(R.drawable.unpaid));
            holder.tvPayment.setVisibility(View.GONE);
        }

        //adjusts margin
        holder.statusLabel.measure(0, 0);
        int width = holder.statusLabel.getMeasuredWidth() + 5;
        int height = holder.statusLabel.getMeasuredHeight();
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        params.setMargins(0,0, width,0);
        holder.tvSheduleTitle.setLayoutParams(params);
        holder.tvTimeLabel.setLayoutParams(params);

        mItemManger.bindView(holder.itemView, position);

        holder.setIsRecyclable(false);

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }


    class ProductHolder extends RecyclerView.ViewHolder {

        LinearLayout mainItemLayout;
        SwipeLayout swipeLayout;
        View dividerView;
        LinearLayout titleSchedLabel, statusLabel;
        ImageView iv_Delete, imgCategory;
        TextView tvTimeMainLabel, tvDoctorLabel, tvHospitalLabel, tvTimeLabel, tvSheduleTitle,
                    tvPayment, tvCancelStatus;

        public ProductHolder(View view) {
            super(view);

            mainItemLayout = (LinearLayout) view.findViewById(R.id.appt_layout_main);
            swipeLayout = (SwipeLayout) view.findViewById(R.id.swipe);
            iv_Delete = (ImageView) itemView.findViewById(R.id.iv_appointment_delete);
            dividerView = (View) view.findViewById(R.id.divider_view);
            statusLabel = (LinearLayout) view.findViewById(R.id.status_view);
            titleSchedLabel = (LinearLayout) view.findViewById(R.id.ll_schedule);
            imgCategory = (ImageView) view.findViewById(R.id.img_category);
            tvTimeMainLabel = (TextView) view.findViewById(R.id.tv_time_main_label);
            tvDoctorLabel = (TextView) view.findViewById(R.id.tv_doctor_label);
            tvHospitalLabel = (TextView) view.findViewById(R.id.tv_hospital_label);
            tvTimeLabel = (TextView) view.findViewById(R.id.tv_timeslot_label);
            tvSheduleTitle = (TextView) view.findViewById(R.id.tv_schedule_label);
            tvPayment = (TextView) view.findViewById(R.id.txt_payment);
            tvCancelStatus = (TextView) view.findViewById(R.id.txt_cancel_status);
        }
    }

    private String addTimeBooking(String starttime, String contime){
        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
        SimpleDateFormat newdf = new SimpleDateFormat("hh:mm aa");
        Date d = null;
        try {
            d = df.parse(starttime);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String[] splitcontime = contime.split(":");
        int hours = Integer.parseInt(splitcontime[0]);
        int minutes = Integer.parseInt(splitcontime[1]);
        int seconds = Integer.parseInt(splitcontime[2]) ;

        Calendar cal = Calendar.getInstance();
        cal.setTime(d);
        cal.add(Calendar.SECOND, seconds);
        cal.add(Calendar.MINUTE, minutes);
        cal.add(Calendar.HOUR_OF_DAY, hours);
        String newTime = df.format(cal.getTime());

        Date d2 = null;
        try {
            d2 = df.parse(newTime);
            newTime = newdf.format(d2);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return newTime;
    }

    private String parsePMAM (String time) {
        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
        SimpleDateFormat newdf = new SimpleDateFormat("hh:mm aa");
        Date d = null;
        try {
            d = df.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar cal = Calendar.getInstance();
        cal.setTime(d);
        String newTime = df.format(cal.getTime());

        Date d2 = null;
        try {
            d2 = df.parse(newTime);
            newTime = newdf.format(d2);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return newTime;
    }
}