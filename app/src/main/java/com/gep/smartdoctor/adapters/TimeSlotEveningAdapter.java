package com.gep.smartdoctor.adapters;

import android.app.Activity;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gep.smartdoctor.R;
import com.gep.smartdoctor.models.SlotsModel;
import com.gep.smartdoctor.util.CommonClass;

import java.util.ArrayList;

public class TimeSlotEveningAdapter extends RecyclerView.Adapter<TimeSlotEveningAdapter.ProductHolder> {

    private int limit = 0;
    private ArrayList<SlotsModel> eveningTimeSlotArray;
    private CommonClass common;
    private Activity activity;

    public TimeSlotEveningAdapter(Activity activity, ArrayList<SlotsModel> list) {
        this.eveningTimeSlotArray = list;
        this.activity = activity;
        this.limit = list.size();
        common = new CommonClass(activity);

    }

    public TimeSlotEveningAdapter(Activity activity, ArrayList<SlotsModel> list, int limit) {
        this.eveningTimeSlotArray = list;
        this.activity = activity;
        this.limit = limit;
        common = new CommonClass(activity);

    }

    @Override
    public TimeSlotEveningAdapter.ProductHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;

        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_time_slot, parent, false);

        return new ProductHolder(view);
    }

    @Override
    public void onBindViewHolder(final TimeSlotEveningAdapter.ProductHolder holder, final int position) {
        final SlotsModel slotsModel = eveningTimeSlotArray.get(position);

        if (slotsModel.getIs_booked()) {
            holder.itemView.setBackground(activity.getResources().getDrawable(R.drawable.timeslot_selected));
        } else {
            holder.itemView.setBackground(activity.getResources().getDrawable(R.drawable.timeslot_unselected));
        }

        holder.timeslot.setText(CommonClass.parseTime(slotsModel.getSlot()));

    }

    @Override
    public int getItemCount() {
        return limit;
    }

    class ProductHolder extends RecyclerView.ViewHolder {
        TextView timeslot;

        public ProductHolder(View itemView) {
            super(itemView);

            timeslot = (TextView) itemView.findViewById(R.id.timeslot);
        }
    }
}