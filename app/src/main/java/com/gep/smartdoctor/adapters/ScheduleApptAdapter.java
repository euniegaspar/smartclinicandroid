package com.gep.smartdoctor.adapters;

import android.app.Activity;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gep.smartdoctor.R;
import com.gep.smartdoctor.fragments.FragmentAppointment;
import com.gep.smartdoctor.models.AppointmentModel;
import com.gep.smartdoctor.models.ScheduleItemModel;
import com.gep.smartdoctor.util.CommonClass;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class ScheduleApptAdapter extends RecyclerView.Adapter<ScheduleApptAdapter.SchedApptHolder>{

    private RecyclerView.RecycledViewPool viewPool = new RecyclerView.RecycledViewPool();
    private MyApptSchedAdapter myAppointmentAdapter;
    private ArrayList<AppointmentModel> appointmentModelArrayList;
    private ArrayList<ScheduleItemModel> list;
    private Activity activity;
    private String responseString;


    ArrayList<String> idList = new ArrayList<String>();
    ArrayList<String> userIdList = new ArrayList<String>();
    ArrayList<String> apptDateList = new ArrayList<String>();
    ArrayList<String> starttimeList = new ArrayList<String>();
    ArrayList<String> timetokenList = new ArrayList<String>();
    ArrayList<String> statusList = new ArrayList<String>();
    ArrayList<String> appnameList = new ArrayList<String>();
    ArrayList<String> appemailList = new ArrayList<String>();
    ArrayList<String> createdatList = new ArrayList<String>();
    ArrayList<String> contimeList = new ArrayList<String>();
    ArrayList<String> busTitleList = new ArrayList<String>();
    ArrayList<String> busSlugList = new ArrayList<String>();
    ArrayList<String> doctNameList = new ArrayList<String>();
    ArrayList<String> doctSpecialtyList = new ArrayList<String>();
    ArrayList<String> paymentRefList = new ArrayList<String>();
    ArrayList<String> imageList = new ArrayList<String>();
    ArrayList<String> cancelStatusList = new ArrayList<String>();


    public ScheduleApptAdapter(Activity activity, ArrayList<ScheduleItemModel> list/*, ArrayList<AppointmentModel> appointmentModelArrayList*//*, String responseString, ArrayList<AppointmentModel> appointmentModelArrayList*/) {
        this.list = list;
        this.activity = activity;
        this.appointmentModelArrayList = new ArrayList<>();
    }

    @Override
    public ScheduleApptAdapter.SchedApptHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_schedule_item, parent, false);

        return new ScheduleApptAdapter.SchedApptHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SchedApptHolder holder, int position) {
        holder.setIsRecyclable(false);
        final ScheduleItemModel scheduleItemModel = list.get(position);

        appointmentModelArrayList.clear();
        idList.clear();
        userIdList.clear();
        apptDateList.clear();
        starttimeList.clear();
        timetokenList.clear();
        statusList.clear();
        appnameList.clear();
        appemailList.clear();
        createdatList.clear();
        contimeList.clear();
        busTitleList.clear();
        busSlugList.clear();
        doctNameList.clear();
        doctSpecialtyList.clear();
        paymentRefList.clear();
        imageList.clear();
        cancelStatusList.clear();

        String datetitle = scheduleItemModel.getScheduleItemTitle().replace("\"", "");
        holder.tvScheddate.setText(CommonClass.getDateText(datetitle));


        responseString = scheduleItemModel.getResponseString();
        Gson gson = new Gson();
        Type type = new TypeToken<List<AppointmentModel>>(){}.getType();
        List<AppointmentModel> initialAppt = gson.fromJson(responseString, type);

        for (AppointmentModel apptModel : initialAppt){
            if(apptModel.getStatus().equals("0")){
                if(apptModel.getCancel_status().equals("0")) {
                    idList.add(apptModel.getId());
                    userIdList.add(apptModel.getUser_id());
                    apptDateList.add(apptModel.getAppointment_date());
                    starttimeList.add(apptModel.getStart_time());
                    timetokenList.add(apptModel.getTime_token());
                    statusList.add(apptModel.getStatus());
                    appnameList.add(apptModel.getApp_name());
                    appemailList.add(apptModel.getApp_email());
                    createdatList.add(apptModel.getCreated_at());
                    contimeList.add(apptModel.getBus_con_time());
                    busTitleList.add(apptModel.getBus_title());
                    busSlugList.add(apptModel.getBus_slug());
                    doctNameList.add(apptModel.getDoct_name());
                    doctSpecialtyList.add(apptModel.getDoct_speciality());
                    paymentRefList.add(apptModel.getPayment_ref());
                    imageList.add(apptModel.getImage());
                    cancelStatusList.add(apptModel.getCancel_status());



                    FragmentAppointment.calendarEvents.add(stringToCalendar(apptModel.getAppointment_date()));
                }
            }
        }


        for(int i = 0 ; i < apptDateList.size() ; i++) {
            if(apptDateList.get(i).equals(datetitle)){
//                if(cancelStatusList.get(i).equalsIgnoreCase("0")) {
                    AppointmentModel appointmentModel = new AppointmentModel();
                    appointmentModel.setId(idList.get(i));
                    appointmentModel.setUser_id(userIdList.get(i));
                    appointmentModel.setAppointment_date(apptDateList.get(i));
                    appointmentModel.setStart_time(starttimeList.get(i));
                    appointmentModel.setTime_token(timetokenList.get(i));
                    appointmentModel.setStatus(statusList.get(i));
                    appointmentModel.setApp_name(appnameList.get(i));
                    appointmentModel.setApp_email(appemailList.get(i));
                    appointmentModel.setCreated_at(createdatList.get(i));
                    appointmentModel.setBus_con_time(contimeList.get(i));
                    appointmentModel.setBus_title(busTitleList.get(i));
                    appointmentModel.setBus_slug(busSlugList.get(i));
                    appointmentModel.setDoct_name(doctNameList.get(i));
                    appointmentModel.setDoct_speciality(doctSpecialtyList.get(i));
                    appointmentModel.setPayment_ref(paymentRefList.get(i));
                    appointmentModel.setImage(imageList.get(i));
                    appointmentModel.setCancel_status(cancelStatusList.get(i));

                    appointmentModelArrayList.add(appointmentModel);
//                }
            }
        }

        //ARRANGE TIME
        Collections.sort(appointmentModelArrayList, new Comparator<AppointmentModel>() {
            @Override
            public int compare(AppointmentModel o1, AppointmentModel o2) {
                if (o1.getStart_time() == null || o2.getStart_time() == null)
                    return 0;
                return o1.getStart_time().compareTo(o2.getStart_time());
            }
        });

        if(appointmentModelArrayList.size() == 0){
            holder.itemView.setVisibility(View.GONE);
            holder.itemView.setLayoutParams(new RecyclerView.LayoutParams(0, 0));
        }

        LinearLayoutManager layoutManager = new LinearLayoutManager(
                holder.rvSchedSubitem.getContext(),
                LinearLayoutManager.VERTICAL,
                false
        );
        layoutManager.setInitialPrefetchItemCount(appointmentModelArrayList.size());
        myAppointmentAdapter = new MyApptSchedAdapter(activity, appointmentModelArrayList);
        myAppointmentAdapter.setHasStableIds(true);

        holder.rvSchedSubitem.setLayoutManager(layoutManager);
        holder.rvSchedSubitem.setAdapter(myAppointmentAdapter);
        holder.rvSchedSubitem.setRecycledViewPool(viewPool);

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class SchedApptHolder extends RecyclerView.ViewHolder {
        LinearLayout mainItemView;
        TextView tvScheddate;
        RecyclerView rvSchedSubitem;

        public SchedApptHolder(View itemView) {
            super(itemView);
            mainItemView = (LinearLayout) itemView.findViewById(R.id.item_view);
            tvScheddate = (TextView) itemView.findViewById(R.id.tv_sched_title);
            rvSchedSubitem = (RecyclerView) itemView.findViewById(R.id.rv_sched_subitem);
        }
    }

    private long stringToCalendar(String stringdate){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        Date d1 = null;

        try {
            d1 = sdf.parse(stringdate);

            //in milliseconds
        } catch (Exception e) {
            e.printStackTrace();
        }

        long d_one = d1.getTime();

        return d_one;
    }
}
