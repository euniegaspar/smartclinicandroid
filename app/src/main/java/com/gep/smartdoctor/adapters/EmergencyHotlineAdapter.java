package com.gep.smartdoctor.adapters;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.gep.smartdoctor.R;
import com.gep.smartdoctor.activities.ActivityMain;
import com.gep.smartdoctor.models.AmbulanceModel;
import com.gep.smartdoctor.models.EmergencyHotlinesModel;

import java.util.ArrayList;

public class EmergencyHotlineAdapter extends RecyclerView.Adapter<EmergencyHotlineAdapter.HotlinesHolder> {

    private ArrayList<EmergencyHotlinesModel> list;
    private Activity activity;

    //temporary
    private int tabIcons[] = {R.drawable.hotline1, R.drawable.hotline2, R.drawable.hotline3};


    public EmergencyHotlineAdapter(Activity activity, ArrayList<EmergencyHotlinesModel> list) {
        this.list = list;
        this.activity = activity;
    }

    @Override
    public EmergencyHotlineAdapter.HotlinesHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_hotlines, parent, false);

        return new EmergencyHotlineAdapter.HotlinesHolder(view);
    }

    @Override
    public void onBindViewHolder(final EmergencyHotlineAdapter.HotlinesHolder holder, final int position) {
        EmergencyHotlinesModel emergencyHotlineModel = list.get(position);
        holder.setIsRecyclable(false);

        holder.tvTitle.setText(emergencyHotlineModel.getTitle());
        holder.tvHotline.setText(emergencyHotlineModel.getHotline());
        holder.tvTrunkline.setText(emergencyHotlineModel.getTrunkline());

        if(emergencyHotlineModel.getHotline().equals("")){
            holder.viewHotline.setVisibility(View.GONE);
        }

        if(emergencyHotlineModel.getTrunkline().equals("")){
            holder.viewTrunkline.setVisibility(View.GONE);
        }

        RequestOptions requestOptions = new RequestOptions();
        requestOptions = requestOptions.transforms(new CenterCrop(), new RoundedCorners(10));
        Glide.with(activity)
                .load(tabIcons[position])
                .apply(requestOptions)
                .into(holder.imgStation);


        holder.tvTrunkline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callNumber(holder.tvTrunkline.getText().toString());
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class HotlinesHolder extends RecyclerView.ViewHolder {
        RelativeLayout viewHotline, viewTrunkline;
        TextView tvTitle, tvHotline, tvTrunkline;
        ImageView imgStation;

        public HotlinesHolder(View itemView) {
            super(itemView);
            tvTitle = (TextView) itemView.findViewById(R.id.tv_title);
            tvHotline = (TextView) itemView.findViewById(R.id.tv_hotline);
            tvTrunkline = (TextView) itemView.findViewById(R.id.tv_trunkline);
            imgStation = (ImageView) itemView.findViewById(R.id.img_station);

            viewHotline = (RelativeLayout) itemView.findViewById(R.id.label_hotline);
            viewTrunkline = (RelativeLayout) itemView.findViewById(R.id.label_trunkline);
        }
    }


    private void callNumber(String number){
        number = number.replace("SUN", "")
                .replace("GLOBE", "")
                .replace("SMART", "")
                .trim();

        if (Build.VERSION.SDK_INT > 22) {
            if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CALL_PHONE}, 101);
                return;
            }
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:" + number));
            activity.startActivity(callIntent);
        } else {

            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:" + number));
            activity.startActivity(callIntent);
        }

        ActivityMain.tabPosition = 1;
    }
}