package com.gep.smartdoctor.models;

import java.util.List;

public class ScheduleItemModel {

    private String scheduleDateID;
    private String scheduleItemTitle;
    private String responseString;

    public ScheduleItemModel() {
    }

    public String getScheduleDateID() {
        return scheduleDateID;
    }

    public void setScheduleDateID(String scheduleDateID) {
        this.scheduleDateID = scheduleDateID;
    }

    public ScheduleItemModel(String scheduleItemTitle, List<AppointmentModel> schedItemSubtitle) {
        this.scheduleItemTitle = scheduleItemTitle;
//        this.schedItemSubtitle = schedItemSubtitle;
    }

    public String getScheduleItemTitle() {
        return scheduleItemTitle;
    }

    public void setScheduleItemTitle(String scheduleItemTitle) {
        this.scheduleItemTitle = scheduleItemTitle;
    }

    public String getResponseString() {
        return responseString;
    }

    public void setResponseString(String responseString) {
        this.responseString = responseString;
    }
}
