package com.gep.smartdoctor.models;

public class EmergencyHotlinesModel {

    private String id;
    private String title;
    private String hotline;
    private String trunkline;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getHotline() {
        return hotline;
    }

    public void setHotline(String hotline) {
        this.hotline = hotline;
    }

    public String getTrunkline() {
        return trunkline;
    }

    public void setTrunkline(String trunkline) {
        this.trunkline = trunkline;
    }
}
