package com.gep.smartdoctor.Config;

/**
 * Created by LENOVO on 4/20/2016.
 * Please Note following BASE_URL is an url of your backend link where you hosted backend.. you can give your ip or domain path here..
 * This is end point of API url.. through this url all api calls.
 * <p>
 * Currency use in app..
 */
public class ConstValue {

    //public static String BASE_URL = "http://iclauncher.com/doctappo";
//    public static String BASE_URL = "https://beta.smartclinicph.com";
    public static String BASE_URL = "https://staging-dashboard.smartclinicph.com";

//    public static String BASE_URL = "http://139.180.138.159";
    public static String CURRENCY = "Rs.";

    public static String PRIVACY_POLICY_URL = BASE_URL + "/index.php/PrivacyPolicy";
    public static String TERMS_CONDITIONS_URL = BASE_URL + "/index.php/PrivacyPolicy";

    /**
     * PAYPAL SETTINGS
     **/
    public static final Boolean enable_paypal = false;
    public static final String paypal_target_url_prefix = "iclauncher.com";

    public static final String PREFS_LANGUAGE = "doct_lang_condensed";
    public static final String PREFS_LANGUAGE_FULL = "doct_lang_full";

    /**
     * ALLOW to show Arabic language choose button on drawer menu_language
     **/
    public static final boolean ENABLE_ARABIC_LANG_TO_CHOOSE = true;



    public  static  final String LANGUAGE="lang_select";



}
