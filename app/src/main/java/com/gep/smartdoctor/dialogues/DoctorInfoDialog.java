package com.gep.smartdoctor.dialogues;

import android.app.Activity;
import android.app.Dialog;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.gep.smartdoctor.R;
import com.gep.smartdoctor.Config.ConstValue;
import com.gep.smartdoctor.models.DoctorModel;
import com.gep.smartdoctor.util.CircleTransform;
import com.squareup.picasso.Picasso;

public class DoctorInfoDialog extends Dialog {

    private Activity mContext;
    private SharedPreferences sharedPreferences;
    private DoctorModel doctorModel;

    public DoctorInfoDialog(final Activity context, DoctorModel arrayList) {
        super(context);
        this.requestWindowFeature(1);
        this.setContentView(R.layout.dialog_doctor_info);
        this.setCanceledOnTouchOutside(false);
        this.setCancelable(true);
        this.mContext = context;
        doctorModel = arrayList;
        //setTitle("Docutor");

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        try {
            getWindow().setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
            getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        } catch (Exception ex) {}

        ImageView btnClose = (ImageView) this.findViewById(R.id.btn_close);
        TextView lbl_title = (TextView) this.findViewById(R.id.title);
        TextView lbl_degree = (TextView) this.findViewById(R.id.degree);
        TextView lbl_email = (TextView) this.findViewById(R.id.email);
        TextView lbl_phone = (TextView) this.findViewById(R.id.phone);
        ImageView icon_image = (ImageView) this.findViewById(R.id.imageView);
        String path = doctorModel.getDoct_photo();

        Picasso.with(context).load(ConstValue.BASE_URL + "/uploads/business/" + path)
                 .placeholder(R.drawable.icon_doctor)
                .error(R.drawable.icon_doctor)
                .resize(200, 200)
                .transform(new CircleTransform())
                .centerCrop()
                .into(icon_image);


        lbl_title.setText(doctorModel.getDoct_name());
        lbl_degree.setText(/*doctorModel.getDoct_degree() + "  " + */doctorModel.getDoct_speciality());
        lbl_email.setText(doctorModel.getDoct_email());
        lbl_phone.setText(doctorModel.getDoct_phone());
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancel();
            }
        });
    }

    public void dismiss() {
        super.dismiss();
    }

}
