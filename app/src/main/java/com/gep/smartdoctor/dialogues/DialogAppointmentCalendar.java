package com.gep.smartdoctor.dialogues;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.applandeo.materialcalendarview.EventDay;
import com.applandeo.materialcalendarview.exceptions.OutOfDateRangeException;
import com.applandeo.materialcalendarview.listeners.OnCalendarPageChangeListener;
import com.applandeo.materialcalendarview.listeners.OnDayClickListener;
import com.gep.smartdoctor.R;
import com.gep.smartdoctor.activities.ActivityTimeSlot;
import com.gep.smartdoctor.fragments.FragmentAppointment;
import com.gep.smartdoctor.fragments.FragmentSchedule;
import com.gep.smartdoctor.models.ScheduleItemModel;
import com.gep.smartdoctor.util.CommonClass;
import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.github.sundeepk.compactcalendarview.domain.Event;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

public class DialogAppointmentCalendar extends Dialog {

    private Activity mContext;
    private CommonClass common;
    com.applandeo.materialcalendarview.CalendarView calendarViewheader;
    CompactCalendarView calendarView;

    TextView tvDateLabel;
    ImageView btnNext, btnPrev;

    public DialogAppointmentCalendar(final Activity context) {
        super(context);
        this.requestWindowFeature(1);
        this.setContentView(R.layout.dialog_calendar_filter);
        this.setCanceledOnTouchOutside(true);
        this.setCancelable(true);
        this.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        this.mContext = context;

        calendarViewheader = (com.applandeo.materialcalendarview.CalendarView) findViewById(R.id.topview);
        calendarView = (CompactCalendarView) findViewById(R.id.calendar_view);
        calendarView.setFirstDayOfWeek(Calendar.SATURDAY);
        calendarView.setUseThreeLetterAbbreviation(true);
        calendarView.shouldScrollMonth(false);

        FragmentAppointment.mCalendar = Calendar.getInstance(TimeZone.getDefault());
        FragmentAppointment.c_day = FragmentAppointment.mCalendar.get(Calendar.DAY_OF_MONTH);
        FragmentAppointment.c_month = FragmentAppointment.mCalendar.get(Calendar.MONTH);
        FragmentAppointment.c_year = FragmentAppointment.mCalendar.get(Calendar.YEAR);
        FragmentAppointment.mCalendar.set(FragmentAppointment.c_year, FragmentAppointment.c_month, FragmentAppointment.c_day);

        try {
            calendarViewheader.setDate(FragmentAppointment.mCalendar);
        } catch (OutOfDateRangeException e) {
            e.printStackTrace();
        }

        //For event filtering
        ArrayList<String> filterLong = new ArrayList<>();
        for(int i = 0 ; i < FragmentAppointment.calendarEvents.size() ; i++) {
            filterLong.add(String.valueOf(FragmentAppointment.calendarEvents.get(i)));
        }

        //filters duplicate item
        HashSet<String> hashSet = new HashSet<String>();
        hashSet.addAll(filterLong);
        filterLong.clear();
        filterLong.addAll(hashSet);

        Event event;
        for(int i = 0 ; i < filterLong.size() ; i++) {
            Long eventLong = Long.parseLong(filterLong.get(i));
            event = new Event(Color.RED, eventLong);
            calendarView.addEvent(event);
        }

        // define a listener to receive callbacks when certain events happen.
        calendarView.setListener(new CompactCalendarView.CompactCalendarViewListener() {
            @Override
            public void onDayClick(Date dateClicked) {
                FragmentAppointment.mCalendar.setTime(dateClicked);

                FragmentAppointment.c_day = FragmentAppointment.mCalendar.get(Calendar.DAY_OF_MONTH);
                FragmentAppointment.c_month = FragmentAppointment.mCalendar.get(Calendar.MONTH);
                FragmentAppointment.c_year = FragmentAppointment.mCalendar.get(Calendar.YEAR);
                FragmentAppointment.mCalendar.set(FragmentAppointment.c_year, FragmentAppointment.c_month, FragmentAppointment.c_day);

                SimpleDateFormat month_date = new SimpleDateFormat("yyyy-MM-dd");
                String currentDate = month_date.format(FragmentAppointment.mCalendar.getTime());

                FragmentAppointment.fetchAppointmentList(context, currentDate);
                dismiss();

                FragmentAppointment.mCalendar.set(FragmentAppointment.c_year, FragmentAppointment.c_month, FragmentAppointment.c_day);
            }

            @Override
            public void onMonthScroll(Date firstDayOfNewMonth) {

            }
        });

        //top calendar
        calendarViewheader.setOnForwardPageChangeListener(new OnCalendarPageChangeListener() {
            @Override
            public void onChange() {
                calendarView.showNextMonth();
            }
        });

        calendarViewheader.setOnPreviousPageChangeListener(new OnCalendarPageChangeListener() {
            @Override
            public void onChange() {
                calendarView.showPreviousMonth();
            }
        });
    }

    public void dismiss() {
        super.dismiss();
    }
}
