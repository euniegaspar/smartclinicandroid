package com.gep.smartdoctor.dialogues;

import android.app.Activity;
import android.app.Dialog;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.gep.smartdoctor.Config.ConstValue;
import com.gep.smartdoctor.R;
import com.gep.smartdoctor.models.PhotosModel;

public class PhotosDialog extends Dialog {

    private Activity mContext;
    private SharedPreferences sharedPreferences;
    private PhotosModel photosModel;

    public PhotosDialog(final Activity context, PhotosModel arrayList) {
        super(context);
        this.requestWindowFeature(1);
        this.setContentView(R.layout.dialog_photos);
        this.setCanceledOnTouchOutside(false);
        this.setCancelable(true);
        this.mContext = context;
        photosModel = arrayList;

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        try {
            getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        } catch (Exception ex) {

        }

        ImageView btnClose = (ImageView) this.findViewById(R.id.btn_close);
        ImageView icon_image = (ImageView) this.findViewById(R.id.imageView);
        String path = photosModel.getPhoto_image();

        RequestOptions requestOptions = new RequestOptions();
        requestOptions = requestOptions.transforms(new CenterCrop(), new RoundedCorners(50));

        Glide.with(context)
                .load(ConstValue.BASE_URL + "/uploads/business/businessphoto/" + path)
                .apply(requestOptions)
                .into(icon_image);

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancel();
            }
        });
    }


    public void dismiss() {
        super.dismiss();
    }

}
