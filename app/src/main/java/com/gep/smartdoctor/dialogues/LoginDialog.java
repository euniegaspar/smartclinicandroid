package com.gep.smartdoctor.dialogues;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.gep.smartdoctor.R;
import com.gep.smartdoctor.activities.ActivityLogin;
import com.gep.smartdoctor.activities.ActivityMain;
import com.gep.smartdoctor.fragments.FragmentHome;

public class LoginDialog extends Dialog {

    private Activity mContext;
    private SharedPreferences sharedPreferences;
    ImageView btnClose;

    public LoginDialog(final Activity context) {
        super(context);
        this.requestWindowFeature(1);
        this.setContentView(R.layout.dialog_login);
        this.setCanceledOnTouchOutside(false);
        this.setCancelable(true);
        this.mContext = context;

        try {
            getWindow().setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
            getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        } catch (Exception ex) {

        }

        ImageView btnClose = (ImageView) this.findViewById(R.id.btn_close);
        TextView btnLogin = (TextView) this.findViewById(R.id.btn_login);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ActivityLogin.class);
                context.startActivity(intent);
                cancel();
            }
        });

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancel();
                ((ActivityMain) context).switchToHome();
            }
        });
    }

    public void dismiss() {
        super.dismiss();
    }
}