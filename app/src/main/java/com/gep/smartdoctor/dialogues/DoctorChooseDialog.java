package com.gep.smartdoctor.dialogues;

import android.app.Activity;
import android.app.Dialog;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.preference.PreferenceManager;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import com.gep.smartdoctor.Config.ConstValue;
import com.gep.smartdoctor.R;
import com.gep.smartdoctor.activities.ActivityTimeSlot;

import java.util.ArrayList;

import com.gep.smartdoctor.adapters.DoctorAdapter;
import com.gep.smartdoctor.models.ActiveModels;
import com.gep.smartdoctor.models.DoctorModel;
import com.gep.smartdoctor.util.CircleTransform;
import com.gep.smartdoctor.util.RecyclerItemClickListener;
import com.squareup.picasso.Picasso;


/**
 * Created by Dell on 21-11-2016.
 */

public class DoctorChooseDialog extends Dialog {

    private Activity mContext;
    private SharedPreferences sharedPreferences;
    private ArrayList<DoctorModel> mDoctorArray;
    private DoctorAdapter doctorAdapter;
    private RecyclerView recyclerView;
    String currentdate;

    public DoctorChooseDialog(final Activity context, ArrayList<DoctorModel> arrayList, final String currentdate) {
        super(context);
        this.requestWindowFeature(1);
        this.setContentView(R.layout.dialog_doctors);
        this.setCanceledOnTouchOutside(false);
        this.setCancelable(true);
        this.mContext = context;
        this.currentdate = currentdate;
        mDoctorArray = arrayList;

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        try {
            getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
            getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        } catch (Exception ex) {

        }

        ImageView btnClose = (ImageView) this.findViewById(R.id.btn_close);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        recyclerView = (RecyclerView) this.findViewById(R.id.rv_list);
        recyclerView.setLayoutManager(layoutManager);
        doctorAdapter = new DoctorAdapter(context, mDoctorArray);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mContext,
                layoutManager.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);
        recyclerView.setAdapter(doctorAdapter);

        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(context, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                ActiveModels.DOCTOR_MODEL = mDoctorArray.get(position); //identify doctor array details
                if (mContext instanceof ActivityTimeSlot) {
                    ((ActivityTimeSlot) mContext).doctor_id = ActiveModels.DOCTOR_MODEL.getDoct_id();
                    ((ActivityTimeSlot) mContext).loadDoctorData();

                    ((ActivityTimeSlot) mContext).tvDoctorName.setText(ActiveModels.DOCTOR_MODEL.getDoct_name());
                    ((ActivityTimeSlot) mContext).tvDoctorDeg.setText(ActiveModels.DOCTOR_MODEL.getDoct_speciality());
                    ((ActivityTimeSlot) mContext).tvDoctorDeg.setVisibility(View.VISIBLE);
                    ((ActivityTimeSlot) mContext).layoutTimeTable.setVisibility(View.VISIBLE);
                    ((ActivityTimeSlot) mContext).tvNoTimeSlotLabel.setVisibility(View.GONE);

                    Picasso.with(mContext)
                            .load(ConstValue.BASE_URL + "/uploads/business/" + ActiveModels.DOCTOR_MODEL.getDoct_photo())
                            .placeholder(R.drawable.icon_doctor)
                            .resize(200, 200)
                            .transform(new CircleTransform())
                            .error(R.drawable.icon_doctor)
                            .into(((ActivityTimeSlot) mContext).imgDoctor);
                }
                dismiss();
            }
        }));

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancel();
            }
        });
    }


    public void dismiss() {
        super.dismiss();
    }

}
