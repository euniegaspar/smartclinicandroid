package com.gep.smartdoctor.dialogues;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;

import com.applandeo.materialcalendarview.EventDay;
import com.applandeo.materialcalendarview.exceptions.OutOfDateRangeException;
import com.applandeo.materialcalendarview.listeners.OnDayClickListener;
import com.gep.smartdoctor.R;
import com.gep.smartdoctor.activities.ActivityTimeSlot;
import com.gep.smartdoctor.util.CommonClass;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DialogCalendar extends Dialog {

//    private Activity mContext;
    private CommonClass common;
    com.applandeo.materialcalendarview.CalendarView calendarView;
    Calendar mCalendar;

    public DialogCalendar(Activity mContext) {
        super(mContext);
        this.requestWindowFeature(1);
        this.setContentView(R.layout.dialog_calendar_timeslot);
        this.setCanceledOnTouchOutside(true);
        this.setCancelable(true);
        this.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        this.mContext = context;

        calendarView = (com.applandeo.materialcalendarview.CalendarView) findViewById(R.id.calendar_view_timeslot);



        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                int c_day = ((ActivityTimeSlot) mContext).c_day;
                int c_month = ((ActivityTimeSlot) mContext).c_month - 1;
                int c_year = ((ActivityTimeSlot) mContext).c_year;

                mCalendar = ((ActivityTimeSlot) mContext).calender;
                mCalendar.set(c_year, c_month, c_day);

                try {
                    calendarView.setDate(mCalendar);
                } catch (OutOfDateRangeException e) {
                    e.printStackTrace();
                }
            }
        }, 500);


        calendarView.setOnDayClickListener(new OnDayClickListener() {
            @Override
            public void onDayClick(EventDay eventDay) {
                ((ActivityTimeSlot) mContext).i_adjustedDayOfYear = eventDay.getCalendar().get(Calendar.DAY_OF_YEAR);
                ((ActivityTimeSlot) mContext).c_day = eventDay.getCalendar().get(Calendar.DAY_OF_MONTH);
                ((ActivityTimeSlot) mContext).c_month = eventDay.getCalendar().get(Calendar.MONTH) + 1;
                ((ActivityTimeSlot) mContext).c_year = eventDay.getCalendar().get(Calendar.YEAR);
                ((ActivityTimeSlot) mContext).currentDayNameString = eventDay.getCalendar().getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, Locale.getDefault());
                ((ActivityTimeSlot) mContext).currentMonthNameString = eventDay.getCalendar().getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault());
                ((ActivityTimeSlot) mContext).currentdate = ((ActivityTimeSlot) mContext).c_year + "-" + ((ActivityTimeSlot) mContext).c_month + "-" + ((ActivityTimeSlot) mContext).c_day;
                ((ActivityTimeSlot) mContext).tvDateLabel.setText(((ActivityTimeSlot) mContext).currentDayNameString + " " + ((ActivityTimeSlot) mContext).c_day + ", " + ((ActivityTimeSlot) mContext).currentMonthNameString);

                ((ActivityTimeSlot) mContext).loadDoctorData();
                dismiss();
            }
        });
    }

    public void dismiss() {
        super.dismiss();
    }
}
