package com.gep.smartdoctor.activities;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.core.app.TaskStackBuilder;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gep.smartdoctor.Config.ConstValue;
import com.gep.smartdoctor.R;
import com.gep.smartdoctor.util.CommonClass;

import java.util.HashMap;

public class ActivitySettings extends CommonActivity implements PopupMenu.OnMenuItemClickListener {

    PackageInfo packageInfo;
    TextView tvVersion, tvLanguage;
    RelativeLayout btnLanguage;
    private SharedPreferences sharedPreferences;
    String languageCondensed, languageFull;
    Button btnSave;
    ImageView btnClose;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        try {
            packageInfo = getApplicationContext().getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        tvLanguage = (TextView) findViewById(R.id.tv_language);
        btnClose = (ImageView) findViewById(R.id.btn_close);

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        languageCondensed = sharedPreferences.getString(ConstValue.PREFS_LANGUAGE,"en");
        languageFull = sharedPreferences.getString(ConstValue.PREFS_LANGUAGE_FULL,"English");
        tvLanguage.setText(languageFull);
        btnLanguage = (RelativeLayout) findViewById(R.id.btn_language);

        String version = packageInfo.versionName;
        int verCode = packageInfo.versionCode;
        tvVersion = (TextView) findViewById(R.id.tv_version);
        tvVersion.setText("Version " + version + " (" + verCode + ")");
        btnSave = (Button) findViewById(R.id.btn_save);
        btnSave.setEnabled(false);

        btnLanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createPopupMenu();
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!languageCondensed.equals(sharedPreferences.getString(ConstValue.PREFS_LANGUAGE,"en"))){
                    saveLanguage(languageCondensed, languageFull);
                    CommonClass.initRTL(getApplicationContext(),languageCondensed);

                    final Dialog dialog = new Dialog(v.getContext());
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setCancelable(false);
                    dialog.setContentView(R.layout.dialog_successful);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                    TextView text = (TextView) dialog.findViewById(R.id.dialog_title);
                    text.setText(getResources().getString(R.string.successful));

                    Button btnConfirm = (Button) dialog.findViewById(R.id.btn_ok);
                    btnConfirm.setAllCaps(true);

                    btnConfirm.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                            TaskStackBuilder tsb = TaskStackBuilder.from(getApplicationContext());
                            tsb.addParentStack(ActivityMain.class);
                            tsb.addNextIntent(new Intent(getApplicationContext(), ActivityMain.class));
                            tsb.addNextIntent(new Intent(getApplicationContext(), ActivitySettings.class));
                            tsb.startActivities();
                            overridePendingTransition(0,0);
                        }
                    });

                    dialog.show();
                } else{
                    onBackPressed();
                }
            }
        });

    }

    public void createPopupMenu(){
        PopupMenu popupMenu = new PopupMenu(ActivitySettings.this, btnLanguage);
        popupMenu.getMenuInflater().inflate(R.menu.menu_language, popupMenu.getMenu());
        popupMenu.setOnMenuItemClickListener(this);
        popupMenu.show();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_language, menu);
        HashMap<String,Object> param=new HashMap<>();

        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        if(languageFull.equals((String) item.getTitle())){
            btnSave.setBackground(getApplicationContext().getResources().getDrawable(R.drawable.rounded_btn_gray));
            btnSave.setEnabled(false);
        } else{
            btnSave.setBackground(getApplicationContext().getResources().getDrawable(R.drawable.rounded_button));
            btnSave.setEnabled(true);
        }

        languageFull = (String) item.getTitle();
        languageCondensed = (String) item.getTitleCondensed();
        tvLanguage.setText(languageFull);
        return false;
    }

    public void saveLanguage(String language, String languageFull) {
        try {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(ConstValue.PREFS_LANGUAGE, language);
            editor.putString(ConstValue.PREFS_LANGUAGE_FULL, languageFull);

            editor.apply();

            language = language.equalsIgnoreCase("ar") ? "Arabic" : "English";
            //Toast.makeText(context, language + " is your preferred language.", Toast.LENGTH_SHORT).show();
        } catch (Exception exc) {

        }

    }
}