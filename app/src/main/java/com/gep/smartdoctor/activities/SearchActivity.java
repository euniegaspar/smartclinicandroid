package com.gep.smartdoctor.activities;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;

import com.gep.smartdoctor.Config.ApiParams;
import com.gep.smartdoctor.R;
import com.gep.smartdoctor.adapters.LocalityAdapter;
import com.gep.smartdoctor.models.LocalityModel;
import com.gep.smartdoctor.util.CommonClass;
import com.gep.smartdoctor.util.VJsonRequest;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SearchActivity extends CommonActivity {

    private ArrayList<LocalityModel> localityArray;
    private ArrayList<LocalityModel> searchArray;
    public RecyclerView localityRecycler;
    private LocalityAdapter localityAdapter;

    private SeekBar seekBar;
    private TextView txtArea;
    private Switch switch1;
    public EditText etSearch, etLocality;
    private ImageView btnDropdown;

    RelativeLayout btnBack;
//    View emptyLayout;
    Button btnRetry;

    public String locality, latitude, longitude, locality_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        localityArray = new ArrayList<>();
        searchArray = new ArrayList<>();

//        emptyLayout = (View) findViewById(R.id.emptyLayout);
//        btnRetry = (Button) emptyLayout.findViewById(R.id.btn_retry);
//        emptyLayout.setVisibility(View.GONE);
//        initInternet();

//        btnRetry.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                initInternet();
//            }
//        });

        btnBack = (RelativeLayout) findViewById(R.id.btn_back);
        btnDropdown = (ImageView) findViewById(R.id.btn_dropdown);
        txtArea = (TextView) findViewById(R.id.textArea);
        seekBar = (SeekBar) findViewById(R.id.seekBar);
        switch1 = (Switch) findViewById(R.id.switch1);
        etSearch = (EditText) findViewById(R.id.search_keyword);
        etSearch.setFilters(CommonClass.getFilterWithoutSpecialChars(150));
        etLocality = (EditText) findViewById(R.id.et_locality);
        etLocality.setFilters(CommonClass.getFilter());
        etLocality.setFilters(CommonClass.getFilterWithoutSpecialChars(200));

        localityRecycler = (RecyclerView) findViewById(R.id.rv_artist);
        localityRecycler.setVisibility(View.GONE);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(localityRecycler.getContext(),
                layoutManager.getOrientation());
        localityRecycler.addItemDecoration(dividerItemDecoration);
        localityRecycler.setLayoutManager(layoutManager);

        localityAdapter = new LocalityAdapter(this, searchArray);
        localityRecycler.setAdapter(localityAdapter);
        loadData();

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                common.hideKeyboard(SearchActivity.this);
                finish();
                ActivityMain.tabPosition = 1;
            }
        });

        btnDropdown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(localityRecycler.getVisibility() == View.GONE){
                    localityRecycler.setVisibility(View.VISIBLE);
                } else{
                    localityRecycler.setVisibility(View.GONE);
                }
            }
        });

        seekBar.setMax(100);
        txtArea.setText(common.getSessionInt("radius") + " " + getString(R.string.KM));
        seekBar.setProgress(common.getSessionInt("radius"));

        etLocality.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                if(etLocality.getText().length() > 0) {
                    localityRecycler.setVisibility(View.VISIBLE);
                } else{
                    localityRecycler.setVisibility(View.GONE);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                searchArray.clear();
                for (LocalityModel str : localityArray) {
                    Pattern p = Pattern.compile(etLocality.getText().toString().toLowerCase() + "(.*)");
                    Matcher m = p.matcher(str.getLocality().toLowerCase());
                    if (m.find()) {
                        searchArray.add(str);
                    }
                }
                localityAdapter.notifyDataSetChanged();

                if(searchArray.size() == 0){
                    localityRecycler.setVisibility(View.GONE);
                }
            }
        });

        if (!common.containKeyInSession("nearby_enable")) {
            common.setSessionBool("nearby_enable", true);
        }
        switch1.setChecked(common.getSessionBool("nearby_enable"));
        switch1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                common.setSessionBool("nearby_enable", b);
                seekBar.setEnabled(b);
            }
        });
        seekBar.setEnabled(common.getSessionBool("nearby_enable"));
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                common.setSessionInt("radius", i);
                txtArea.setText(common.getSessionInt("radius") + " " + getString(R.string.KM));
//                txtArea.setText(common.getSessionInt("60") + getString(R.string.KM));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    private void initInternet(){
//        if(CommonClass.is_internet_connected(this)){
//            emptyLayout.setVisibility(View.GONE);
//        } else{
//            emptyLayout.setVisibility(View.VISIBLE);
//        }
    }

    // load data from api
    public void loadData() {
        // this class for handle request response thread and return response data
        VJsonRequest vJsonRequest = new VJsonRequest(SearchActivity.this, ApiParams.GET_LOCALITY,
                new VJsonRequest.VJsonResponce() {
                    @Override
                    public void VResponce(String responce) {

                        Gson gson = new Gson();
                        Type listType = new TypeToken<List<LocalityModel>>() {
                        }.getType();
                        localityArray.clear();
                        searchArray.clear();
                        localityArray.addAll((Collection<? extends LocalityModel>) gson.fromJson(responce, listType));
                        searchArray.addAll((Collection<? extends LocalityModel>) gson.fromJson(responce, listType));
                        localityAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void VError(String responce) {

                    }
                });
    }

    // search button click event
    public void SearchButtonClick(View view) {

        Intent intent = new Intent(SearchActivity.this, ActivityClinicList.class);
        Bundle b = new Bundle();
        if (!TextUtils.isEmpty(etSearch.getText().toString())) {
            b.putString("search", etSearch.getText().toString());
        } else {
            b.putString("search", "");
        }

        if(!etLocality.getText().toString().equals("")){
            if(latitude == null || longitude == null){
                goToLocationFromAddress(etLocality.getText().toString());
                b.putString("locality", "");
                b.putString("locality_id", "");
            }

            if(locality != null || locality_id != null){
                b.putString("lat", latitude);
                b.putString("lon", longitude);
                b.putString("locality", locality);
                b.putString("locality_id", locality_id);
            }
        }
        intent.putExtras(b);
        startActivity(intent);

        latitude = null;
        longitude = null;
    }

    public void goToLocationFromAddress(String strAddress) {
        //Create coder with Activity context - this
        Geocoder coder = new Geocoder(this);
        List<Address> address;

        try {
            //Get latLng from String
            address = coder.getFromLocationName(strAddress, 5);

            //check for null
            if (address != null) {

                //Lets take first possibility from the all possibilities.
                try {
                    Address location = address.get(0);
                    LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());

//                    //Animate and Zoon on that map location
//                    map.moveCamera(CameraUpdateFactory.newLatLng(latLng));
//                    mMap.animateCamera(CameraUpdateFactory.zoomTo(15));

                    latitude = String.valueOf(location.getLatitude());
                    longitude = String.valueOf(location.getLongitude());

                    if(common.getSessionBool("nearby_enable")){
                        latitude = String.valueOf(location.getLatitude() + common.getSessionInt("radius"));
                        longitude = String.valueOf(location.getLongitude() + common.getSessionInt("radius"));
                    }

                } catch (IndexOutOfBoundsException er) {
//                    Toast.makeText(this, "Location isn't available", Toast.LENGTH_SHORT).show();
                }

            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }
}
