package com.gep.smartdoctor.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gep.smartdoctor.R;
import com.gep.smartdoctor.adapters.AmbulanceAdapter;
import com.gep.smartdoctor.adapters.LocalityAdapter;
import com.gep.smartdoctor.models.AmbulanceModel;
import com.gep.smartdoctor.models.AppointmentModel;

import java.util.ArrayList;

public class ActivityAmbulance extends CommonActivity {

    RelativeLayout btnBack;
    RecyclerView rvAmbulance;
    AmbulanceAdapter ambulanceAdapter;
    ArrayList<AmbulanceModel> ambulanceArray;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ambulance);

        ambulanceArray = new ArrayList<>();
        ArrayList<String> title = new ArrayList<String>();
        ArrayList<String> address = new ArrayList<String>();
        ArrayList<String> phone = new ArrayList<String>();

        btnBack = (RelativeLayout) findViewById(R.id.btn_back);
        rvAmbulance = (RecyclerView) findViewById(R.id.rv_ambulance);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rvAmbulance.setLayoutManager(layoutManager);
        ambulanceAdapter = new AmbulanceAdapter(this, ambulanceArray);
        rvAmbulance.setAdapter(ambulanceAdapter);

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                ActivityMain.tabPosition = 1;
            }
        });


        //temporary
        title.add("Ace-Cor Ambulance Service");
        title.add("Ambucore Ambulance Services");
        title.add("Statcare EMS Ambulance");
        title.add("Transacare EMS - Ambulance Service");
        title.add("Stat Med Ambulance Service");

        address.add("756 Instruccion St., Sampaloc, Manila, 1008 Metro Manila");
        address.add("42B Nicanor Roxas St., Quezon City, Metro Manila");
        address.add("19-A Samuel Dee, St., Grace Village, Quezon City, 1106 Metro Manila");
        address.add("Brgy. 259 Ibuna, San Juan, Metro Manila");
        address.add("26-C Maginoo, Diliman, Quezon City, 1100 Metro Manila");

        phone.add("0917 506 2280");
        phone.add("(02) 8925 6292");
        phone.add("(02) 8280 4280");
        phone.add("(02) 8514 4243");
        phone.add("(02) 8355 8171");


        for(int i = 0; i < title.size() ; i++) {
            AmbulanceModel ambulanceModel = new AmbulanceModel();
            ambulanceModel.setTitle(title.get(i));
            ambulanceModel.setAddress(address.get(i));
            ambulanceModel.setPhone(phone.get(i));

            ambulanceArray.add(ambulanceModel);
        }
        ambulanceAdapter.notifyDataSetChanged();

    }
}