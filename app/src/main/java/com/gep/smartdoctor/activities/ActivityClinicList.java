package com.gep.smartdoctor.activities;

import android.os.Bundle;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gep.smartdoctor.R;

import com.gep.smartdoctor.fragments.FragmentClinicList;
import com.gep.smartdoctor.fragments.MapFragment;
import com.gep.smartdoctor.models.ActiveModels;
import com.gep.smartdoctor.util.CommonClass;
import com.gep.smartdoctor.util.GPSTracker;
import com.google.android.material.tabs.TabLayout;

public class ActivityClinicList extends CommonActivity {

    private GPSTracker gpsTracker;
    private Double cur_latitude, cur_longitude;
    private DemoCollectionPagerAdapter mDemoCollectionPagerAdapter;
    private ViewPager mViewPager;
    private TabLayout tabLayout;
    public String header;
    View emptyLayout;
    Button btnRetry;

    RelativeLayout btnBack;
    TextView tvHeader, tvList, tvMap;
    ImageView ivList;
    RadioGroup radioGroup;
    LinearLayout btnList, btnMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clinic_list);

        if(getIntent().getExtras().containsKey("search")){
            header = getResources().getString(R.string.clinic_details);
        } else{
            if(ActiveModels.CATEGORY_MODEL != null){
                header = ActiveModels.CATEGORY_MODEL.getTitle();
            }
        }

        initActList();
//        layoutClinics = (LinearLayout) findViewById(R.id.layout_clinics);
        btnBack = (RelativeLayout) findViewById(R.id.btn_back);
        tvHeader = (TextView) findViewById(R.id.tv_header);
        tvList = (TextView) findViewById(R.id.tv_list);
        tvMap = (TextView) findViewById(R.id.tv_map);
        ivList = (ImageView) findViewById(R.id.iv_list);
        radioGroup = (RadioGroup) findViewById(R.id.radiogroup);
        btnList = (LinearLayout) findViewById(R.id.rg_lists);
        btnMap = (LinearLayout) findViewById(R.id.rg_map);

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                ActivityMain.tabPosition = 1;
            }
        });

        btnList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showList();              
            }
        });

        btnMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMap();
            }
        });

        btnList.callOnClick();

        btnRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initActList();
            }
        });

    }

    private void initActList(){
        mViewPager = (ViewPager) findViewById(R.id.pager);
        emptyLayout = (View) findViewById(R.id.emptyLayout);
        btnRetry = (Button) emptyLayout.findViewById(R.id.btn_retry);
        emptyLayout.setVisibility(View.GONE);

        if(CommonClass.is_internet_connected(this)){ ///temporary only
            emptyLayout.setVisibility(View.GONE);

            if (!common.containKeyInSession("nearby_enable")) {
                common.setSessionBool("nearby_enable", true);
            }
            if (common.getSessionBool("nearby_enable")) {
                gpsTracker = new GPSTracker(this);
                if (gpsTracker.canGetLocation()) {
                    if (gpsTracker.getLatitude() != 0.0)
                        cur_latitude = gpsTracker.getLatitude();
                    if (gpsTracker.getLongitude() != 0.0)
                        cur_longitude = gpsTracker.getLongitude();
                } else {
                    gpsTracker.showSettingsAlert();
                }
            }

            mDemoCollectionPagerAdapter = new DemoCollectionPagerAdapter(getSupportFragmentManager());
            mViewPager.setAdapter(mDemoCollectionPagerAdapter);
            mViewPager.setOnPageChangeListener(
                    new ViewPager.SimpleOnPageChangeListener() {
                        @Override
                        public void onPageSelected(int position) {
                            // When swiping between pages, select the
                            // corresponding tab.
                            if(mViewPager.getCurrentItem() == 0) { // Clinic
                                showList();
                            } else { // Map
                                showMap();
                            }
                        }
                    });
        } else{
            emptyLayout.setVisibility(View.VISIBLE);
        }
    }

    private void showList() {
        // layoutClinics.setBackgroundColor(getResources().getColor(R.color.colorTextWhite));
        tvHeader.setText("Clinic");
        btnList.setSelected(true);
        btnMap.setSelected(false);
        btnList.setBackground(getResources().getDrawable(R.drawable.btn_list_selected));
        btnMap.setBackground(getResources().getDrawable(R.drawable.btn_map_unselected));
        ivList.setColorFilter(getResources().getColor(R.color.colorTextWhite), android.graphics.PorterDuff.Mode.SRC_IN);
        tvList.setTextColor(getResources().getColor(R.color.colorTextWhite));
        tvMap.setTextColor(getResources().getColor(R.color.colorPrimary));
        mViewPager.setCurrentItem(0);
    }
    
    private void showMap() {
        // layoutClinics.setBackgroundColor(getResources().getColor(R.color.colorGray));
        tvHeader.setText("Map");
        btnList.setSelected(false);
        btnMap.setSelected(true);
        btnList.setBackground(getResources().getDrawable(R.drawable.btn_list_unselected));
        btnMap.setBackground(getResources().getDrawable(R.drawable.btn_map_selected));
        ivList.setColorFilter(getResources().getColor(R.color.colorPrimary), android.graphics.PorterDuff.Mode.SRC_IN);
        tvList.setTextColor(getResources().getColor(R.color.colorPrimary));
        tvMap.setTextColor(getResources().getColor(R.color.colorTextWhite));
        mViewPager.setCurrentItem(1);
    }

    // set icons on tab
    private void setupTabIcons() {
        tabLayout.getTabAt(0).setIcon(R.drawable.list_icon);
        tabLayout.getTabAt(1).setIcon(R.drawable.map_icon);

    }

    // bind fragment in viewpager
    public class DemoCollectionPagerAdapter extends FragmentStatePagerAdapter {
        public DemoCollectionPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        private String tabTitles[] = new String[]{getString(R.string.tab_list), getString(R.string.tab_map)};

        @Override
        public Fragment getItem(int i) {
            Fragment fragment = null;
            if (i == 0) {
                fragment = new FragmentClinicList(); //LIST OF CLINICS !!
            } else if (i == 1) {
                fragment = new MapFragment();
            }
            if (fragment != null) {
                Bundle args = new Bundle();
                if (getIntent().getExtras().containsKey("cat_id")) {
                    args.putString("cat_id", getIntent().getExtras().getString("cat_id"));
                }
                if (getIntent().getExtras().containsKey("search")) {
                    args.putString("search", getIntent().getExtras().getString("search"));
                }
                if (getIntent().getExtras().containsKey("lat")) {
                    cur_latitude = Double.parseDouble(getIntent().getExtras().getString("lat"));
                }
                if (getIntent().getExtras().containsKey("lon")) {
                    cur_longitude = Double.parseDouble(getIntent().getExtras().getString("lon"));
                }
                if (cur_latitude != null) {
                    args.putString("lat", String.valueOf(cur_latitude));
                }

                if (cur_longitude != null) {
                    args.putString("lon", String.valueOf(cur_longitude));
                }
                if (getIntent().getExtras().containsKey("locality")) {
                    args.putString("locality", getIntent().getExtras().getString("locality"));
                }
                if (getIntent().getExtras().containsKey("locality_id")) {
                    args.putString("locality_id", getIntent().getExtras().getString("locality_id"));
                }
                fragment.setArguments(args);
            }
            return fragment;
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return tabTitles[position];
        }
    }
}
