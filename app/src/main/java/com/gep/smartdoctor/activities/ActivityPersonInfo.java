package com.gep.smartdoctor.activities;

import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gep.smartdoctor.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import com.gep.smartdoctor.Config.ApiParams;
import com.gep.smartdoctor.Config.ConstValue;
import com.gep.smartdoctor.models.ActiveModels;
import com.gep.smartdoctor.util.CommonClass;
import com.gep.smartdoctor.util.NotifyService;
import com.gep.smartdoctor.util.VJsonRequest;
import com.google.android.material.textfield.TextInputLayout;

public class ActivityPersonInfo extends CommonActivity {

    RelativeLayout btnBack;
    private TextInputLayout tilFullName, tilRelationship, tilPhone, tilEmail;
    private EditText editEmail, editPhone, editFullname, etRelationship, etReason;
    private TextView tvPatientNameLabel, tvRelationshipLabel, tvPhoneLabel, tvEmailLabel;

    private String choose_date;
    Button btnConfirm;
    RadioGroup radioGroupPatient;
    RadioButton btnMe, btnOthers;
    LinearLayout layoutPatient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_person_info);

        btnBack = (RelativeLayout) findViewById(R.id.btn_back);
        btnConfirm = (Button) findViewById(R.id.btn_confirm);
        layoutPatient = (LinearLayout) findViewById(R.id.layout_patient);
        radioGroupPatient = (RadioGroup) findViewById(R.id.rg_patient);
        btnMe = (RadioButton) findViewById(R.id.rb_me);
        btnOthers = (RadioButton) findViewById(R.id.rb_others);

        tilFullName = (TextInputLayout) findViewById(R.id.til_fullname);
        tilRelationship = (TextInputLayout) findViewById(R.id.til_relationship);
        tilPhone = (TextInputLayout) findViewById(R.id.til_phone);
        tilEmail = (TextInputLayout) findViewById(R.id.til_email);

        tvPatientNameLabel = (TextView) findViewById(R.id.tv_patient_name_label);
        tvRelationshipLabel = (TextView) findViewById(R.id.tv_relationship_label);
        tvPhoneLabel = (TextView) findViewById(R.id.tv_phone_label);
        tvEmailLabel = (TextView) findViewById(R.id.tv_email_label);

        choose_date = getIntent().getExtras().getString("date");
        editEmail = (EditText) findViewById(R.id.txtEmail);
        editFullname = (EditText) findViewById(R.id.txtFirstname);
        editPhone = (EditText) findViewById(R.id.txtPhone);
        etRelationship = (EditText) findViewById(R.id.et_relation);
        etReason = (EditText) findViewById(R.id.et_reason);
        editFullname.setFilters(CommonClass.getFilterWithoutSpecialChars(50));
        editFullname.setSelection(editFullname.getText().length());
        editPhone.setFilters(CommonClass.getFilterWithoutSpecialChars(15));
        etRelationship.setFilters(CommonClass.getFilterWithoutSpecialChars(30));
        etReason.setFilters(CommonClass.getFilterWithoutSpecialChars(150));

        editFullname.setText(common.getSession(ApiParams.USER_FULLNAME));
        editPhone.setText(common.getSession(ApiParams.USER_PHONE));
        editEmail.setText(common.getSession(ApiParams.USER_EMAIL));
        etReason.setText("");
        layoutPatient.setVisibility(View.GONE);
        btnConfirm.setEnabled(false);

        textInputFunction();

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        if(btnMe.isChecked()){
            btnConfirm.setBackground(getResources().getDrawable(R.drawable.rounded_button));
            btnConfirm.setEnabled(true);
            editFullname.setText(common.getSession(ApiParams.USER_FULLNAME));
            editPhone.setText(common.getSession(ApiParams.USER_PHONE));
            editEmail.setText(common.getSession(ApiParams.USER_EMAIL));
            etReason.setText("");
            layoutPatient.setVisibility(View.GONE);
        }

        btnMe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnConfirm.setBackground(getResources().getDrawable(R.drawable.rounded_button));
                btnConfirm.setEnabled(true);
                editFullname.setText(common.getSession(ApiParams.USER_FULLNAME));
                editPhone.setText(common.getSession(ApiParams.USER_PHONE));
                editEmail.setText(common.getSession(ApiParams.USER_EMAIL));
                etReason.setText("");
                layoutPatient.setVisibility(View.GONE);
            }
        });

        btnOthers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnConfirm.setBackground(getResources().getDrawable(R.drawable.rounded_button));
                btnConfirm.setEnabled(true);
                editFullname.setText("");
                etRelationship.setText("");
                editPhone.setText("");
                editEmail.setText("");
                etReason.setText("");
                layoutPatient.setVisibility(View.VISIBLE);
            }
        });

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!etReason.getText().toString().trim().isEmpty()){
                    final Dialog dialog = new Dialog(v.getContext());
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setCancelable(false);
                    dialog.setContentView(R.layout.dialog_confirm);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                    TextView text = (TextView) dialog.findViewById(R.id.dialog_title);
                    text.setText(getResources().getString(R.string.confirm_booking_label));

                    Button btnConfirm = (Button) dialog.findViewById(R.id.btn_logout_yes);
                    Button btnCancel = (Button) dialog.findViewById(R.id.btn_logout_no);
                    btnConfirm.setAllCaps(true);
                    btnCancel.setAllCaps(true);

                    btnConfirm.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(CommonClass.is_internet_connected(v.getContext())) {
                                register();
                                dialog.dismiss();
                            } else {
                                dialog.dismiss();
                                common.setToastMessage(getResources().getString(R.string.error_no_internet));
                            }
                        }
                    });

                    btnCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });

                    dialog.show();
                } else{
                    common.setToastMessage(getResources().getString(R.string.empty_field));
                }

            }
        });

    }

    public void textInputFunction(){
        editFullname.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(tilFullName.isErrorEnabled()){
                    tilFullName.setErrorEnabled(false);
                    tvPatientNameLabel.setTextColor(getResources().getColor(R.color.colorPrimary));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });

        etRelationship.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(tilRelationship.isErrorEnabled()){
                    tilRelationship.setErrorEnabled(false);
                    tvRelationshipLabel.setTextColor(getResources().getColor(R.color.colorPrimary));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });

        editPhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(tilPhone.isErrorEnabled()){
                    tilPhone.setErrorEnabled(false);
                    tvPhoneLabel.setTextColor(getResources().getColor(R.color.colorPrimary));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });

        editEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(tilEmail.isErrorEnabled()){
                    tilEmail.setErrorEnabled(false);
                    tvEmailLabel.setTextColor(getResources().getColor(R.color.colorPrimary));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });
    }

    // attempt book appointment with required validation
    public void register() {

        String email = editEmail.getText().toString();
        String fullname = editFullname.getText().toString();
        String relationship = etRelationship.getText().toString();
        String phone = editPhone.getText().toString();
        String reason = etReason.getText().toString();
        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(fullname)) {
            tvPatientNameLabel.setTextColor(getResources().getColor(R.color.colorRed));
            editFullname.getBackground().setColorFilter(getResources().getColor(R.color.colorRed), PorterDuff.Mode.SRC_ATOP);
            tilFullName.setError(getString(R.string.valid_required_fullname_patient));
            focusView = editFullname;
            cancel = true;
        }
        if (TextUtils.isEmpty(relationship)) {
            if(btnOthers.isChecked()){
                tvRelationshipLabel.setTextColor(getResources().getColor(R.color.colorRed));
                etRelationship.getBackground().setColorFilter(getResources().getColor(R.color.colorRed), PorterDuff.Mode.SRC_ATOP);
                tilRelationship.setError(getString(R.string.valid_required_relationship));
                focusView = etRelationship;
                cancel = true;
            }
        }

        if (TextUtils.isEmpty(phone)) {
            tvPhoneLabel.setTextColor(getResources().getColor(R.color.colorRed));
            editPhone.getBackground().setColorFilter(getResources().getColor(R.color.colorRed), PorterDuff.Mode.SRC_ATOP);
            tilPhone.setError(getString(R.string.valid_required_phone));
            focusView = editPhone;
            cancel = true;
        } else{
            if (!isValidCellPhone(phone)) {
                tvPhoneLabel.setTextColor(getResources().getColor(R.color.colorRed));
                editPhone.getBackground().setColorFilter(getResources().getColor(R.color.colorRed), PorterDuff.Mode.SRC_ATOP);
                tilPhone.setError("Invalid phone number");
                focusView = editPhone;
                cancel = true;
            }
        }

        if (TextUtils.isEmpty(email)) {
            tvEmailLabel.setTextColor(getResources().getColor(R.color.colorRed));
            editEmail.getBackground().setColorFilter(getResources().getColor(R.color.colorRed), PorterDuff.Mode.SRC_ATOP);
            tilEmail.setError(getString(R.string.valid_required_email));
            focusView = editEmail;
            cancel = true;
        } else {
            if (!isValidEmail(email)) {
                tvEmailLabel.setTextColor(getResources().getColor(R.color.colorRed));
                editEmail.getBackground().setColorFilter(getResources().getColor(R.color.colorRed), PorterDuff.Mode.SRC_ATOP);
                tilEmail.setError(getString(R.string.valid_email));
                focusView = editEmail;
                cancel = true;
            }
        }
        if (TextUtils.isEmpty(reason)) {
            etReason.setError(getString(R.string.valid_required_reason));
            focusView = etReason;
            cancel = true;
        }


        if (cancel) {
            if (focusView != null)
                focusView.requestFocus();
        } else {

            HashMap<String, String> map = new HashMap<>();
            map.put("doct_id", ActiveModels.DOCTOR_MODEL.getDoct_id());
            map.put("bus_id", ActiveModels.BUSINESS_MODEL.getBus_id());
            map.put("service_id", String.valueOf(getIntent().getExtras().getInt("serviceId")));
            map.put("user_fullname", fullname);
            map.put("user_phone", phone);
            map.put("user_email", email);
            map.put("start_time", ActiveModels.SELECTED_SLOT.getSlot());
            map.put("time_token", String.valueOf(ActiveModels.SELECTED_SLOT.getTime_token()));
            map.put("appointment_date", choose_date);
            map.put("user_id", common.get_user_id());
            map.put("relationship_to_patient", relationship);
            map.put("checkup_reason", reason);

//            String str_services = "";
//            for (int i = 0; i < ActiveModels.LIST_SERVICES_MODEL.size(); i++) {
//                if (ActiveModels.LIST_SERVICES_MODEL.get(i).isChecked()) {
//                    if (str_services.equalsIgnoreCase("")) {
//                        str_services = ActiveModels.LIST_SERVICES_MODEL.get(i).getId();
//                    } else {
//                        str_services = str_services + "," + ActiveModels.LIST_SERVICES_MODEL.get(i).getId();
//                    }
//                }
//            }


            if (ConstValue.enable_paypal) {
                // this class for handle request response thread and return response data
                VJsonRequest vJsonRequest = new VJsonRequest(this, ApiParams.BOOKAPPOINTMENT_TEMP_URL, map,
                        new VJsonRequest.VJsonResponce() {
                            @Override
                            public void VResponce(String responce) {
                                Intent intent = new Intent(ActivityPersonInfo.this, PaymentActivity.class);
                                intent.putExtra("order_details", responce);
                                startActivity(intent);
                            }

                            @Override
                            public void VError(String responce) {
                                common.setToastMessage(responce);
                            }
                        });
            } else {
                // this class for handle request response thread and return response data
                VJsonRequest vJsonRequest = new VJsonRequest(this, ApiParams.BOOKAPPOINTMENT_URL, map,
                        new VJsonRequest.VJsonResponce() {
                            @Override
                            public void VResponce(String responce) {

                                JSONObject appointmentData = null;
                                try {
                                    appointmentData = new JSONObject(responce);


                                    String messageType = getString(R.string.appoitnment_confirm_message_part1) + appointmentData.getString("id") + " " + getString(R.string.appoitnment_confirm_message_part1);
//                                    common.setToastMessage(getString(R.string.appoitnment_confirm_message_part1) + appointmentData.getString("id") + " " + getString(R.string.appoitnment_confirm_message_part1));
                                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                                    SimpleDateFormat formatter2 = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());
                                    try {
                                        Date testDate = formatter.parse(choose_date);
                                        Date testTime = formatter2.parse(ActiveModels.SELECTED_SLOT.getSlot());
                                        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);


                                        Calendar myAlarmDate = Calendar.getInstance();
                                        myAlarmDate.setTimeInMillis(System.currentTimeMillis());
                                        myAlarmDate.set(testDate.getYear(), testDate.getMonth(), testDate.getDate(), testTime.getHours(), testTime.getMinutes(), 0);

                                        Intent _myIntent = new Intent(ActivityPersonInfo.this, NotifyService.class);
                                        _myIntent.putExtra("MyMessage", messageType);
                                        PendingIntent _myPendingIntent = PendingIntent.getBroadcast(ActivityPersonInfo.this, 123, _myIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                                        alarmManager.set(AlarmManager.RTC_WAKEUP, myAlarmDate.getTimeInMillis(), _myPendingIntent);
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }


                                    Intent intent = null;

                                    intent = new Intent(ActivityPersonInfo.this, ActivityThanks.class);
                                    intent.putExtra("date", getIntent().getExtras().getString("date"));
                                    intent.putExtra("timeslot", ActiveModels.SELECTED_SLOT.getSlot());
                                    intent.putExtra("doctor", getIntent().getExtras().getString("doctor"));
                                    intent.putExtra("ampm", getIntent().getExtras().getString("ampm"));
                                    intent.putExtra("clinic", getIntent().getExtras().getString("clinic"));
                                    intent.putExtra("clinicLoc", getIntent().getExtras().getString("clinicLoc"));

                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                    finish();

                                    Log.d("jbeeBooking", "date" + " == " + getIntent().getExtras().getString("date"));
                                    Log.d("jbeeBooking", "timeslot" + " == " + ActiveModels.SELECTED_SLOT.getSlot());
                                    Log.d("jbeeBooking", "doctor" + " == " + getIntent().getExtras().getString("doctor"));
                                    Log.d("jbeeBooking", "ampm" + " == " + getIntent().getExtras().getString("ampm"));
                                    Log.d("jbeeBooking", "clinic" + " == " + getIntent().getExtras().getString("clinic"));
                                    Log.d("jbeeBooking", "clinicLoc" + " == " + getIntent().getExtras().getString("clinicLoc"));

                                    Log.d("jbeeBooking", responce);
                                } catch (JSONException e) {
                                    e.printStackTrace();

                                    Log.d("jbeeBooking", e.toString());
                                }
                            }

                            @Override
                            public void VError(String responce) {
                                common.setToastMessage(responce);
                            }
                        });
            }
        }

    }
}
