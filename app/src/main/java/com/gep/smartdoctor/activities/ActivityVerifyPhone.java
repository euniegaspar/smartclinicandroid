package com.gep.smartdoctor.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;

import android.text.Editable;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gep.smartdoctor.Config.ApiParams;
import com.gep.smartdoctor.R;
import com.gep.smartdoctor.util.VJsonRequest;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthOptions;
import com.google.firebase.auth.PhoneAuthProvider;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ActivityVerifyPhone extends CommonActivity {

    String verificationCodeBySystem;
    TextView tvLabelPhonePrompt, tvRequestCode;
    RelativeLayout btnBack;
    private EditText editText1, editText2, editText3, editText4, editText5, editText6;
    private EditText[] editTexts;
    private Button btnVerify;
    private ProgressBar progressBar;
    private FirebaseAuth mAuth;

    String fullname, phone, email, password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_phone);
        mAuth = FirebaseAuth.getInstance();

        btnBack = (RelativeLayout) findViewById(R.id.btn_back);
        fullname = getIntent().getExtras().getString("user_fullname");
        phone = getIntent().getExtras().getString("user_phone");
        email = getIntent().getExtras().getString("user_email");
        password = getIntent().getExtras().getString("user_password");
        tvLabelPhonePrompt = (TextView) findViewById(R.id.label_prompt);
        tvRequestCode = (TextView) findViewById(R.id.request_code);
        btnVerify = (Button) findViewById(R.id.btn_verify);
        progressBar = (ProgressBar) findViewById(R.id.progressbar);
        tvLabelPhonePrompt.setText(getResources().getString(R.string.digit_prompt) + " \n" + phone);
        otpPattern();
        retryCode(getResources().getString(R.string.request_code));

//        Log.d("testreg123","phone no "+phone);

        sendVerificationCode(phone);

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                finish();
            }
        });

        btnVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String code = editText1.getText().toString() +
                        editText2.getText().toString() +
                        editText3.getText().toString() +
                        editText4.getText().toString() +
                        editText5.getText().toString() +
                        editText6.getText().toString();

                if (code.isEmpty() || code.length() < 6) {
                    common.setToastMessage("Wrong OTP...");
                    return;
                }
                progressBar.setVisibility(View.VISIBLE);

                try{
                    InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                    inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                } catch (Exception e){}
                verifyCode(code);
            }
        });
    }

    private void sendVerificationCode(String phoneNo){

        PhoneAuthOptions options =
                PhoneAuthOptions.newBuilder(mAuth)
                        .setPhoneNumber(phoneNo)       // Phone number to verify
                        .setTimeout(60L, TimeUnit.SECONDS) // Timeout and unit
                        .setActivity(this)                 // Activity (for callback binding)
                        .setCallbacks(mCallbacks)          // OnVerificationStateChangedCallbacks
                        .build();
        PhoneAuthProvider.verifyPhoneNumber(options);
    }

    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        @Override
        public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            super.onCodeSent(s, forceResendingToken);
            verificationCodeBySystem = s;
        }

        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {

            String code = phoneAuthCredential.getSmsCode();
            if(code != null){
                progressBar.setVisibility(View.VISIBLE);
                verifyCode(code);
            }
        }

        @Override
        public void onVerificationFailed(FirebaseException e) {
            common.setToastMessage(e.getMessage());
        }
    };

    private void verifyCode(String codeByUser){
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationCodeBySystem, codeByUser);
        signInTheUserByCredentials(credential);
    }

    private void signInTheUserByCredentials(PhoneAuthCredential credential){
        FirebaseAuth fireBaseAuth = FirebaseAuth.getInstance();

        fireBaseAuth.signInWithCredential(credential)
                .addOnCompleteListener(ActivityVerifyPhone.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            progressBar.setVisibility(View.GONE);
                            registerVerifiedNumber();

                        } else{
                            common.setToastMessage("Wrong OTP..");
                            progressBar.setVisibility(View.GONE);
                        }
                    }
                });
    }

    private void registerVerifiedNumber(){
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }

        if(getIntent().getExtras().getString("registration").equals("true")) {


            HashMap<String, String> params = new HashMap<>();
            params.put("user_fullname", fullname);
            params.put("user_phone", phone);
            params.put("user_email", email);
            params.put("user_password", password);

            VJsonRequest vJsonRequest = new VJsonRequest(this, ApiParams.REGISTER_URL, params,
                    new VJsonRequest.VJsonResponce() {
                        @Override
                        public void VResponce(String responce) {
                            JSONObject userdata = null;
                            try {
                                userdata = new JSONObject(responce);

                                Intent intent = new Intent(getApplicationContext(), ActivityMain.class);
                                common.setSession(ApiParams.COMMON_KEY, userdata.getString("user_id"));
                                common.setSession(ApiParams.USER_FULLNAME, userdata.getString("user_fullname"));
                                common.setSession(ApiParams.USER_EMAIL, userdata.getString("user_email"));
                                common.setSession(ApiParams.USER_PHONE, userdata.getString("user_phone"));

                                if (getCurrentFocus() != null) {
                                    InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                                    inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                                }

                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                finish();

//                                Log.d("testreg123","it went here registered "+responce);
                                ActivityMain.tabPosition = 1;
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void VError(String responce) {
//                            Log.d("testreg123","it went here error "+responce);
                            common.setToastMessage(getResources().getString(R.string.email_already_registered));
                        }
                    });
        } else{
            HashMap<String, String> params = new HashMap<>();
            params.put("user_fullname", getIntent().getExtras().getString("user_fullname"));
            params.put("user_phone", getIntent().getExtras().getString("user_phone"));
            params.put("user_id", common.get_user_id());

            // this class for handle request response thread and return response data
            VJsonRequest vJsonRequest = new VJsonRequest(this, ApiParams.UPDATEPROFILE_URL, params,
                    new VJsonRequest.VJsonResponce() {
                        @Override
                        public void VResponce(String responce) {

                            JSONObject userdata = null;
                            try {
                                userdata = new JSONObject(responce);

                                common.setSession(ApiParams.COMMON_KEY, userdata.getString("user_id"));
                                common.setSession(ApiParams.USER_FULLNAME, userdata.getString("user_fullname"));
                                common.setSession(ApiParams.USER_EMAIL, userdata.getString("user_email"));
                                common.setSession(ApiParams.USER_PHONE, userdata.getString("user_phone"));

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void VError(String responce) {
                            common.setToastMessage(responce);
                        }
                    });

            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if(getIntent().getExtras().getString("isChangePass").equals("true")){
                        changePassword();
                    } else{
                        Intent intent = new Intent(getApplicationContext(), ActivityMain.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        finish();
                    }
                }
            }, 3000);
        }
    }

    private void changePassword() {
        HashMap<String, String> params2 = new HashMap<>();
        params2.put("user_id", common.get_user_id());
        params2.put("c_password", getIntent().getExtras().getString("c_password"));
        params2.put("n_password", getIntent().getExtras().getString("n_password"));
        params2.put("r_password", getIntent().getExtras().getString("r_password"));

        // this class for handle request response thread and return response data PASSWORD
        VJsonRequest vJsonRequestPassword = new VJsonRequest(this, ApiParams.CHANGE_PASSWORD_URL, params2,
                new VJsonRequest.VJsonResponce() {
                    @Override
                    public void VResponce(String responce) {
                        common.setToastMessage(responce);

                        Intent intent = new Intent(getApplicationContext(), ActivityMain.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        finish();
                    }

                    @Override
                    public void VError(String responce) {
                        Intent intent = new Intent(getApplicationContext(), ActivityMain.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        finish();
                    }
                });
    }

    private void otpPattern(){
        editText1 = (EditText) findViewById(R.id.et1);
        editText2 = (EditText) findViewById(R.id.et2);
        editText3 = (EditText) findViewById(R.id.et3);
        editText4 = (EditText) findViewById(R.id.et4);
        editText5 = (EditText) findViewById(R.id.et5);
        editText6 = (EditText) findViewById(R.id.et6);
        editTexts = new EditText[]{editText1, editText2, editText3, editText4, editText5, editText6};
        editText1.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);

        editText1.addTextChangedListener(new PinTextWatcher(0));
        editText2.addTextChangedListener(new PinTextWatcher(1));
        editText3.addTextChangedListener(new PinTextWatcher(2));
        editText4.addTextChangedListener(new PinTextWatcher(3));
        editText5.addTextChangedListener(new PinTextWatcher(4));
        editText6.addTextChangedListener(new PinTextWatcher(5));

        editText1.setOnKeyListener(new PinOnKeyListener(0));
        editText2.setOnKeyListener(new PinOnKeyListener(1));
        editText3.setOnKeyListener(new PinOnKeyListener(2));
        editText4.setOnKeyListener(new PinOnKeyListener(3));
        editText5.setOnKeyListener(new PinOnKeyListener(4));
        editText6.setOnKeyListener(new PinOnKeyListener(5));
    }

    private void retryCode(String message){
        tvRequestCode.setText(Html.fromHtml(message));

        //Span "terms"
        Pattern word = Pattern.compile("Request new code");
        Matcher match = word.matcher(tvRequestCode.getText().toString());
        int startIndexterms = 0;
        int endIndexterms = 0;

        while (match.find()) {
            startIndexterms = match.start();
            endIndexterms = match.end();
        }


        SpannableString ss = new SpannableString(message);

        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void updateDrawState(TextPaint ds) {
                ds.setUnderlineText(true);
            }

            @Override
            public void onClick(View widget) {
                common.setToastMessage("Requesting new code...");

                sendVerificationCode(phone);
            }
        };

        ss.setSpan(clickableSpan, startIndexterms, endIndexterms, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        tvRequestCode.setText(ss);
        tvRequestCode.setMovementMethod(LinkMovementMethod.getInstance());
    }

    public class PinTextWatcher implements TextWatcher {

        private int currentIndex;
        private boolean isFirst = false, isLast = false;
        private String newTypedString = "";

        PinTextWatcher(int currentIndex) {
            this.currentIndex = currentIndex;

            if (currentIndex == 0)
                this.isFirst = true;
            else if (currentIndex == editTexts.length - 1)
                this.isLast = true;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            newTypedString = s.subSequence(start, start + count).toString().trim();
        }

        @Override
        public void afterTextChanged(Editable s) {

            String text = newTypedString;

            /* Detect paste event and set first char */
            if (text.length() > 1)
                text = String.valueOf(text.charAt(0)); // TODO: We can fill out other EditTexts

            editTexts[currentIndex].removeTextChangedListener(this);
            editTexts[currentIndex].setText(text);
            editTexts[currentIndex].setSelection(text.length());
            editTexts[currentIndex].addTextChangedListener(this);

            if (text.length() == 1)
                moveToNext();
            else if (text.length() == 0)
                moveToPrevious();
        }

        private void moveToNext() {
            if (!isLast)
                editTexts[currentIndex + 1].requestFocus();

            if (isAllEditTextsFilled() && isLast) { // isLast is optional
                editTexts[currentIndex].clearFocus();
                common.hideKeyboard(ActivityVerifyPhone.this);
            }
        }

        private void moveToPrevious() {
            if (!isFirst)
                editTexts[currentIndex - 1].requestFocus();
        }

        private boolean isAllEditTextsFilled() {
            for (EditText editText : editTexts)
                if (editText.getText().toString().trim().length() == 0)
                    return false;
            return true;
        }
    }

    public class PinOnKeyListener implements View.OnKeyListener {

        private int currentIndex;

        PinOnKeyListener(int currentIndex) {
            this.currentIndex = currentIndex;
        }

        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {
            if (keyCode == KeyEvent.KEYCODE_DEL && event.getAction() == KeyEvent.ACTION_DOWN) {
                if (editTexts[currentIndex].getText().toString().isEmpty() && currentIndex != 0)
                    editTexts[currentIndex - 1].requestFocus();
            }
            return false;
        }

    }

    public void onDestroy() {
        super.onDestroy();
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }
}