package com.gep.smartdoctor.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.gep.smartdoctor.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.gep.smartdoctor.Config.ApiParams;
import com.gep.smartdoctor.dialogues.LoginDialog;
import com.gep.smartdoctor.util.CommonClass;
import com.gep.smartdoctor.util.VJsonRequest;
import com.google.android.material.textfield.TextInputLayout;

public class ActivityLogin extends CommonActivity {

    CommonClass common;
    private EditText editEmail, editPassword;
    private TextInputLayout tilEmail, tilPassword;
    private TextView btnForgotPassword;
    RelativeLayout btnBack;
    Button btnLogin;
    TextView tvRegister, tvErrorEmail, tvErrorPass;

    private boolean isUpdateSuccess = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        common = new CommonClass(this);

        tvRegister = (TextView) findViewById(R.id.tv_register);
        tvErrorEmail = (TextView) findViewById(R.id.error_email_label);
        tvErrorPass = (TextView) findViewById(R.id.error_pass_label);
        tilEmail = (TextInputLayout) findViewById(R.id.til_email);
        tilPassword = (TextInputLayout) findViewById(R.id.til_password);
        editEmail = (EditText) findViewById(R.id.et_email);
        editEmail.setFilters(CommonClass.getFilter());
        editPassword = (EditText) findViewById(R.id.et_password);
        editPassword.setFilters(CommonClass.getFilter());
        btnForgotPassword = (TextView) findViewById(R.id.btnForgetPassword);
        btnBack = (RelativeLayout) findViewById(R.id.btn_back);
        btnLogin = (Button) findViewById(R.id.btn_login);
        checkInput();
        labelSpannable();

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                ActivityMain.tabPosition = 1;
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(CommonClass.is_internet_connected(getApplicationContext())){ 
                    if (getCurrentFocus() != null) {
                        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                        inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                    }

                    login();
                } else{
                    Toast.makeText(getApplicationContext(), R.string.error_no_internet, Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ActivityForgotPassword.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
        ActivityMain.tabPosition = 1;
    }

    private void labelSpannable(){
        String registerstring = getResources().getString(R.string.dont_have_acct) + " " +
                getResources().getString(R.string.sign_up) + "";
        tvRegister.setText(Html.fromHtml(registerstring));

        //Span "signup"
        Pattern word = Pattern.compile(getResources().getString(R.string.sign_up));
        Matcher match = word.matcher(tvRegister.getText().toString());
        int startIndexterms = 0;
        int endIndexterms = 0;

        while (match.find()) {
            startIndexterms = match.start();
            endIndexterms = match.end();
        }

        SpannableString ss = new SpannableString(registerstring);

        ClickableSpan clickableSpan1 = new ClickableSpan() {
            @Override
            public void updateDrawState(TextPaint ds) {
                ds.setFakeBoldText(false);
                ds.setUnderlineText(true);    // this remove the underline
            }

            @Override
            public void onClick(View widget) {
                registerClick();
            }
        };
        ss.setSpan(clickableSpan1, startIndexterms, endIndexterms, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        tvRegister.setText(ss);
        tvRegister.setMovementMethod(LinkMovementMethod.getInstance());
    }

    public void checkInput(){
        editEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tvErrorEmail.setVisibility(View.GONE);

                if(tilEmail.isErrorEnabled()){
                    tilEmail.setErrorEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });

        editPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tvErrorPass.setVisibility(View.GONE);
                if(tilPassword.isErrorEnabled()){
                    tilPassword.setErrorEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });
    }

    public void login() {
//        btnLogin.setEnabled(false);

        String email = editEmail.getText().toString();
        String password = editPassword.getText().toString();
        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(email)) {
            tvErrorEmail.setText(getString(R.string.valid_required_email));
            tvErrorEmail.setVisibility(View.VISIBLE);
            tilEmail.setError(null);
            focusView = editEmail;
            cancel = true;
        } else {
            if (!isValidEmail(email)) {
                tvErrorEmail.setText(getString(R.string.valid_email));
                tvErrorEmail.setVisibility(View.VISIBLE);
                tilEmail.setError(null);
                focusView = editEmail;
                cancel = true;
            }
        }
        if (TextUtils.isEmpty(password)) {
            tvErrorPass.setText(getString(R.string.valid_required_password));
            tvErrorPass.setVisibility(View.VISIBLE);
            tilPassword.setError(null);
            focusView = editPassword;
            cancel = true;
        }


        if (cancel) {
            if (focusView != null)
                focusView.requestFocus();
        } else {
            HashMap<String, String> params = new HashMap<>();

            params.put("user_email", email);
            params.put("user_password", password);

            VJsonRequest vJsonRequest = new VJsonRequest(this, ApiParams.LOGIN_URL, params,
                    new VJsonRequest.VJsonResponce() {
                        @Override
                        public void VResponce(String responce) {

                            JSONObject userdata = null;
                            try {
                                userdata = new JSONObject(responce);

                                common.setSession(ApiParams.COMMON_KEY, userdata.getString("user_id"));
                                common.setSession(ApiParams.USER_FULLNAME, userdata.getString("user_fullname"));
                                common.setSession(ApiParams.USER_EMAIL, userdata.getString("user_email"));
                                common.setSession(ApiParams.USER_PHONE, userdata.getString("user_phone"));

//                                isUpdateSuccess = true;
                            } catch (JSONException e) {
                                e.printStackTrace();
//                                isUpdateSuccess = false;
                            }

                            isUpdateSuccess = true;
                        }

                        @Override
                        public void VError(String responce) {
                            btnLogin.setEnabled(true);
                            isUpdateSuccess = false;

                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ActivityLogin.this);
                            alertDialogBuilder.setMessage(responce);
                            alertDialogBuilder.setPositiveButton("Okay",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface arg0, int arg1) {
//                                            Toast.makeText(getApplicationContext(),R.string.invalid_email_and_password,Toast.LENGTH_LONG).show();
                                        }
                                    });

                            AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();
                        }
                    });

            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (ActivityMain.tabPosition == 2) {
                        Intent intent = new Intent(getApplicationContext(), ActivityMain.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                        if(isUpdateSuccess) {
                            startActivity(intent);
                            finish();
                        }
                    } else {
                        if(isUpdateSuccess) {
                            finish();
                        }
                    }
                }
            }, 1500);
        }

    }

    // return true if email is valid otherwise return false
    public final static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    // register button click event
    public void registerClick() {
        Intent intent = new Intent(ActivityLogin.this, RegisterActivity.class);
        startActivity(intent);
//        finish();
    }

}
