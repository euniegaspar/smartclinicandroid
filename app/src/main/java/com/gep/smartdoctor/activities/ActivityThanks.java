package com.gep.smartdoctor.activities;

import android.content.Intent;
import android.os.Bundle;
import androidx.core.app.TaskStackBuilder;
import android.view.View;
import android.widget.TextView;

import com.gep.smartdoctor.R;

import com.gep.smartdoctor.models.ActiveModels;
import com.gep.smartdoctor.models.BusinessModel;
import com.gep.smartdoctor.util.CommonClass;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ActivityThanks extends CommonActivity {

    private BusinessModel selected_business;
    private TextView tvApptTime, tvDoctorName, tvApptDate, tvClinicName, tvClinicLocation;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thanks);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        selected_business = ActiveModels.BUSINESS_MODEL;

        tvApptTime = (TextView) findViewById(R.id.tv_appt_time);
        tvDoctorName = (TextView) findViewById(R.id.tv_doctor_name);
        tvApptDate = (TextView) findViewById(R.id.tv_appt_date);
        tvClinicName = (TextView) findViewById(R.id.tv_clinic_name);
        tvClinicLocation = (TextView) findViewById(R.id.tv_clinic_location);

        String date = getIntent().getExtras().getString("date");
        String time = getIntent().getExtras().getString("timeslot");
        String ampm = getIntent().getExtras().getString("ampm");
        String clinicName = getIntent().getExtras().getString("clinic");
        String clinicLoc = getIntent().getExtras().getString("clinicLoc");

        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss");
        Date dt = null;
        try {
            dt = sdf.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        SimpleDateFormat sdfs = new SimpleDateFormat("hh:mm");
        String formatedTime = sdfs.format(dt);

        tvApptTime.setText(formatedTime + " " + ampm);
        tvDoctorName.setText(getIntent().getExtras().getString("doctor"));
//        String apptdate = weekday + ", " + month + " " + day + ", " + year;
        tvApptDate.setText(CommonClass.getDateText(date));
        tvClinicName.setText(clinicName);
        tvClinicLocation.setText(clinicLoc);

    }

    public void orderFinish(View view) {
        ActiveModels.reset();
        Intent intent = new Intent(ActivityThanks.this, ActivityMain.class);
        startActivity(intent);
        finish();
    }

    public void myOrderActivity(View view) {
        ActivityMain.fromAppointment = true;
        ActiveModels.reset();

//        ActivityMain.tabPosition = 0;
//        ActiveModels.reset();
//        TaskStackBuilder tsb = TaskStackBuilder.from(this);
//        tsb.addParentStack(this);
//        tsb.addNextIntent(new Intent(this, ActivityMain.class));
//        tsb.startActivities();

        //new
        Intent intentSurvey = new Intent(this, ActivityMain.class);
        startActivity(intentSurvey);
    }

}
