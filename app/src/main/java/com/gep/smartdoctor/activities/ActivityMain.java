package com.gep.smartdoctor.activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.core.view.GravityCompat;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;


import com.gep.smartdoctor.R;
import com.gep.smartdoctor.dialogues.LoginDialog;
import com.gep.smartdoctor.fragments.FragmentAppointment;
import com.gep.smartdoctor.fragments.FragmentHome;
import com.gep.smartdoctor.fragments.FragmentProfile;
import com.gep.smartdoctor.fragments.FragmentRecordsHistory;
import com.gep.smartdoctor.util.CommonClass;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.HashMap;

import com.gep.smartdoctor.Config.ApiParams;
import com.gep.smartdoctor.configfcm.MyFirebaseRegister;
import com.gep.smartdoctor.util.VJsonRequest;

import org.json.JSONException;
import org.json.JSONObject;

public class ActivityMain extends CommonActivity implements NavigationView.OnNavigationItemSelectedListener {

    private Toolbar toolbar;
    private SharedPreferences sharedPreferences;

    public DrawerLayout drawer;
    private SampleFragmentPagerAdapter mSampleFragmentManager;
    public ViewPager mViewPager;
    public TabLayout tabLayout;
    public static ArrayList<String> hospitalLists;
//    public boolean isFirstLoad = true;
    public static boolean fromNotification = false;
    public static boolean fromAppointment = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_layout);
        setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        mSampleFragmentManager = new SampleFragmentPagerAdapter(getSupportFragmentManager());
        mViewPager = (ViewPager) findViewById(R.id.fragment_container);
        mViewPager.setOffscreenPageLimit(3);
        mViewPager.setAdapter(mSampleFragmentManager);
        mViewPager.setOnPageChangeListener(
                new ViewPager.SimpleOnPageChangeListener() {
                    @Override
                    public void onPageSelected(int position) {
                        if(mViewPager.getCurrentItem() == 0 || mViewPager.getCurrentItem() == 2){
                            if (!common.is_user_login()){
                                LoginDialog loginDialog = new LoginDialog(ActivityMain.this);
                                loginDialog.show();
                            }
                        }
                        ActivityMain.tabPosition = mViewPager.getCurrentItem();
                        Log.d("test123", "SimpleOnPageChangeListener");
//                        if(!isFirstLoad) {
//                            refreshFragments();
//                        }
                    }
                });
        tabLayout = (TabLayout) findViewById(R.id.bottom_navigation);
        tabLayout.setupWithViewPager(mViewPager);
        setupTabIcons();

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        if (!common.getSessionBool("fcm_registered") && common.is_user_login()) {
            MyFirebaseRegister fireReg = new MyFirebaseRegister(this);
            fireReg.RegisterUser(common.get_user_id());
        }
        hospitalLists = new ArrayList<>();

        initKeyBoardListener();

        if(fromNotification){
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    fromNotification = false;
                    tabPosition = 0;
                    mViewPager.setCurrentItem(tabPosition);
                }
            }, 500);
        }

    }

    public void refreshFragments(){
        mViewPager.setAdapter(mSampleFragmentManager);
    }

    private void initKeyBoardListener() {
        // Threshold for minimal keyboard height.
        final int MIN_KEYBOARD_HEIGHT_PX = 150;
        final View decorView = getWindow().getDecorView();
        decorView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            private final Rect windowVisibleDisplayFrame = new Rect();
            private int lastVisibleDecorViewHeight;

            @Override
            public void onGlobalLayout() {
                decorView.getWindowVisibleDisplayFrame(windowVisibleDisplayFrame);
                final int visibleDecorViewHeight = windowVisibleDisplayFrame.height();

                if (lastVisibleDecorViewHeight != 0) {
                    if (lastVisibleDecorViewHeight > visibleDecorViewHeight + MIN_KEYBOARD_HEIGHT_PX) {
                       tabLayout.setVisibility(View.GONE);
                    } else if (lastVisibleDecorViewHeight + MIN_KEYBOARD_HEIGHT_PX < visibleDecorViewHeight) {
                        tabLayout.setVisibility(View.VISIBLE);
                    }
                }
                lastVisibleDecorViewHeight = visibleDecorViewHeight;
            }
        });
    }

    private void setupTabIcons() {
        tabLayout.getTabAt(0).setIcon(R.drawable.appointment_selector);
        tabLayout.getTabAt(1).setIcon(R.drawable.home_selector);
        tabLayout.getTabAt(2).setIcon(R.drawable.profile_selector);
    }

    public void switchToHome() {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mViewPager.setCurrentItem(1);
            }
        }, 200);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_language, menu);
        HashMap<String,Object> param=new HashMap<>();


        super.onCreateContextMenu(menu, v, menuInfo);
    }

    public void bindView() {
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close){

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };
        drawer.addDrawerListener(toggle);
        toggle.syncState();


    }

    View navHeader;
    public static int tabPosition = 1;
    @Override
    protected void onResume() {
        mViewPager.setCurrentItem(tabPosition);
        bindView();

        //        if(fromNotification){
        if(fromAppointment){
            final Handler tabHandler = new Handler();
            tabHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    fromAppointment = false;
                    tabPosition = 0;
                    mViewPager.setCurrentItem(tabPosition);
                }
            }, 500);
        }
        //    }

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navHeader = navigationView.getHeaderView(0);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setItemIconTintList(null);

        Menu nav_Menu = navigationView.getMenu();
        if (!common.is_user_login()) {
            nav_Menu.findItem(R.id.nav_appointment).setVisible(false);
            nav_Menu.findItem(R.id.nav_logout).setVisible(false);
            nav_Menu.findItem(R.id.nav_profile).setVisible(false);
            nav_Menu.findItem(R.id.nav_login).setVisible(true);
            navHeader.findViewById(R.id.txtFullName).setVisibility(View.VISIBLE);
        } else {
            nav_Menu.findItem(R.id.nav_appointment).setVisible(false);
            nav_Menu.findItem(R.id.nav_logout).setVisible(true);
            nav_Menu.findItem(R.id.nav_profile).setVisible(false);
            nav_Menu.findItem(R.id.nav_login).setVisible(false);
            navHeader.findViewById(R.id.txtFullName).setVisibility(View.VISIBLE);

            HashMap<String, String> params = new HashMap<>();
            params.put("user_id", common.get_user_id());

            // this class for handle request response thread and return response data
            VJsonRequest vJsonRequest = new VJsonRequest(this, ApiParams.USERDATA_URL, params,
                    new VJsonRequest.VJsonResponce() {
                        @Override
                        public void VResponce(String response) {
                            JSONObject userdata = null;
                            try {
                                userdata = new JSONObject(response);

                                ((TextView) navHeader.findViewById(R.id.txtFullName)).setText(userdata.getString("user_fullname"));
                                ((TextView) navHeader.findViewById(R.id.textEmailId)).setText(userdata.getString("user_email"));


                                common.setSession(ApiParams.USER_FULLNAME, userdata.getString("user_fullname"));
                                common.setSession(ApiParams.USER_EMAIL, userdata.getString("user_email"));
                                common.setSession(ApiParams.USER_PHONE, userdata.getString("user_phone"));

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void VError(String response) {
                            common.setToastMessage(response);
                        }
                    });
        }

        super.onResume();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    AlertDialog.Builder alertDialog;
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.

        int id = item.getItemId();

        if (id == R.id.nav_settings) { //EXITS APP AFTER BOOKING
            Intent intent = new Intent(ActivityMain.this, ActivitySettings.class);
            startActivity(intent);
            tabPosition = 1;
        }
        else if (id == R.id.nav_logout) {
            final Dialog alertDialog = new Dialog(this);
            alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            alertDialog.setCancelable(false);
            alertDialog.setContentView(R.layout.dialog_logout);

            final TextView tvDialog = alertDialog.findViewById(R.id.dialog_title);   // access to text view of custom layout
            tvDialog.setText(getResources().getString(R.string.logout_title));
            Button btnYes = alertDialog.findViewById(R.id.btn_logout_yes);
            Button btnNo = alertDialog.findViewById(R.id.btn_logout_no);

            btnYes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(CommonClass.is_internet_connected(getApplicationContext())){
                        common.logOut();
                        tabPosition = 1;
                    } else {
                        common.setToastMessage(getResources().getString(R.string.error_no_internet));
                        alertDialog.dismiss();
                    }
                }
            });

            btnNo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                }
            });

            alertDialog.show();
        } else if (id == R.id.nav_login) {
            Intent intent = new Intent(ActivityMain.this, ActivityLogin.class);
            startActivity(intent);
        } else if (id == R.id.nav_share) {
            shareApp();
            tabPosition = mViewPager.getCurrentItem();
        } else if (id == R.id.nav_rating) {
            reviewOnApp();
            tabPosition = mViewPager.getCurrentItem();
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void shareApp() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, "Hi friends i am using ." + " http://play.google.com/store/apps/details?id=" + getPackageName() + " APP");
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }

    public void reviewOnApp() {
        Uri uri = Uri.parse("market://details?id=" + getPackageName());
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        // To count with Play market backstack, After pressing back button,
        // to taken back to our application, we need to add following flags to intent.
        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        try {
            startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=" + getPackageName())));
        }
    }

    // bind fragment in view pager
    public class SampleFragmentPagerAdapter extends FragmentPagerAdapter {
        final int PAGE_COUNT = 3;
        private String tabTitles[] = new String[]{getString(R.string.appointment), getString(R.string.home_label), getString(R.string.prof_label)};

        public SampleFragmentPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return PAGE_COUNT;
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment = null;
            if (position == 0) {
                tabPosition = 0;
                fragment = new FragmentAppointment();
            } else if (position == 1) {
                tabPosition = 1;
                fragment = new FragmentHome();
            } else {
                tabPosition = 2;
                fragment = new FragmentProfile();
            }

            return fragment;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return tabTitles[position];
        }

        @Override
        public int getItemPosition(Object object) {

            // Returning POSITION_NONE means the current data does not matches the data this fragment is showing right now.  Returning POSITION_NONE constant will force the fragment to redraw its view layout all over again and show new data.
            return POSITION_NONE;
        }
    }

    @Override
    public void onPause(){
        super.onPause();

        tabPosition = mViewPager.getCurrentItem();
    }

    ProgressDialog pDialog;
    @Override
    public void onDestroy() {
        super.onDestroy();
        tabPosition = 1;

        if(pDialog!=null){
            if(pDialog.isShowing()){
                pDialog.dismiss();
            }
        } else{
            pDialog = new ProgressDialog(this);
            if(pDialog.isShowing()){
                pDialog.dismiss();
            }
        }
    }
}
