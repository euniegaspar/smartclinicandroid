package com.gep.smartdoctor.activities;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.DecelerateInterpolator;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.gep.smartdoctor.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.gep.smartdoctor.Config.ApiParams;
import com.gep.smartdoctor.util.CommonClass;
import com.gep.smartdoctor.util.ContextWrapper;

public abstract class CommonActivity extends AppCompatActivity {

    public CommonClass common;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        common = new CommonClass(this);

        super.onCreate(savedInstanceState);
    }

    // set back arrow on actionbar and back event
    public void allowBack() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);

//            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorTextWhite)));
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.back_small);

        }
    }

    // set custom header title in actionbar
    public void setHeaderCenterTitle(String title, boolean allcaps) {

        ActionBar mActionBar = getSupportActionBar();
        View viewActionBar = getLayoutInflater().inflate(R.layout.custom_header_bar, null);
        ActionBar.LayoutParams params = new ActionBar.LayoutParams(//Center the textview in the ActionBar !
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.MATCH_PARENT,
                Gravity.CENTER);

        TextView mTitleTextView = (TextView) viewActionBar.findViewById(R.id.action_bar_title);
        mTitleTextView.setText(title);
        mTitleTextView.setAllCaps(allcaps);
        mActionBar.setCustomView(viewActionBar, params);
        mActionBar.setDisplayShowCustomEnabled(true);
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
    }

    // set custom header title in actionbar
    public void setHeaderTitle(String title) {

        ActionBar mActionBar = getSupportActionBar();
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View mCustomView = mInflater.inflate(R.layout.custom_header_bar, null);
        TextView mTitleTextView = (TextView) mCustomView.findViewById(R.id.action_bar_title);
        mTitleTextView.setText(title);
        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);
    }

    // set animation in progressbar
    public static void setProgressBarAnimation(ProgressBar progressBar1) {
        ObjectAnimator animation = ObjectAnimator.ofInt(progressBar1, "progress", 0, 500); // see this max value coming back here, we animale towards that value
        animation.setDuration(5000); //in milliseconds
        animation.setInterpolator(new DecelerateInterpolator());
        animation.setRepeatCount(AlphaAnimation.INFINITE);
        animation.start();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void attachBaseContext(Context newBase) {

        Log.i(newBase.toString(), "attachBaseContext: " + CommonClass.getLanguage(newBase));

        Locale newLocale = new Locale(CommonClass.getLanguage(newBase));
        Context context = ContextWrapper.wrap(newBase, newLocale);

        super.attachBaseContext(context);
    }

    public static boolean setFavorite(Context context, String id) {
        List<String> list = new ArrayList<String>(Arrays.asList(getFavorite(context).split(",")));

        if (list.contains(id)) {
            list.remove(id);
            context.getSharedPreferences(ApiParams.PREF_NAME, 0).edit().putString("favorite", TextUtils.join(",", list)).apply();
            return false;
        } else {
            list.add(id);
            context.getSharedPreferences(ApiParams.PREF_NAME, 0).edit().putString("favorite", TextUtils.join(",", list)).apply();
            return true;
        }
    }

    public static String getFavorite(Context context) {
        return context.getSharedPreferences(ApiParams.PREF_NAME, 0).getString("favorite", "");
    }

    // return true if phone is valid otherwise return false
    public boolean isValidCellPhone(String phone){
        if(!Pattern.matches("[a-zA-Z]+", phone)) {
            return phone.length() > 10 && phone.length() <= 13;
        }
        return false;
//        return android.util.Patterns.PHONE.matcher(number).matches();
    }

    // return true if email is valid otherwise return false
    public static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }
    public static boolean isValidPassword(String password) {
        Matcher matcher = Pattern.compile("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,12}$").matcher(password);
        return matcher.matches();
    }

}
