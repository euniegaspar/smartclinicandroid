package com.gep.smartdoctor.activities;

import android.content.Intent;
import android.os.Bundle;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.gep.smartdoctor.Config.ApiParams;
import com.gep.smartdoctor.R;
import com.gep.smartdoctor.adapters.CategoryListAdapter;
import com.gep.smartdoctor.adapters.MoreListSearchAdapter;
import com.gep.smartdoctor.models.CategoryModel;
import com.gep.smartdoctor.util.CommonClass;
import com.gep.smartdoctor.util.VJsonRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ActivityCategoryMoreList extends CommonActivity {

    RelativeLayout rootView;
    private ArrayList<CategoryModel> searchCategorryArray;
    private ArrayList<CategoryModel> searchCategorryListArray;
    private ArrayList<CategoryModel> categoryArray;
    private RecyclerView catRecyclerView;
    private CategoryListAdapter categoryListAdapter;
    private MoreListSearchAdapter moreListSearchAdapter;
    View emptyLayout;
    Button btnRetry;
    EditText etSearchList;

    RecyclerView rvList;
    RelativeLayout btnBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_list);
        rootView = (RelativeLayout) findViewById(R.id.rootview);

        categoryArray = new ArrayList<>();
        searchCategorryArray = new ArrayList<>();
        searchCategorryListArray = new ArrayList<>();
        etSearchList = (EditText) findViewById(R.id.et_search);
        btnBack = (RelativeLayout) findViewById(R.id.btn_back);
        etSearchList.setFilters(CommonClass.getFilterWithoutSpecialChars(30));
        etSearchList.setImeActionLabel("Search", KeyEvent.KEYCODE_ENTER);

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                ActivityMain.tabPosition = 1;
            }
        });

//        emptyLayout = (View) findViewById(R.id.emptyLayout);
//        btnRetry = (Button) emptyLayout.findViewById(R.id.btn_retry);
//        emptyLayout.setVisibility(View.GONE);
        initAct();

//        btnRetry.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                initAct();
//            }
//        });
    }

    private void initAct(){
        if(CommonClass.is_internet_connected(this)){
            loadData();
            bindView();
//            emptyLayout.setVisibility(View.GONE);
        } else{
//            emptyLayout.setVisibility(View.VISIBLE);
        }
    }

    public void bindView() {
        final LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        catRecyclerView = (RecyclerView) findViewById(R.id.rv_artist);
        catRecyclerView.setLayoutManager(layoutManager);
        categoryListAdapter = new CategoryListAdapter(this, categoryArray);
        catRecyclerView.setAdapter(categoryListAdapter);

        final LinearLayoutManager layoutManager2 = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rvList = (RecyclerView) findViewById(R.id.rv_list_search);
        rvList.setLayoutManager(layoutManager2);
        moreListSearchAdapter = new MoreListSearchAdapter(this, searchCategorryArray);

        DividerItemDecoration dividerItemDecoration2 = new DividerItemDecoration(rvList.getContext(), layoutManager2.getOrientation());
        rvList.addItemDecoration(dividerItemDecoration2);
        rvList.setAdapter(moreListSearchAdapter);

        rootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rvList.setVisibility(View.GONE);
            }
        });

        etSearchList.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                if(etSearchList.getText().length() > 0) {
                    rvList.setVisibility(View.VISIBLE);
                } else{
                    rvList.setVisibility(View.GONE);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                searchCategorryArray.clear();
                for(CategoryModel str : searchCategorryListArray){
                    Pattern p = Pattern.compile(etSearchList.getText().toString().toLowerCase() + "(.*)");
                    Matcher m = p.matcher(str.getTitle().toLowerCase());
                    if (m.find()) {
                        searchCategorryArray.add(str);
                    }
                    moreListSearchAdapter.notifyDataSetChanged();
                }

                if(searchCategorryArray.size() == 0){
                    rvList.setVisibility(View.GONE);
                }
            }
        });

        etSearchList.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    // Perform action on key press
                    SearchButtonClick();
                    return true;
                }
                return false;
            }
        });
    }

    public void loadData() {
        // this class for handle request response thread and return response data
        VJsonRequest vJsonRequest = new VJsonRequest(ActivityCategoryMoreList.this, ApiParams.CATEGORY_LIST,
                new VJsonRequest.VJsonResponce() {
                    @Override
                    public void VResponce(String responce) {

                        Gson gson = new Gson();
                        Type listType = new TypeToken<List<CategoryModel>>() {
                        }.getType();
                        categoryArray.clear();
                        searchCategorryListArray.clear();

                        categoryArray.addAll((Collection<? extends CategoryModel>) gson.fromJson(responce, listType));
                        searchCategorryListArray.addAll((Collection<? extends CategoryModel>) gson.fromJson(responce, listType));
                        Collections.sort(categoryArray, CategoryModel.BY_TITLE_ALPHABETICAL);
                        categoryListAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void VError(String responce) {
//                        progressBar1.setVisibility(View.GONE);
                    }
                });
    }

    public void SearchButtonClick() {

        Intent intent = new Intent(ActivityCategoryMoreList.this, ActivityClinicList.class);
        Bundle b = new Bundle();
        if (!TextUtils.isEmpty(etSearchList.getText().toString())) {
            b.putString("search", etSearchList.getText().toString());
        } else {
            b.putString("search", "");
        }

        intent.putExtras(b);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }
}
