package com.gep.smartdoctor.activities;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.gep.smartdoctor.R;
import com.gep.smartdoctor.dialogues.DialogCalendar;
import com.gep.smartdoctor.util.CircleTransform;
import com.gep.smartdoctor.util.CommonClass;
import com.gep.smartdoctor.util.CustomViewPager;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import com.gep.smartdoctor.Config.ApiParams;
import com.gep.smartdoctor.Config.ConstValue;
import com.gep.smartdoctor.dialogues.DoctorChooseDialog;
import com.gep.smartdoctor.fragments.TimeSlotFragment;
import com.gep.smartdoctor.models.ActiveModels;
import com.gep.smartdoctor.models.BusinessModel;
import com.gep.smartdoctor.models.DoctorModel;
import com.gep.smartdoctor.util.VJsonRequest;
import com.squareup.picasso.Picasso;

public class ActivityTimeSlot extends CommonActivity {

    public BusinessModel selected_business;
    private ImageView imageLogo;
    public ImageView imgDoctor;
    private RelativeLayout btnChooseDoctor;
    public TextView tvClinicName, tvClinicLoc,
                        tvDoctorName, tvDoctorDeg,
                        tvDateLabel;
    public static TextView tvNoTimeSlotLabel;
    public static LinearLayout layoutTimeTable;

    private TabLayout tabLayout;
    public Calendar calender;
    public String doctor_id;
    public int c_day;
    public int c_month;
    public int c_year;

    private SimpleDateFormat df, df2;
    public String currentdate;
    private CustomViewPager viewPager;
    private SampleFragmentPagerAdapter sampleFragmentPagerAdapter;
    private ArrayList<DoctorModel> mDoctorArray;
    private int service_id;

    RelativeLayout btnBack;
    ImageView btnPrevDay, btnNextDay;
    View emptyLayout;
    Button btnRetry;
    LinearLayout btnCalendar;

    int FIRST_PAGE = 0;
    int LAST_PAGE = 0;
    int CURRENT_PAGE = 0;

    boolean firstLoad  = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time_slot);

        service_id = getIntent().getExtras().getInt("serviceId");

        btnBack = (RelativeLayout) findViewById(R.id.btn_back);
        emptyLayout = (View) findViewById(R.id.emptyLayout);
        btnRetry = (Button) emptyLayout.findViewById(R.id.btn_retry);
        btnCalendar = (LinearLayout) findViewById(R.id.btn_calendar);
        emptyLayout.setVisibility(View.GONE);
        initActTimeSlot();


        btnCalendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogCalendar dialogCalendar = new DialogCalendar(ActivityTimeSlot.this);
                dialogCalendar.show();
            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initActTimeSlot();
            }
        });
    }

    private void initActTimeSlot(){
        if(CommonClass.is_internet_connected(this)){
            emptyLayout.setVisibility(View.GONE);

            viewPager = (CustomViewPager) findViewById(R.id.viewpager);
            viewPager.setPagingEnabled(false);

            selected_business = ActiveModels.BUSINESS_MODEL;
            mDoctorArray = new ArrayList<>();
            layoutTimeTable = (LinearLayout) findViewById(R.id.layout_schedules);
            layoutTimeTable.setVisibility(View.VISIBLE);
            tvNoTimeSlotLabel = (TextView) findViewById(R.id.tv_timeslot_label);
            tvNoTimeSlotLabel.setText(getResources().getString(R.string.no_slots_available));
            tvNoTimeSlotLabel.setVisibility(View.GONE);
            tvClinicName = (TextView) findViewById(R.id.tv_clinic_name);
            tvClinicName.setText(Html.fromHtml(selected_business.getBus_title()));
            tvClinicLoc = (TextView) findViewById(R.id.tv_clinic_location);
            tvClinicLoc.setText(Html.fromHtml(selected_business.getBus_google_street()));
            tvDoctorName = (TextView) findViewById(R.id.tv_doctor_name);
            tvDoctorName.setText("Choose Doctor");
            tvDoctorDeg = (TextView) findViewById(R.id.tv_doctor_deg);
            tvDoctorDeg.setVisibility(View.GONE);

            imageLogo = (ImageView) findViewById(R.id.salonImage);
            RequestOptions requestOptions = new RequestOptions();
            requestOptions = requestOptions.transforms(new CenterCrop(), new RoundedCorners(20));
            Glide.with(this)
                    .load(ConstValue.BASE_URL + "/uploads/business/" + selected_business.getBus_logo())
                    .apply(requestOptions)
                    .into(imageLogo);

            btnChooseDoctor = (RelativeLayout) findViewById(R.id.btn_choose_doctor);
            imgDoctor = (ImageView) findViewById(R.id.imgdoctor);
            tvDateLabel = (TextView) findViewById(R.id.tv_date);
            btnPrevDay = (ImageView) findViewById(R.id.btn_prev);
            btnNextDay = (ImageView) findViewById(R.id.btn_next);
            getDateText();
            loadDoctorData();

            btnChooseDoctor.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mDoctorArray != null && mDoctorArray.size() > 0) {
                        DoctorChooseDialog doctorChooseDialog = new DoctorChooseDialog(ActivityTimeSlot.this, mDoctorArray, currentdate);
                        doctorChooseDialog.show();
                    }
                }
            });

            btnPrevDay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(CURRENT_PAGE > FIRST_PAGE) {
                        if(i_adjustedDayOfYear == 0){
                            i_adjustedDayOfYear = calender.get(Calendar.DAY_OF_YEAR);
                        }
                        i_adjustedDayOfYear = i_adjustedDayOfYear - 1;
                        CURRENT_PAGE = CURRENT_PAGE - 1;

                        calender.set(Calendar.DAY_OF_YEAR, i_adjustedDayOfYear);
                        c_day = calender.get(Calendar.DAY_OF_MONTH);
                        c_month = calender.get(Calendar.MONTH) + 1;
                        c_year = calender.get(Calendar.YEAR);
                        currentDayNameString = calender.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, Locale.getDefault());
                        currentMonthNameString = calender.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault());
                        currentdate = c_year + "-" + c_month + "-" + c_day;
                        tvDateLabel.setText(currentDayNameString + " " + c_day + ", " + currentMonthNameString);
                        viewPager.setCurrentItem(viewPager.getCurrentItem()-1, true);

                        loadDoctorData();
                    } else{
                        Toast.makeText(getApplicationContext(), "Cannot load previous days", Toast.LENGTH_SHORT).show();
                    }
                }
            });

            btnNextDay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(i_adjustedDayOfYear == 0){
                        i_adjustedDayOfYear = calender.get(Calendar.DAY_OF_YEAR);
                    }
                    i_adjustedDayOfYear = i_adjustedDayOfYear + 1;
                    CURRENT_PAGE = CURRENT_PAGE + 1;

                    calender.set(Calendar.DAY_OF_YEAR, i_adjustedDayOfYear);
                    c_day = calender.get(Calendar.DAY_OF_MONTH);
                    c_month = calender.get(Calendar.MONTH) + 1;
                    c_year = calender.get(Calendar.YEAR);
                    currentDayNameString = calender.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, Locale.getDefault());
                    currentMonthNameString = calender.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault());
                    currentdate = c_year + "-" + c_month + "-" + c_day;
                    tvDateLabel.setText(currentDayNameString + " " + c_day + ", " + currentMonthNameString);
                    viewPager.setCurrentItem(viewPager.getCurrentItem()+1, true);

                    loadDoctorData();
                }

            });

        } else{
            emptyLayout.setVisibility(View.VISIBLE);
        }
    }

    Date date;
    public String currentDayNameString, currentMonthNameString;
    public int i_currentDayOfYear,i_adjustedDayOfYear;

    public void getDateText(){
        //PLEASE CHECK VARIABLES, IT MAY VARY FROM THE OLD DEVS INITIALIZATION!!
        calender = Calendar.getInstance(TimeZone.getDefault());
        date = new Date();
        SimpleDateFormat month_date = new SimpleDateFormat("MMMM");
        currentMonthNameString = month_date.format(calender.getTime());

        c_day = calender.get(Calendar.DAY_OF_MONTH);
        c_month = calender.get(Calendar.MONTH) + 1;
        c_year = calender.get(Calendar.YEAR);
        i_currentDayOfYear = calender.get(Calendar.DAY_OF_YEAR);
        currentdate = c_year + "-" + c_month + "-" + c_day;
        currentDayNameString = calender.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, Locale.getDefault());
        tvDateLabel.setText(currentDayNameString + " " + c_day + ", " + currentMonthNameString);
        i_adjustedDayOfYear = 0;
        FIRST_PAGE = 1;
        CURRENT_PAGE = FIRST_PAGE;
        LAST_PAGE = FIRST_PAGE + 30;
    }

    public void loadDoctorData() {
        HashMap<String, String> params = new HashMap<>();
        params.put("bus_id", selected_business.getBus_id());

        VJsonRequest vJsonRequest = new VJsonRequest(ActivityTimeSlot.this, ApiParams.GET_DOCTORS, params,
                new VJsonRequest.VJsonResponce() {
                    @Override
                    public void VResponce(String responce) {

                        Gson gson = new Gson();
                        Type listType = new TypeToken<List<DoctorModel>>() {
                        }.getType();
                        mDoctorArray.clear();
                        mDoctorArray.addAll((Collection<? extends DoctorModel>) gson.fromJson(responce, listType));
                        if (mDoctorArray != null && mDoctorArray.size() > 0) {
                            if(firstLoad) { ///FOR DOCTOR LABEL
                                ActiveModels.DOCTOR_MODEL = mDoctorArray.get(0);  //identify doctor array details
                                doctor_id = ActiveModels.DOCTOR_MODEL.getDoct_id();

                                tvDoctorName.setText(ActiveModels.DOCTOR_MODEL.getDoct_name());
                                tvDoctorDeg.setText(ActiveModels.DOCTOR_MODEL.getDoct_speciality());
                                tvDoctorDeg.setVisibility(View.VISIBLE);
                                layoutTimeTable.setVisibility(View.VISIBLE);
                                tvNoTimeSlotLabel.setVisibility(View.GONE);

                                Picasso.with(getApplicationContext())
                                        .load(ConstValue.BASE_URL + "/uploads/business/" + ActiveModels.DOCTOR_MODEL.getDoct_photo())
                                        .placeholder(R.drawable.icon_doctor)
                                        .resize(200, 200)
                                        .transform(new CircleTransform())
                                        .centerCrop()
                                        .error(R.drawable.icon_doctor)
                                        .into(imgDoctor);

                                firstLoad = false;
                            }

                            sampleFragmentPagerAdapter = new SampleFragmentPagerAdapter(getSupportFragmentManager());
                            viewPager.setAdapter(sampleFragmentPagerAdapter);
                        } else{
                            layoutTimeTable.setVisibility(View.GONE);
                            tvNoTimeSlotLabel.setVisibility(View.VISIBLE);
                        }
                    }

                    @Override
                    public void VError(String responce) {
                        layoutTimeTable.setVisibility(View.GONE);
                        tvNoTimeSlotLabel.setVisibility(View.VISIBLE);
                    }
                });


    }

    class SampleFragmentPagerAdapter extends FragmentStatePagerAdapter {
        final int PAGE_COUNT = 40;

        public SampleFragmentPagerAdapter(FragmentManager fm) {
            super(fm);
        }
        @Override
        public Fragment getItem(int position) {
            return TimeSlotFragment.newInstance(doctor_id, service_id, currentdate, tvDoctorName.getText().toString(),
                    tvClinicName.getText().toString(), tvClinicLoc.getText().toString()/*,
                    calender.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.getDefault()),
                    calender.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault()),
                    String.valueOf(c_day), String.valueOf(c_year)*/);
        }
        @Override
        public int getCount() {
            return PAGE_COUNT;
        }

        @Override
        public int getItemPosition(Object object) {
            TimeSlotFragment f = (TimeSlotFragment) object;
            return super.getItemPosition(object);   //Don't comment this...para di magdikit ang cards
        }
    }

}
