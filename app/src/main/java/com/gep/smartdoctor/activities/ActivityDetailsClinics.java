package com.gep.smartdoctor.activities;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.core.widget.NestedScrollView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gep.smartdoctor.R;
import com.gep.smartdoctor.util.CommonClass;
import com.google.android.material.tabs.TabLayout;
import com.squareup.picasso.Picasso;

import com.gep.smartdoctor.Config.ConstValue;
import com.gep.smartdoctor.fragments.DetailsFragment;
import com.gep.smartdoctor.fragments.DoctorsFragment;
import com.gep.smartdoctor.fragments.PhotosFragment;
import com.gep.smartdoctor.fragments.PriceFragment;
import com.gep.smartdoctor.fragments.ReviewsFragment;
import com.gep.smartdoctor.models.ActiveModels;
import com.gep.smartdoctor.models.BusinessModel;

public class ActivityDetailsClinics extends CommonActivity {

    private BusinessModel selected_business;
    private TextView tvTypeService, tvServiceTime, tvServiceCharge, tvAmount, tvDiscount, tvTotalAmount;
    private TabLayout tabLayout;
    RelativeLayout btnBack;
    private int tabIcons[] = {R.drawable.icon_services, R.drawable.icon_details, R.drawable.icon_doctor, R.drawable.icon_review_clinic, R.drawable.icon_photo};
    ImageView imgStar1, imgStar2, imgStar3, imgStar4, imgStar5;
    LinearLayout btnBook;
    View cartLayout, emptyLayout;
    ImageView btnCloseCart;
    Button btnRetry;
    ImageView bannerImage;
    TextView tvClinicName, tvClinicLoc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_clinics);

        selected_business = ActiveModels.BUSINESS_MODEL;
        cartLayout = (View) findViewById(R.id.cart_layout);
        bannerImage = (ImageView) findViewById(R.id.bannerImage);
        tvClinicName = (TextView) findViewById(R.id.title);
        tvClinicLoc = (TextView) findViewById(R.id.subTitle);

        emptyLayout = (View) findViewById(R.id.emptyLayout);
        btnRetry = (Button) emptyLayout.findViewById(R.id.btn_retry);
        emptyLayout.setVisibility(View.GONE);

        btnBack = (RelativeLayout) findViewById(R.id.btn_back);
        initAct();

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initAct();
            }
        });
    }

    private void initAct(){
        if(CommonClass.is_internet_connected(this)){
            emptyLayout.setVisibility(View.GONE);

            tvClinicName.setText(selected_business.getBus_title());
            tvClinicLoc.setText(selected_business.getBus_google_street());

            ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
            viewPager.setAdapter(new SampleFragmentPagerAdapter(getSupportFragmentManager()));
            viewPager.setOffscreenPageLimit(0);
            tabLayout = (TabLayout) findViewById(R.id.tabs);
            tabLayout.setupWithViewPager(viewPager);
            setupTabIcons();

            NestedScrollView scrollView = (NestedScrollView) findViewById(R.id.nsv);
            scrollView.setFillViewport(true);

            if (selected_business != null) {
                Picasso.with(this).load(ConstValue.BASE_URL + "/uploads/business/" +
                        selected_business.getBus_logo())
                        .placeholder(R.drawable.placeholder_image)
                        .error(R.drawable.placeholder_image)
                        .into(bannerImage);
            }

            btnCloseCart = (ImageView) cartLayout.findViewById(R.id.btn_close_cart);
            tvTypeService = (TextView) cartLayout.findViewById(R.id.label_type_service);
            tvServiceTime = (TextView) cartLayout.findViewById(R.id.tv_service_time);
            tvServiceCharge = (TextView) cartLayout.findViewById(R.id.tv_service_charge);
            tvAmount = (TextView) cartLayout.findViewById(R.id.tv_amount);
            tvDiscount = (TextView) cartLayout.findViewById(R.id.tv_discount);
            tvTotalAmount = (TextView) cartLayout.findViewById(R.id.tv_total_amount);

            showRatingBar(Float.parseFloat(selected_business.getAvg_rating()));
            cartLayout.setVisibility(View.GONE);

            btnCloseCart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    cartLayout.setVisibility(View.INVISIBLE);
                }
            });

        } else{
            emptyLayout.setVisibility(View.VISIBLE);
        }
    }

    private void setupTabIcons() {
        tabLayout.getTabAt(0).setIcon(R.drawable.icon_services);
        tabLayout.getTabAt(1).setIcon(R.drawable.icon_details);
        tabLayout.getTabAt(2).setIcon(R.drawable.icon_doctor);
        tabLayout.getTabAt(3).setIcon(R.drawable.icon_review_clinic);
        tabLayout.getTabAt(4).setIcon(R.drawable.icon_photo);

        View view = LayoutInflater.from(this).inflate(R.layout.clinic_details_tab, null);
        ((ImageView) view.findViewById(R.id.tab_icon)).setImageResource(R.drawable.icon_services);
        ((TextView) view.findViewById(R.id.tab_text)).setText(getResources().getString(R.string.tab_service));

        View view2 = LayoutInflater.from(this).inflate(R.layout.clinic_details_tab, null);
        ((ImageView) view2.findViewById(R.id.tab_icon)).setImageResource(R.drawable.icon_details);
        ((TextView) view2.findViewById(R.id.tab_text)).setText(getResources().getString(R.string.tab_details));

        View view3 = LayoutInflater.from(this).inflate(R.layout.clinic_details_tab, null);
        ((ImageView) view3.findViewById(R.id.tab_icon)).setImageResource(R.drawable.icon_doctor);
        ((TextView) view3.findViewById(R.id.tab_text)).setText(getResources().getString(R.string.tab_doctors));

        View view4 = LayoutInflater.from(this).inflate(R.layout.clinic_details_tab, null);
        ((ImageView) view4.findViewById(R.id.tab_icon)).setImageResource(R.drawable.icon_review_clinic);
        ((TextView) view4.findViewById(R.id.tab_text)).setText(getResources().getString(R.string.tab_reviews));

        View view5 = LayoutInflater.from(this).inflate(R.layout.clinic_details_tab, null);
        ((ImageView) view5.findViewById(R.id.tab_icon)).setImageResource(R.drawable.icon_photo);
        ((TextView) view5.findViewById(R.id.tab_text)).setText(getResources().getString(R.string.tab_photo));

        tabLayout.getTabAt(0).setCustomView(view);
        tabLayout.getTabAt(1).setCustomView(view2);
        tabLayout.getTabAt(2).setCustomView(view3);
        tabLayout.getTabAt(3).setCustomView(view4);
        tabLayout.getTabAt(4).setCustomView(view5);
    }

    public void showRatingBar(float rate){
        imgStar1 = (ImageView) findViewById(R.id.img_star1);
        imgStar2 = (ImageView) findViewById(R.id.img_star2);
        imgStar3 = (ImageView) findViewById(R.id.img_star3);
        imgStar4 = (ImageView) findViewById(R.id.img_star4);
        imgStar5 = (ImageView) findViewById(R.id.img_star5);

        if(rate >= 5){
            imgStar1.setImageResource(R.drawable.star_small_yellow);
            imgStar2.setImageResource(R.drawable.star_small_yellow);
            imgStar3.setImageResource(R.drawable.star_small_yellow);
            imgStar4.setImageResource(R.drawable.star_small_yellow);
            imgStar5.setImageResource(R.drawable.star_small_yellow);
        }
        if(rate >= 4 && rate < 5){
            imgStar1.setImageResource(R.drawable.star_small_yellow);
            imgStar2.setImageResource(R.drawable.star_small_yellow);
            imgStar3.setImageResource(R.drawable.star_small_yellow);
            imgStar4.setImageResource(R.drawable.star_small_yellow);
            imgStar5.setImageResource(R.drawable.star_small_gray);

//            if(rate >= 4.5 && rate < 5){
//                imgStar5.setImageResource(R.drawable.halfstar_white_small);
//            }
        }
        if(rate >= 3 && rate < 4){
            imgStar1.setImageResource(R.drawable.star_small_yellow);
            imgStar2.setImageResource(R.drawable.star_small_yellow);
            imgStar3.setImageResource(R.drawable.star_small_yellow);
            imgStar4.setImageResource(R.drawable.star_small_gray);
            imgStar5.setImageResource(R.drawable.star_small_gray);

//            if(rate >= 3.5 && rate < 4){
//                imgStar4.setImageResource(R.drawable.halfstar_white_small);
//            }
        }
        if(rate >= 2 && rate < 3){
            imgStar1.setImageResource(R.drawable.star_small_yellow);
            imgStar2.setImageResource(R.drawable.star_small_yellow);
            imgStar3.setImageResource(R.drawable.star_small_gray);
            imgStar4.setImageResource(R.drawable.star_small_gray);
            imgStar5.setImageResource(R.drawable.star_small_gray);

//            if(rate >= 2.5 && rate < 3){
//                imgStar3.setImageResource(R.drawable.halfstar_white_small);
//            }
        }
        if(rate >= 1 && rate < 2){
            imgStar1.setImageResource(R.drawable.star_small_yellow);
            imgStar2.setImageResource(R.drawable.star_small_gray);
            imgStar3.setImageResource(R.drawable.star_small_gray);
            imgStar4.setImageResource(R.drawable.star_small_gray);
            imgStar5.setImageResource(R.drawable.star_small_gray);

//            if(rate >= 1.5 && rate < 2){
//                imgStar2.setImageResource(R.drawable.halfstar_white_small);
//            }
        }
        if(rate >= 0){
            if(rate >= .5 && rate < 1){
                imgStar1.setImageResource(R.drawable.star_small_gray);
                imgStar2.setImageResource(R.drawable.star_small_gray);
                imgStar3.setImageResource(R.drawable.star_small_gray);
                imgStar4.setImageResource(R.drawable.star_small_gray);
                imgStar5.setImageResource(R.drawable.star_small_gray);
            }
        }
    }

    public void showCartLayout() {
        cartLayout.setVisibility(View.VISIBLE);
    }

    // update price with selected services
    public void updatePrice(String txtTypeService,
                            String[] timesplit,
                            String servicecharge,
                            String amount,
                            String discount,
                            String totalamount) {

        tvTypeService.setText(getResources().getString(R.string.label_type_service) + " " + txtTypeService);
        tvServiceTime.setText(timesplit[0] + " " + getString(R.string.hr) + " " + timesplit[1] + " " + getString(R.string.min));
        tvServiceCharge.setText("P. " + servicecharge + ".00");
        if(servicecharge.equals("-0")){
            tvServiceCharge.setText("");
        }
        tvAmount.setText("P. " + amount + "0");
        tvDiscount.setText("- P. " + discount + "0");
        if(discount.equals("0.0")){
            tvDiscount.setText("");
        }
        tvTotalAmount.setText("P. " + totalamount + "0");
    }

    // bind fragment in view pager
    public class SampleFragmentPagerAdapter extends FragmentPagerAdapter {
        final int PAGE_COUNT = 5;
        private String tabTitles[] = new String[]{getString(R.string.tab_service), getString(R.string.tab_details), getString(R.string.tab_doctors), getString(R.string.tab_reviews), getString(R.string.tab_photo)};

        public SampleFragmentPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return PAGE_COUNT;
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment = null;
            if (position == 0) {
                fragment = new PriceFragment();

            } else if (position == 1) {
                fragment = new DetailsFragment();
            } else if (position == 2) {
                fragment = new DoctorsFragment();
            } else if (position == 3) {
                fragment = new ReviewsFragment();
            } else if (position == 4) {
                fragment = new PhotosFragment();
            }
            if (fragment != null) {
                Bundle args = new Bundle();
                fragment.setArguments(args);
            }
            return fragment;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return tabTitles[position];
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();

        selected_business = ActiveModels.BUSINESS_MODEL;
        tvClinicName = (TextView) findViewById(R.id.title);
        tvClinicLoc = (TextView) findViewById(R.id.subTitle);
    }

}
