package com.gep.smartdoctor.activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gep.smartdoctor.Config.ApiParams;
import com.gep.smartdoctor.R;
import com.gep.smartdoctor.util.VJsonRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class ActivityForgotPassword extends CommonActivity {

    RelativeLayout btnBack;
    EditText etEmail;
    Button btnConfirm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        btnBack = (RelativeLayout) findViewById(R.id.btn_back);
        etEmail = (EditText) findViewById(R.id.et_email);
        btnConfirm = (Button) findViewById(R.id.btn_confirm);
        btnConfirm.setEnabled(false);
        btnConfirm.setBackground(getResources().getDrawable(R.drawable.rounded_btn_gray));

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        etEmail.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                if(etEmail.getText().length() > 0) {
                    btnConfirm.setEnabled(true);
                    btnConfirm.setBackground(getResources().getDrawable(R.drawable.rounded_button));
                } else{
                    btnConfirm.setEnabled(false);
                    btnConfirm.setBackground(getResources().getDrawable(R.drawable.rounded_btn_gray));
                }

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(v.getContext());
                HashMap<String, String> params = new HashMap<>();

                params.put("user_email", etEmail.getText().toString());

                VJsonRequest vJsonRequest = new VJsonRequest(ActivityForgotPassword.this, ApiParams.FORGOT_PASSWORD_URL, params,
                        new VJsonRequest.VJsonResponce() {
                            @Override
                            public void VResponce(String responce) {
                                Log.d("responce123", "success: "+responce);

                                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                dialog.setCancelable(false);
                                dialog.setContentView(R.layout.dialog_successful);
                                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                                TextView text = (TextView) dialog.findViewById(R.id.dialog_title);
                                text.setText(getResources().getString(R.string.email_confirm));

                                Button btnOk = (Button) dialog.findViewById(R.id.btn_ok);
                                btnOk.setAllCaps(true);

                                btnOk.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        dialog.dismiss();
                                        onBackPressed();
                                    }
                                });

                                dialog.show();
                            }

                            @Override
                            public void VError(String responce) {
                                Log.d("responce123", "error: "+ responce);

                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ActivityForgotPassword.this);
                                alertDialogBuilder.setMessage(responce.replace(": ",""));
                                alertDialogBuilder.setPositiveButton("Okay",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface arg0, int arg1) {
                                            }
                                        });

                                AlertDialog alertDialog = alertDialogBuilder.create();
                                alertDialog.show();
                            }
                        });
            }
        });
    }
}