package com.gep.smartdoctor.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gep.smartdoctor.R;
import com.gep.smartdoctor.adapters.AmbulanceAdapter;
import com.gep.smartdoctor.adapters.EmergencyHotlineAdapter;
import com.gep.smartdoctor.models.AmbulanceModel;
import com.gep.smartdoctor.models.EmergencyHotlinesModel;

import java.util.ArrayList;

public class ActivityEmergencyHotlines extends CommonActivity {

    RelativeLayout btnBack;
    RecyclerView rvEmerHotlines;
    EmergencyHotlineAdapter emergencyHotlineAdapter;
    ArrayList<EmergencyHotlinesModel> emergencyHotlinesArray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emergency_hotlines);

        emergencyHotlinesArray = new ArrayList<>();
        ArrayList<String> title = new ArrayList<String>();
        ArrayList<String> hotline = new ArrayList<String>();
        ArrayList<String> trunkline = new ArrayList<String>();

        btnBack = (RelativeLayout) findViewById(R.id.btn_back);
        rvEmerHotlines = (RecyclerView) findViewById(R.id.rv_hotlines);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rvEmerHotlines.setLayoutManager(layoutManager);
        emergencyHotlineAdapter = new EmergencyHotlineAdapter(this, emergencyHotlinesArray);
        rvEmerHotlines.setAdapter(emergencyHotlineAdapter);

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                ActivityMain.tabPosition = 1;
            }
        });


        //temporary
        title.add("UNTV News and Rescue Emergency");
        title.add("RED CROSS");
        title.add("DEPARTMENT OF INTERIOR AND LOCAL GOVERNMENT (DILG)");

        hotline.add("0923-544-5376 (SUN)\n0915-460-5540 (GLOBE)");
        hotline.add("143\n(02) 8527-8385 to 95");
        hotline.add("");

        trunkline.add("09461926752 (SMART)");
        trunkline.add("(02) 8790-2300");
        trunkline.add("(02)8876 3454");


        for(int i = 0; i < title.size() ; i++) {
            EmergencyHotlinesModel emergencyHotlinesModel = new EmergencyHotlinesModel();
            emergencyHotlinesModel.setTitle(title.get(i));
            emergencyHotlinesModel.setHotline(hotline.get(i));
            emergencyHotlinesModel.setTrunkline(trunkline.get(i));

            emergencyHotlinesArray.add(emergencyHotlinesModel);
        }
        emergencyHotlineAdapter.notifyDataSetChanged();


    }
}