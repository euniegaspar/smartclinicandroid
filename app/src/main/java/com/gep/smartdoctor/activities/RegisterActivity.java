package com.gep.smartdoctor.activities;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.gep.smartdoctor.Config.ApiParams;
import com.gep.smartdoctor.Config.ConstValue;
import com.gep.smartdoctor.R;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.gep.smartdoctor.util.CommonClass;
import com.gep.smartdoctor.util.VJsonRequest;
import com.google.android.material.textfield.TextInputLayout;
import com.hbb20.CountryCodePicker;

import org.json.JSONException;
import org.json.JSONObject;

public class RegisterActivity extends CommonActivity {

    private EditText editEmail, editPassword, editConfirmPassword, editPhone, editFullname;
    TextView txtTermsAndPolicy, tvSignIn;
    RelativeLayout btnBack;
    private CheckBox checkBox;
//    CountryCodePicker ccp;

    TextInputLayout tilFullName, tilPhone, tilEmail, tilPassword, tilConfirmPassword;
    TextView tvNameErrorLabel, tvPhoneErrorLabel, tvEmailErrorLabel, tvPassErrorLabel, tvConfirmPassErrorLabel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        btnBack = (RelativeLayout) findViewById(R.id.btn_back);
        tvNameErrorLabel = (TextView) findViewById(R.id.error_name_label);
        tvPhoneErrorLabel = (TextView) findViewById(R.id.error_phone_label);
        tvEmailErrorLabel = (TextView) findViewById(R.id.error_email_label);
        tvPassErrorLabel = (TextView) findViewById(R.id.error_pw_label);
        tvConfirmPassErrorLabel = (TextView) findViewById(R.id.error_cpw_label);

        tvSignIn = (TextView) findViewById(R.id.tv_signin);
//        ccp = (CountryCodePicker) findViewById(R.id.ccp);
//        ccp.hideNameCode(true);
//        ccp.setFullNumber("63");
        tilFullName = (TextInputLayout) findViewById(R.id.til_fullname);
        tilPhone = (TextInputLayout) findViewById(R.id.til_phone);
        tilEmail = (TextInputLayout) findViewById(R.id.til_email);
        tilPassword = (TextInputLayout) findViewById(R.id.til_password);
        tilConfirmPassword = (TextInputLayout) findViewById(R.id.til_confirmpassword);

        editFullname = (EditText) findViewById(R.id.txtFirstname);
        editPhone = (EditText) findViewById(R.id.txtPhone);
        editEmail = (EditText) findViewById(R.id.txtEmail);
        editPassword = (EditText) findViewById(R.id.txtPassword);
        editPassword = (EditText) findViewById(R.id.txtPassword);
        editConfirmPassword = (EditText) findViewById(R.id.txtConfirmPassword);

        editFullname.requestFocus();
        editFullname.setFilters(CommonClass.getFilterWithoutSpecialChars(50));
        editPhone.setFilters(CommonClass.getFilterWithoutSpecialChars(15));
        editEmail.setFilters(CommonClass.getFilterWithCharLimit(80));
        editPassword.setFilters(CommonClass.getFilterWithCharLimit(50));
        editConfirmPassword.setFilters(CommonClass.getFilterWithCharLimit(50));

        initTermsAndConditions();
        textInputFunction();

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                finish();
            }
        });

        Button btnSignUp = (Button) findViewById(R.id.btn_signup);
        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(CommonClass.is_internet_connected(getApplicationContext())){ ///temporary only
                    InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                    inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                    register();
                } else{
                    Toast.makeText(getApplicationContext(), R.string.error_no_internet, Toast.LENGTH_SHORT).show();
                }
//                register();
            }
        });


        String registerstring = getResources().getString(R.string.already_have_account) + " " +
                getResources().getString(R.string.sign_in_caps) + "";
        tvSignIn.setText(Html.fromHtml(registerstring));

        //Span "signup"
        Pattern word = Pattern.compile(getResources().getString(R.string.sign_in_caps));
        Matcher match = word.matcher(tvSignIn.getText().toString());
        int startIndexterms = 0;
        int endIndexterms = 0;

        while (match.find()) {
            startIndexterms = match.start();
            endIndexterms = match.end();
        }

        SpannableString ss = new SpannableString(registerstring);

        ClickableSpan clickableSpan1 = new ClickableSpan() {
            @Override
            public void updateDrawState(TextPaint ds) {
                ds.setFakeBoldText(false);
                ds.setUnderlineText(true);    // this remove the underline
            }

            @Override
            public void onClick(View widget) {
                finish();
            }
        };
        ss.setSpan(clickableSpan1, startIndexterms, endIndexterms, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        tvSignIn.setText(ss);
        tvSignIn.setMovementMethod(LinkMovementMethod.getInstance());

    }

    public void initTermsAndConditions(){
        checkBox =  (CheckBox)findViewById(R.id.checkbox);
        txtTermsAndPolicy = (TextView) findViewById(R.id.about_terms);
        String termsString = getResources().getString(R.string.createaccount_termsandprivacypolicy) + " " +
                getResources().getString(R.string.createaccount_terms) + " " +
                getResources().getString(R.string.createaccount_and) + " " +
                getResources().getString(R.string.createaccount_privacypolicy) + "";
        txtTermsAndPolicy.setText(Html.fromHtml(termsString));

//        //Span "terms"
//        Pattern word1 = Pattern.compile(getResources().getString(R.string.createaccount_terms));
//        Matcher match1 = word1.matcher(txtTermsAndPolicy.getText().toString());
//        int startIndexterms1 = 0;
//        int endIndexterms1 = 0;
//
//        while (match1.find()) {
//            startIndexterms1 = match1.start();
//            endIndexterms1 = match1.end();
//        }

        //Span "privacy policy"
        Pattern word2 = Pattern.compile("Terms and Privacy Policy");
        Matcher match2 = word2.matcher(txtTermsAndPolicy.getText().toString());
        int startIndexterms2 = 0;
        int endIndexterms2 = 0;

        while (match2.find()) {
            startIndexterms2 = match2.start();
            endIndexterms2 = match2.end();
        }


        SpannableString ss = new SpannableString(termsString);

        ClickableSpan clickableSpan1 = new ClickableSpan() {
            @Override
            public void updateDrawState(TextPaint ds) {
                ds.setFakeBoldText(true);
                ds.setUnderlineText(false);    // this remove the underline
            }

            @Override
            public void onClick(View widget) {
                if(CommonClass.is_internet_connected(getApplicationContext())){
                    Uri uri = Uri.parse(ConstValue.TERMS_CONDITIONS_URL); // missing 'http://' will cause crashed
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    startActivity(intent);
                }
            }
        };

        ClickableSpan clickableSpan2 = new ClickableSpan() {
            @Override
            public void updateDrawState(TextPaint ds) {
                ds.setFakeBoldText(true);
                ds.setUnderlineText(false);    // this remove the underline
            }

            @Override
            public void onClick(View widget) {
//                common.setToastMessage("Privacy Policy");
                if(CommonClass.is_internet_connected(getApplicationContext())){
                    Uri uri = Uri.parse(ConstValue.PRIVACY_POLICY_URL);
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);

                    startActivity(intent);
                }
            }
        };

//        ss.setSpan(clickableSpan1, startIndexterms1, endIndexterms1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss.setSpan(clickableSpan2, startIndexterms2, endIndexterms2, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        txtTermsAndPolicy.setText(ss);
        txtTermsAndPolicy.setMovementMethod(LinkMovementMethod.getInstance());
    }

    public void textInputFunction(){
        editFullname.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tvNameErrorLabel.setVisibility(View.GONE);
                if(tilFullName.isErrorEnabled()){
                    tilFullName.setErrorEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });

        editPhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tvPhoneErrorLabel.setVisibility(View.GONE);
                if(tilPhone.isErrorEnabled()){
                    tilPhone.setErrorEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });

        editEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tvEmailErrorLabel.setVisibility(View.GONE);
                if(tilEmail.isErrorEnabled()){
                    tilEmail.setErrorEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });

        editPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tvPassErrorLabel.setVisibility(View.GONE);
                if(tilPassword.isErrorEnabled()){
                    tilPassword.setErrorEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });
    }

    // attempt register user with required validation
    public void register() {

        String email = editEmail.getText().toString();
        String password = editPassword.getText().toString();
        String confirmpassword = editConfirmPassword.getText().toString();
        String fullname = editFullname.getText().toString();
        String phone = "+63" + editPhone.getText().toString();
        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(fullname)) {
            tvNameErrorLabel.setText(getString(R.string.valid_required_fullname));
            tvNameErrorLabel.setVisibility(View.VISIBLE);
            focusView = editFullname;
            cancel = true;
        }
        if (TextUtils.isEmpty(phone)) {
            tvPhoneErrorLabel.setText(getString(R.string.valid_required_phone));
            tvPhoneErrorLabel.setVisibility(View.VISIBLE);
            focusView = editPhone;
            cancel = true;
        } else{
            // Check for a valid phone number
            if (!isValidCellPhone(phone)) {
                tvPhoneErrorLabel.setText(getString(R.string.invalid_phone_number));
                tvPhoneErrorLabel.setVisibility(View.VISIBLE);
                focusView = editPhone;
                cancel = true;
            }
        }

        if (TextUtils.isEmpty(email)) {
            tvEmailErrorLabel.setText(getString(R.string.valid_required_email));
            tvEmailErrorLabel.setVisibility(View.VISIBLE);
            focusView = editEmail;
            cancel = true;
        } else {
            // Check for a valid email address.
            if (!isValidEmail(email)) {
                tvEmailErrorLabel.setText(getString(R.string.valid_email));
                tvEmailErrorLabel.setVisibility(View.VISIBLE);
                focusView = editEmail;
                cancel = true;
            }
        }
        if (TextUtils.isEmpty(password)) {
            tvPassErrorLabel.setText(getString(R.string.valid_required_password));
            tvPassErrorLabel.setVisibility(View.VISIBLE);
            focusView = editPassword;
            cancel = true;
        } else {
            if (!isValidPassword(password)) {
                tvPassErrorLabel.setText("Password should be 6-12 char alphanumeric, uppercase, lowercase, and special symbol from #?!@$%^&*-.");
                tvPassErrorLabel.setVisibility(View.VISIBLE);
                focusView = editPassword;
                cancel = true;
            }
        }
        if (TextUtils.isEmpty(confirmpassword)) {
            tvConfirmPassErrorLabel.setText(getString(R.string.valid_required_confirmpassword));
            tvConfirmPassErrorLabel.setVisibility(View.VISIBLE);
            focusView = editConfirmPassword;
            cancel = true;
        } else {
            if (!isValidPassword(confirmpassword)) {
                tvConfirmPassErrorLabel.setText("Confirm Password should be 6-12 char alphanumeric, uppercase, lowercase, and special symbol from #?!@$%^&*-.");
                tvConfirmPassErrorLabel.setVisibility(View.VISIBLE);
                focusView = editConfirmPassword;
                cancel = true;
            }
        }

        if(!password.equals(confirmpassword)) {
            tvConfirmPassErrorLabel.setText("Password and Confirm Password should be same");
            tvConfirmPassErrorLabel.setVisibility(View.VISIBLE);
            focusView = editConfirmPassword;
            cancel = true;
        } else {
            tvConfirmPassErrorLabel.setVisibility(View.GONE);
        }


        if (cancel) {
            if (focusView != null)
                focusView.requestFocus();
        } else {
            if (!checkBox.isChecked()){
                try{
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            checkBox.setButtonTintList(getColorStateList(R.color.colorRed));
                        }
                    } else{
                        checkBox.setBackgroundColor(getResources().getColor(R.color.colorRed));
                    }
                } catch(Exception e){

                }
                return;
            } else {

                HashMap<String, String> params = new HashMap<>();
                params.put("user_fullname", fullname);
                params.put("user_phone", phone);
                params.put("user_email", email);
                params.put("user_password", password);

                VJsonRequest vJsonRequest = new VJsonRequest(this, ApiParams.SIGNUPVALIDATION_URL, params,
                        new VJsonRequest.VJsonResponce() {
                            @Override
                            public void VResponce(String responce) {
                                JSONObject userdata = null;
                                try {
                                    userdata = new JSONObject(responce);

                                    Intent intent = new Intent(RegisterActivity.this, ActivityVerifyPhone.class);
                                    intent.putExtra("user_fullname", fullname);
                                    intent.putExtra("user_phone", phone);
                                    intent.putExtra("user_email", email);
                                    intent.putExtra("user_password", password);
                                    intent.putExtra("registration", "true");
                                    startActivity(intent);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void VError(String responce) {
                                JSONObject userdata = null;
                                try {
                                    userdata = new JSONObject(responce);

                                    if(userdata.getString("user_phone") != null && userdata.getString("user_email") != null){
                                        common.setToastMessage("Email address and mobile number is already taken");
                                    }

                                } catch (JSONException e) {

                                    if(e.getMessage().equals("No value for user_phone")){
                                        common.setToastMessage(getResources().getString(R.string.email_already_registered));
                                    } else if (e.getMessage().equals("No value for user_email")) {
                                        common.setToastMessage(getResources().getString(R.string.no_already_registered));
                                    }

                                    e.printStackTrace();
                                }

                            }
                        });
            }
        }

    }
}
