package com.gep.smartdoctor.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gep.smartdoctor.Config.ApiParams;
import com.gep.smartdoctor.R;
import com.gep.smartdoctor.activities.ActivityMain;
import com.gep.smartdoctor.adapters.MyAppointmentAdapter;
import com.gep.smartdoctor.models.AppointmentModel;
import com.gep.smartdoctor.util.CommonClass;
import com.gep.smartdoctor.util.VJsonRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

public class FragmentRecordsHistory extends Fragment {

    View view;
    CommonClass common;
    private ArrayList<AppointmentModel> appointmentArray;
    private RecyclerView rvRecordsItem;
    private MyAppointmentAdapter myAppointmentAdapter;

    ArrayList<String> idList;
    ArrayList<String> userIdList;
    ArrayList<String> apptDateList;
    ArrayList<String> starttimeList;
    ArrayList<String> timetokenList;
    ArrayList<String> statusList;
    ArrayList<String> appnameList;
    ArrayList<String> appemailList;
    ArrayList<String> createdatList;
    ArrayList<String> contimeList;
    ArrayList<String> busTitleList;
    ArrayList<String> busSlugList;
    ArrayList<String> doctNameList;
    ArrayList<String> doctSpecialtyList;
    ArrayList<String> paymentRefList;
    ArrayList<String> imageList;
    ArrayList<String> cancelStatusList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_records, container, false);
        common = new CommonClass(getActivity());
        appointmentArray = new ArrayList<>();
        ActivityMain.hospitalLists.clear();

        idList = new ArrayList<String>();
        userIdList = new ArrayList<String>();
        apptDateList = new ArrayList<String>();
        starttimeList = new ArrayList<String>();
        timetokenList = new ArrayList<String>();
        statusList = new ArrayList<String>();
        appnameList = new ArrayList<String>();
        appemailList = new ArrayList<String>();
        createdatList = new ArrayList<String>();
        contimeList = new ArrayList<String>();
        busTitleList = new ArrayList<String>();
        busSlugList = new ArrayList<String>();
        doctNameList = new ArrayList<String>();
        doctSpecialtyList = new ArrayList<String>();
        paymentRefList = new ArrayList<String>();
        imageList = new ArrayList<String>();
        cancelStatusList = new ArrayList<String>();


        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        rvRecordsItem = (RecyclerView) view.findViewById(R.id.rv_records_item);
        rvRecordsItem.setLayoutManager(layoutManager);
        myAppointmentAdapter = new MyAppointmentAdapter(getActivity(), appointmentArray);
        myAppointmentAdapter.setHasStableIds(true);
        rvRecordsItem.setAdapter(myAppointmentAdapter);



        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", common.get_user_id());

        // this class for handle request response thread and return response data
        VJsonRequest vJsonRequest = new VJsonRequest(getActivity(), ApiParams.MYAPPOINTMENTS_URL, params,
                new VJsonRequest.VJsonResponce() {
                    @Override
                    public void VResponce(String responce) {
                        Gson gson = new Gson();
                        Type type = new TypeToken<List<AppointmentModel>>(){}.getType();
                        List<AppointmentModel> initialAppt = gson.fromJson(responce, type);

                        if(initialAppt.size() > 0){
                            for (AppointmentModel apptModel : initialAppt){
                                idList.add(apptModel.getId());
                                userIdList.add(apptModel.getUser_id());
                                apptDateList.add(apptModel.getAppointment_date());
                                starttimeList.add(apptModel.getStart_time());
                                timetokenList.add(apptModel.getTime_token());
                                statusList.add(apptModel.getStatus());
                                appnameList.add(apptModel.getApp_name());
                                appemailList.add(apptModel.getApp_email());
                                createdatList.add(apptModel.getCreated_at());
                                contimeList.add(apptModel.getBus_con_time());
                                busTitleList.add(apptModel.getBus_title());
                                busSlugList.add(apptModel.getBus_slug());
                                doctNameList.add(apptModel.getDoct_name());
                                doctSpecialtyList.add(apptModel.getDoct_speciality());
                                paymentRefList.add(apptModel.getPayment_ref());
                                imageList.add(apptModel.getImage());
                                cancelStatusList.add(apptModel.getCancel_status());
                            }


                            for(int i = 0 ; i < idList.size() ; i++) {
                                if(statusList.get(i).equalsIgnoreCase("1") ||
                                        cancelStatusList.get(i).equalsIgnoreCase("1")){
                                    AppointmentModel appointmentModel = new AppointmentModel();
                                    appointmentModel.setId(idList.get(i));
                                    appointmentModel.setUser_id(userIdList.get(i));
                                    appointmentModel.setAppointment_date(apptDateList.get(i));
                                    appointmentModel.setStart_time(starttimeList.get(i));
                                    appointmentModel.setTime_token(timetokenList.get(i));
                                    appointmentModel.setStatus(statusList.get(i));
                                    appointmentModel.setApp_name(appnameList.get(i));
                                    appointmentModel.setApp_email(appemailList.get(i));
                                    appointmentModel.setCreated_at(createdatList.get(i));
                                    appointmentModel.setBus_con_time(contimeList.get(i));
                                    appointmentModel.setBus_title(busTitleList.get(i));
                                    appointmentModel.setBus_slug(busSlugList.get(i));
                                    appointmentModel.setDoct_name(doctNameList.get(i));
                                    appointmentModel.setDoct_speciality(doctSpecialtyList.get(i));
                                    appointmentModel.setPayment_ref(paymentRefList.get(i));
                                    appointmentModel.setImage(imageList.get(i));
                                    appointmentModel.setCancel_status(cancelStatusList.get(i));

                                    appointmentArray.add(appointmentModel);


                                    if(statusList.get(i).equalsIgnoreCase("1") &&
                                            cancelStatusList.get(i).equalsIgnoreCase("0")){
                                        ActivityMain.hospitalLists.add(busTitleList.get(i));
                                    }
                                }
                            }

                            //ARRANGE TIME
                            Collections.sort(appointmentArray, new Comparator<AppointmentModel>() {
                                @Override
                                public int compare(AppointmentModel o1, AppointmentModel o2) {
                                    if (o1.getAppointment_date() == null || o2.getAppointment_date() == null)
                                        return 0;
                                    return o1.getAppointment_date().compareTo(o2.getAppointment_date());
                                }
                            });
                            Collections.reverse(appointmentArray);
                            myAppointmentAdapter.notifyDataSetChanged();
                        }

                    }

                    @Override
                    public void VError(String responce) {
                        common.setToastMessage(responce);
                    }
                });

        return view;
    }
}