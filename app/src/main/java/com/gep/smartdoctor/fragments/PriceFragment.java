package com.gep.smartdoctor.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.gep.smartdoctor.Config.ApiParams;
import com.gep.smartdoctor.activities.ActivityDetailsClinics;
import com.gep.smartdoctor.R;
import com.gep.smartdoctor.activities.ActivityTimeSlot;
import com.gep.smartdoctor.models.ActiveModels;
import com.gep.smartdoctor.models.BusinessModel;
import com.gep.smartdoctor.models.ServicesModel;
import com.gep.smartdoctor.util.CommonClass;
import com.gep.smartdoctor.util.VJsonRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import com.gep.smartdoctor.adapters.ServiceChargeAdapter;

/**
 * Created by LENOVO on 7/10/2016.
 */
public class PriceFragment extends Fragment {

    private CommonClass common;
    public static ArrayList<ServicesModel> mServiceArray;
    private ServiceChargeAdapter serviceChargeAdapter;
    private RecyclerView businessRecyclerView;
    public static int getServiceId;

    private Activity act;
    private Bundle args;
    private BusinessModel selected_business;
//    View emptyLayout;
    Button btnBook, btnCart;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_list_service, container, false);
        act = getActivity();
        setRetainInstance(true);

        common = new CommonClass(act);
        selected_business = ActiveModels.BUSINESS_MODEL;

        args = this.getArguments();
        mServiceArray = new ArrayList<ServicesModel>();
        btnBook = (Button) rootView.findViewById(R.id.btnbook);
        btnCart = (Button) rootView.findViewById(R.id.btn_cart);
        btnBook.setEnabled(false);
        btnBook.setClickable(false);
        btnCart.setEnabled(false);
        btnCart.setClickable(false);

        bundView(rootView);
//        emptyLayout = (View)rootView.findViewById(R.id.emptyLayout);
//        emptyLayout.setVisibility(View.GONE);

        btnCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ActivityDetailsClinics) getActivity()).showCartLayout();
            }
        });

        btnBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActiveModels.LIST_SERVICES_MODEL = mServiceArray;
                Intent intent = new Intent(getActivity(), ActivityTimeSlot.class);
                Bundle b = new Bundle();
                b.putInt("serviceId", getServiceId);
                intent.putExtras(b);
                startActivity(intent);
            }
        });

        loadData();

        return rootView;
    }

    public void checkBookBtn(boolean isChecked, String serviceId){
        if(!isChecked){
            btnBook.setEnabled(false);
            btnBook.setClickable(false);
            btnCart.setEnabled(false);
            btnCart.setClickable(false);
            btnBook.setBackground(getActivity().getResources().getDrawable(R.drawable.rounded_btn_gray));
            btnCart.setBackground(getActivity().getResources().getDrawable(R.drawable.icon_cart_gray));
        } else {
            btnBook.setEnabled(true);
            btnBook.setClickable(true);
            btnCart.setEnabled(true);
            btnCart.setClickable(true);
            btnBook.setBackground(getActivity().getResources().getDrawable(R.drawable.rounded_button));
            btnCart.setBackground(getActivity().getResources().getDrawable(R.drawable.icon_cart));
        }
    }

    public void bundView(View rootView) {
        businessRecyclerView = (RecyclerView) rootView.findViewById(R.id.rv_list);
        final LinearLayoutManager layoutManager
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        businessRecyclerView.setLayoutManager(layoutManager);

        serviceChargeAdapter = new ServiceChargeAdapter(getActivity(), mServiceArray, PriceFragment.this);
        businessRecyclerView.setAdapter(serviceChargeAdapter);
    }

    public void loadData() {
        HashMap<String, String> params = new HashMap<>();
        params.put("bus_id", selected_business.getBus_id());

        // this class for handle request response thread and return response data
        VJsonRequest vJsonRequest = new VJsonRequest(getActivity(), ApiParams.BUSINESS_SERVICES, params,
                new VJsonRequest.VJsonResponce() {
                    @Override
                    public void VResponce(String responce) {

                        Gson gson = new Gson();
                        Type listType = new TypeToken<List<ServicesModel>>() {
                        }.getType();
                        mServiceArray.clear();
                        mServiceArray.addAll((Collection<? extends ServicesModel>) gson.fromJson(responce, listType));
                        serviceChargeAdapter.notifyDataSetChanged();
                        if (mServiceArray.size() > 0) {
                            serviceChargeAdapter.notifyDataSetChanged();
//                            emptyLayout.setVisibility(View.GONE);

                        } else {
//                            emptyLayout.setVisibility(View.VISIBLE);
                            //Toast.makeText(act, "data not found", Toast.LENGTH_SHORT).show();
                        }

//                        Log.d("jbeePrice", responce);
                    }

                    @Override
                    public void VError(String responce) {
                        //progressBar1.setVisibility(View.GONE);
                    }
                });

    }

}
