package com.gep.smartdoctor.fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.gep.smartdoctor.Config.ApiParams;
import com.gep.smartdoctor.R;
import com.gep.smartdoctor.activities.ActivityAmbulance;
import com.gep.smartdoctor.activities.ActivityEmergencyHotlines;
import com.gep.smartdoctor.activities.ActivityMain;
import com.gep.smartdoctor.activities.CommonActivity;
import com.gep.smartdoctor.activities.SearchActivity;
import com.gep.smartdoctor.adapters.CategoryAdapter;
import com.gep.smartdoctor.adapters.SliderViewPagerAdapter;
import com.gep.smartdoctor.models.CategoryModel;
import com.gep.smartdoctor.util.CommonClass;
import com.gep.smartdoctor.util.VJsonRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class FragmentHome extends Fragment {

    View rootView, emptyLayout;
    Button btnRetry;
    private ArrayList<CategoryModel> categoryArray;
    private RecyclerView categoryRecyclerView;
    private CategoryAdapter categoryAdapter;
    private ProgressBar progressBar1;
    private RelativeLayout btnAmbulance, btnEmerHotlines;
    private SwipeRefreshLayout swipeRefreshLayout;

    //slider viewpager
    LinearLayout llSlider, searchLayout;
    ViewPager viewPager;
    SliderViewPagerAdapter viewPagerAdapter;
    LinearLayout sliderDotspanel;
    private int dotscount;
    private ImageView[] dots;

    ImageView btnDrawer;
    boolean firstload = true;

//    private boolean isViewShown = false;
//    @Override
//    public void setUserVisibleHint(boolean isVisibleToUser) {
//        super.setUserVisibleHint(isVisibleToUser);
//        if (getView() != null && isVisibleToUser) {
//            isViewShown = true; // fetchdata() contains logic to show data when page is selected mostly asynctask to fill the data
//            initAct();
//        } else {
//            isViewShown = false;
//        } }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState){
        rootView = inflater.inflate(R.layout.fragment_home, container, false);

        initImageSlider();
        categoryArray = new ArrayList<>();

        emptyLayout = (View) rootView.findViewById(R.id.emptyLayout);
        swipeRefreshLayout = rootView.findViewById(R.id.swiperefresh_layout);
        btnRetry = (Button) emptyLayout.findViewById(R.id.btn_retry);
        emptyLayout.setVisibility(View.GONE);
        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimary));


        if(firstload) {
            initAct();
        }

        btnRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initAct();
            }
        });

        initializeRefreshListener();

        return rootView;
    }

    private void initAct(){
        if(CommonClass.is_internet_connected(getActivity())){
            bindView();
            emptyLayout.setVisibility(View.GONE);
            ((ActivityMain) getActivity()).tabLayout.setVisibility(View.VISIBLE);
        } else{
            emptyLayout.setVisibility(View.VISIBLE);
            ((ActivityMain) getActivity()).tabLayout.setVisibility(View.GONE);
        }
    }

    private void bindView(){
        searchLayout = (LinearLayout) rootView.findViewById(R.id.search_layout);
        categoryRecyclerView = (RecyclerView) rootView.findViewById(R.id.rv_artist);
        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 3);
        categoryRecyclerView.setLayoutManager(layoutManager);

        categoryAdapter = new CategoryAdapter(getActivity(), categoryArray);
        categoryRecyclerView.setAdapter(categoryAdapter);
        progressBar1 = (ProgressBar) rootView.findViewById(R.id.progressBar1);
        CommonActivity.setProgressBarAnimation(progressBar1);

        btnAmbulance = (RelativeLayout) rootView.findViewById(R.id.btn_ambulance);
        btnEmerHotlines = (RelativeLayout) rootView.findViewById(R.id.btn_emergency_hotlines);

        btnDrawer = (ImageView) rootView.findViewById(R.id.btn_drawer);

        btnDrawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ActivityMain) getActivity()).drawer.openDrawer(Gravity.START);
            }
        });

        btnAmbulance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ActivityAmbulance.class);
                startActivity(intent);
            }
        });

        btnEmerHotlines.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ActivityEmergencyHotlines.class);
                startActivity(intent);
            }
        });

        searchLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SearchActivity.class);
                startActivity(intent);
            }
        });

        if(firstload) {
            firstload = false;
            loadData();
        }
    }

    private void initImageSlider(){
        llSlider = (LinearLayout) rootView.findViewById(R.id.layout_slider);
        viewPager = (ViewPager) rootView.findViewById(R.id.viewPager);
        sliderDotspanel = (LinearLayout) rootView.findViewById(R.id.SliderDots);
        viewPagerAdapter = new SliderViewPagerAdapter(getActivity());
        viewPager.setAdapter(viewPagerAdapter);
        setSliderViews();

        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);

        int screenWidth = dm.widthPixels;
        int screenHeight = dm.heightPixels;

        LinearLayout.LayoutParams params= new LinearLayout.LayoutParams(screenWidth, screenHeight/4);
        llSlider.setLayoutParams(params);
    }

    private void setSliderViews(){
        dotscount = viewPagerAdapter.getCount();
        dots = new ImageView[dotscount];

        for(int i = 0; i < dotscount; i++){
            dots[i] = new ImageView(getActivity());
            dots[i].setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.dot_notactive));

            LinearLayout.LayoutParams paramsSliderImage = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            paramsSliderImage.setMargins(8, 0, 8, 0);
            sliderDotspanel.addView(dots[i], paramsSliderImage);
        }
        dots[0].setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.dot_active));

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                for(int i = 0; i< dotscount; i++){
                    dots[i].setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.dot_notactive));
                }
                dots[position].setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.dot_active));
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new MyTimerTask(), 2000, 4000);
    }

    int position = 1;
    public class MyTimerTask extends TimerTask {
        @Override
        public void run() {
            if (getActivity() != null) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        position = viewPager.getCurrentItem() + 1;
                        if (position >= viewPagerAdapter.getCount()) {
                            position = 0;
                        }
                        viewPager.setCurrentItem(position);
                    }
                });
            }
        }

    }

    private void initializeRefreshListener() {
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // This method gets called when user pull for refresh,
                // You can make your API call here,
                // We are using adding a delay for the moment
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if(swipeRefreshLayout.isRefreshing()) {
                            loadData();
                            swipeRefreshLayout.setRefreshing(false);
                        }
                    }
                }, 2000);
            }
        });
    }

    // load category in gridview
    public void loadData() {
        // this class for handle request response thread and return response data
        VJsonRequest vJsonRequest = new VJsonRequest(getActivity(), ApiParams.CATEGORY_LIST,
                new VJsonRequest.VJsonResponce() {
                    @Override
                    public void VResponce(String responce) {

                        Gson gson = new Gson();
                        Type listType = new TypeToken<List<CategoryModel>>() {
                        }.getType();
                        categoryArray.clear();

                        categoryArray.addAll((Collection<? extends CategoryModel>) gson.fromJson(responce, listType));
                        categoryAdapter.notifyDataSetChanged();
                        progressBar1.setVisibility(View.GONE);

                    }

                    @Override
                    public void VError(String responce) {
                        progressBar1.setVisibility(View.GONE);
                    }
                });

    }
}
