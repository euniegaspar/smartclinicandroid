package com.gep.smartdoctor.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gep.smartdoctor.activities.MapActivity;
import com.gep.smartdoctor.R;
import com.gep.smartdoctor.models.ActiveModels;
import com.gep.smartdoctor.models.BusinessModel;
import com.gep.smartdoctor.util.CommonClass;

/**
 * Created by LENOVO on 7/10/2016.
 */
public class DetailsFragment extends Fragment {

    private CommonClass common;
    private Activity act;
    private Bundle args;
    private BusinessModel selected_business;
    private ImageView imageView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_about_details, container, false);
        act = getActivity();
        common = new CommonClass(act);
        selected_business = ActiveModels.BUSINESS_MODEL;
        imageView=rootView.findViewById(R.id.bannerImage);

        TextView textDescription = (TextView) rootView.findViewById(R.id.textDescription);
        textDescription.setText(Html.fromHtml(selected_business.getBus_description()));
        TextView tvClinicName = (TextView) rootView.findViewById(R.id.tv_clinic_name);
        TextView txtPhone = (TextView) rootView.findViewById(R.id.textPhone);
        TextView txtAddress = (TextView) rootView.findViewById(R.id.textAddress);
        tvClinicName.setText(selected_business.getBus_title());
        txtPhone.setText(selected_business.getBus_contact());
        txtAddress.setText(selected_business.getBus_google_street());

        args = this.getArguments();
        LinearLayout locationBtn = (LinearLayout) rootView.findViewById(R.id.locationBtn);
        locationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), MapActivity.class);
                getActivity().startActivity(intent);
            }
        });

        return rootView;
    }

}
