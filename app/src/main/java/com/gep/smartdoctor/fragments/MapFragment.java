package com.gep.smartdoctor.fragments;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gep.smartdoctor.Config.ApiParams;
import com.gep.smartdoctor.activities.ActivityClinicList;
import com.gep.smartdoctor.activities.ActivityDetailsClinics;
import com.gep.smartdoctor.models.ActiveModels;
import com.gep.smartdoctor.models.BusinessModel;
import com.gep.smartdoctor.util.CommonClass;
import com.gep.smartdoctor.util.GPSTracker;
import com.gep.smartdoctor.util.VJsonRequest;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.gep.smartdoctor.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by LENOVO on 7/10/2016.
 */
public class MapFragment extends Fragment implements OnMapReadyCallback, GoogleMap.OnInfoWindowClickListener {

    private CommonClass common;
    private ArrayList<BusinessModel> postItems;
    String PREF_BUSINESS = "pref_business";
    private GPSTracker gpsTracker;
    private Double cur_latitude, cur_longitude;

    private Activity act;
    private GoogleMap mMap;

    BusinessModel jObj;

//    @Override
//    public void setUserVisibleHint(boolean isVisibleToUser) {
//        super.setUserVisibleHint(isVisibleToUser);
//        if (isVisibleToUser) {
//            // animate here
//            Log.e("Map", "animate here");
//
//        } else {
//            Log.e("Map", "fragment is no longer visible");
//            // fragment is no longer visible
//        }
//    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_map, container, false);
        act = getActivity();

        postItems = new ArrayList<>();
        common = new CommonClass(act);
        loadGetResult();
        return rootView;
    }


    @Override
    public void onResume() {
        super.onResume();
    }


    boolean firstload = true;

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (firstload) {
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            mMap.setMyLocationEnabled(true);
            firstload = false;
        }

        mMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
            @Override
            public void onMyLocationChange(Location location) {

                CameraUpdate center = CameraUpdateFactory.newLatLng(new LatLng(location.getLatitude(), location.getLongitude()));
                CameraUpdate zoom = CameraUpdateFactory.zoomTo(11);
                mMap.clear();

                MarkerOptions mp = new MarkerOptions();

                mp.position(new LatLng(location.getLatitude(), location.getLongitude()));

                mp.title("My current location");

                mMap.addMarker(mp);
                mMap.moveCamera(center);
                mMap.animateCamera(zoom);

                if (cur_latitude != null && cur_longitude != null) {
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                            new LatLng(cur_latitude, cur_longitude), 12));

                    // You can customize the marker image using images bundled with
                    // your app, or dynamically generated bitmaps.
                    mMap.addMarker(new MarkerOptions()
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker_icon))
                            .anchor(0.0f, 1.0f) // Anchors the marker on the bottom left

                            //  .title(cur_latitude.toString())
                            //  .title(cur_longitude.toString())

                            .position(new LatLng(cur_latitude, cur_longitude)));
                }


                if (postItems != null) {
                    for (int i = 0; i < postItems.size(); i++) {

                        /*BusinessModel */jObj = postItems.get(i);
                        Double lat = Double.parseDouble(jObj.getBus_latitude());
                        Double lon = Double.parseDouble(jObj.getBus_longitude());
                        mMap.addMarker(new MarkerOptions()
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.flag_icon))
                                .anchor(0.0f, 1.0f) // Anchors the marker on the bottom left
                                .snippet(jObj.getBus_google_street() + "\n" + jObj.getBus_contact())
                                .title(jObj.getBus_title())
//                                .title(jObj.getBus_latitude())
                                .position(new LatLng(lat, lon)));

                        info_map();

                    }
                }

                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                mMap.setMyLocationEnabled(false); //this will prevent restarting the map
            }
        });

        mMap.setOnInfoWindowClickListener(this);

//        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
//            @Override
//            public boolean onMarkerClick(Marker marker) {
////                ActiveModels.BUSINESS_MODEL = postItems.get(marker.getPosition());
////                Intent intent = new Intent(getActivity(), ActivityDetailsClinics.class);
////                getActivity().startActivity(intent);
//
//
//                String title = marker.getTitle();
//                BusinessModel businessModel = null;
//                for(int i = 0;i < postItems.size();i++) {
//                    if(postItems.get(i).getBus_title().equals(title))
//                        businessModel = postItems.get(i);
//                }
//                Intent intent = new Intent(getActivity(), ActivityDetailsClinics.class);
//                getActivity().startActivity(intent);
//
//                return false;
//            }
//        });
    }

    // get business data from api
    public void loadGetResult() {

        HashMap<String, String> params = new HashMap<>();
        if (getArguments() != null && getArguments().containsKey("search"))
            params.put("search", getArguments().getString("search"));

        //if (type !=null && !type.equalsIgnoreCase("0"))
        //    params.put("type","page_type_id");

        if (getArguments().containsKey("cat_id"))
            params.put("cat_id", getArguments().getString("cat_id"));
        if (getArguments().containsKey("locality_id"))
            params.put("locality_id", getArguments().getString("locality_id"));
        int radius = 60;
        if (common.containKeyInSession("radius")) {
            radius = common.getSessionInt("radius");
            if (radius <= 0) {
                radius = 60;
            }
        }

        if (getArguments().containsKey("lat"))
            params.put("lat", getArguments().getString("lat"));
        if (getArguments().containsKey("lon"))
            params.put("lon", getArguments().getString("lon"));

        params.put("rad", String.valueOf(radius));

        // this class for handle request response thread and return response data
        VJsonRequest vJsonRequest = new VJsonRequest(getActivity(), ApiParams.BUSINESS_LIST, params,
                new VJsonRequest.VJsonResponce() {
                    @Override
                    public void VResponce(String responce) {

                        Gson gson = new Gson();
                        Type listType = new TypeToken<List<BusinessModel>>() {
                        }.getType();
                        ArrayList<BusinessModel> arraylist = (ArrayList<BusinessModel>) gson.fromJson(responce, listType);


                        postItems.clear();


                        postItems.addAll(arraylist);

                        com.google.android.gms.maps.MapFragment mapFragment = (com.google.android.gms.maps.MapFragment) act.getFragmentManager()
                                .findFragmentById(R.id.map);
                        mapFragment.getMapAsync(MapFragment.this);

                    }

                    @Override
                    public void VError(String responce) {

                    }
                });
    }

    public void  info_map(){

        gpsTracker = new GPSTracker(act);
        if (gpsTracker.canGetLocation()) {
            if (gpsTracker.getLatitude() != 0.0)
                cur_latitude = gpsTracker.getLatitude();
            if (gpsTracker.getLongitude() != 0.0)
                cur_longitude = gpsTracker.getLongitude();
        } else {
            gpsTracker.showSettingsAlert();
        }


    }

    @Override
    public void onInfoWindowClick(Marker marker) {

        String title = marker.getTitle();
        for(int i = 0;i < postItems.size();i++) {
            if(postItems.get(i).getBus_title().equals(title))
                ActiveModels.BUSINESS_MODEL = postItems.get(i);
        }
        Intent intent = new Intent(getActivity(), ActivityDetailsClinics.class);
        getActivity().startActivity(intent);
    }
}