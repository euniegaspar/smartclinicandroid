package com.gep.smartdoctor.fragments;

import android.app.Activity;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gep.smartdoctor.R;
import com.gep.smartdoctor.Config.ApiParams;
import com.gep.smartdoctor.dialogues.PhotosDialog;
import com.gep.smartdoctor.models.ActiveModels;
import com.gep.smartdoctor.models.BusinessModel;
import com.gep.smartdoctor.models.PhotosModel;
import com.gep.smartdoctor.util.CommonClass;
import com.gep.smartdoctor.util.RecyclerItemClickListener;
import com.gep.smartdoctor.util.VJsonRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import com.gep.smartdoctor.adapters.PhotosAdapter;

/**
 * Created by LENOVO on 7/10/2016.
 */
public class PhotosFragment extends Fragment {

    private CommonClass common;
    private ArrayList<PhotosModel> mPhotoArray;
    private PhotosAdapter photoAdapter;
    private RecyclerView businessRecyclerView;

    private Activity act;
    private Bundle args;
    private BusinessModel selected_business;

    View emptyLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_photos, container, false);

        emptyLayout = (View)rootView.findViewById(R.id.emptyLayout);
        emptyLayout.setVisibility(View.GONE);

        act = getActivity();

        common = new CommonClass(act);
        selected_business = ActiveModels.BUSINESS_MODEL;

        args = this.getArguments();

        mPhotoArray = new ArrayList<>();
        bindView(rootView);
        loadData();

        return rootView;
    }

    // get view from xml
    public void bindView(View rootView) {
        businessRecyclerView = (RecyclerView) rootView.findViewById(R.id.rv_list);
        final GridLayoutManager layoutManager
                = new GridLayoutManager(getActivity(), 2);
        businessRecyclerView.setLayoutManager(layoutManager);

        photoAdapter = new PhotosAdapter(getActivity(), mPhotoArray);
        businessRecyclerView.setAdapter(photoAdapter);


        businessRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {

            @Override
            public void onItemClick(View view, int position) {
                PhotosDialog reviewInfoDialog = new PhotosDialog(getActivity(), mPhotoArray.get(position));
                reviewInfoDialog.show();
            }
        }));
    }

    // load bussiness photo from api
    public void loadData() {
        HashMap<String, String> params = new HashMap<>();
        params.put("bus_id", selected_business.getBus_id());

        // this class for handle request response thread and return response data
        VJsonRequest vJsonRequest = new VJsonRequest(getActivity(), ApiParams.BUSINESS_PHOTOS, params,
                new VJsonRequest.VJsonResponce() {
                    @Override
                    public void VResponce(String responce) {

                        Gson gson = new Gson();
                        Type listType = new TypeToken<List<PhotosModel>>() {
                        }.getType();
                        mPhotoArray.clear();
                        mPhotoArray.addAll((Collection<? extends PhotosModel>) gson.fromJson(responce, listType));
                        photoAdapter.notifyDataSetChanged();
                        //progressBar1.setVisibility(View.GONE);

                        if (mPhotoArray.size() > 0) {
                            photoAdapter.notifyDataSetChanged();
                            emptyLayout.setVisibility(View.GONE);

                        } else {
                            emptyLayout.setVisibility(View.VISIBLE);
                            //Toast.makeText(act, "data not found", Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void VError(String responce) {
                        //progressBar1.setVisibility(View.GONE);
                    }
                });
    }



}
