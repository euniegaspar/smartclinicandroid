package com.gep.smartdoctor.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gep.smartdoctor.R;
import com.gep.smartdoctor.Config.ApiParams;
import com.gep.smartdoctor.activities.ActivityMain;
import com.gep.smartdoctor.models.ActiveModels;
import com.gep.smartdoctor.models.AppointmentModel;
import com.gep.smartdoctor.models.BusinessModel;
import com.gep.smartdoctor.models.ReviewsModel;
import com.gep.smartdoctor.util.CommonClass;
import com.gep.smartdoctor.util.VJsonRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import com.gep.smartdoctor.adapters.ReviewsAdapter;

/**
 * Created by LENOVO on 7/10/2016.
 */
public class ReviewsFragment extends Fragment {

    private CommonClass common;
    private ArrayList<AppointmentModel> appointmentArray;
    private ArrayList<ReviewsModel> mReviewsArray;
    private ReviewsAdapter reviewsAdapter;
    private RecyclerView businessRecyclerView;

    private Activity act;
    private Bundle args;
    private BusinessModel selected_business;

    private RatingBar ratingbar;
    private EditText etComment;
    private ImageView btnAddReview;
//    private ReviewsAdapter reviewsAdapter;
    private RelativeLayout topLayout;
    protected AlertDialog alertDialog;
    View emptyLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_reviews, container, false);
        act = getActivity();

        common = new CommonClass(act);
        selected_business = ActiveModels.BUSINESS_MODEL;

        args = this.getArguments();

        appointmentArray = new ArrayList<>();
        mReviewsArray = new ArrayList<ReviewsModel>();
        bundView(rootView);
        emptyLayout = (View)rootView.findViewById(R.id.emptyLayout);
        emptyLayout.setVisibility(View.GONE);

        loadData();

        topLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (common.is_user_login()){
                    if(ActivityMain.hospitalLists.contains(selected_business.getBus_title())){
                        openReviewDialog(v);
                    } else{
                        common.setToastMessage(getResources().getString(R.string.you_have_no_appt_clinic));
                    }
                } else{
                    common.setToastMessage(getResources().getString(R.string.you_are_not_logged_in));
                }
            }
        });

        return rootView;
    }

    // load view from xml
    public void bundView(View rootView) {
        businessRecyclerView = (RecyclerView) rootView.findViewById(R.id.rv_list);
        final LinearLayoutManager layoutManager
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        businessRecyclerView.setLayoutManager(layoutManager);

        reviewsAdapter = new ReviewsAdapter(getActivity(), mReviewsArray);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(businessRecyclerView.getContext(),
                layoutManager.getOrientation());
        businessRecyclerView.addItemDecoration(dividerItemDecoration);
        businessRecyclerView.setAdapter(reviewsAdapter);

        topLayout = (RelativeLayout) rootView.findViewById(R.id.topLayout);
        btnAddReview = (ImageView) rootView.findViewById(R.id.addreview);

        if(common.is_user_login()){
            HashMap<String, String> params = new HashMap<>();
            params.put("user_id", common.get_user_id());

            // this class for handle request response thread and return response data
            VJsonRequest vJsonRequest = new VJsonRequest(getActivity(), ApiParams.MYAPPOINTMENTS_URL, params,
                    new VJsonRequest.VJsonResponce() {
                        @Override
                        public void VResponce(String responce) {

                            Gson gson = new Gson();
                            Type listType = new TypeToken<List<AppointmentModel>>() {
                            }.getType();
                            appointmentArray.clear();
                            appointmentArray.addAll((Collection<? extends AppointmentModel>) gson.fromJson(responce, listType));

                            Log.d("jbeeReview", responce);
                        }

                        @Override
                        public void VError(String responce) {
                            common.setToastMessage(responce);
                        }
                    });
        }
    }

    public void loadData() {
        HashMap<String, String> params = new HashMap<>();
        params.put("bus_id", selected_business.getBus_id());

        VJsonRequest vJsonRequest = new VJsonRequest(getActivity(), ApiParams.BUSINESS_REVIEWS, params,
                new VJsonRequest.VJsonResponce() {
                    @Override
                    public void VResponce(String responce) {

                        Gson gson = new Gson();
                        Type listType = new TypeToken<List<ReviewsModel>>() {
                        }.getType();
                        mReviewsArray.clear();
                        mReviewsArray.addAll((Collection<? extends ReviewsModel>) gson.fromJson(responce, listType));
                        reviewsAdapter.notifyDataSetChanged();
                        //progressBar1.setVisibility(View.GONE);


                        if (mReviewsArray.size() > 0) {
                            emptyLayout.setVisibility(View.GONE);
                        } else {
                            emptyLayout.setVisibility(View.VISIBLE);
                        }
                    }

                    @Override
                    public void VError(String responce) {
                        //progressBar1.setVisibility(View.GONE);
                    }
                });


    }

    // show review dialog
    public void openReviewDialog(final View view) {
        final Dialog dialog = new Dialog(view.getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_review);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        etComment = (EditText) dialog.findViewById(R.id.et_comment);
        ratingbar = (RatingBar) dialog.findViewById(R.id.ratingBar1);
        final TextView btnAddReview = (TextView) dialog.findViewById(R.id.btn_add_review);
        TextView btnCancel = (TextView) dialog.findViewById(R.id.btn_cancel);
        etComment.setFilters(CommonClass.getFilterWithoutSpecialChars(250));
        btnAddReview.setEnabled(false);
        btnAddReview.setTextColor(view.getResources().getColor(R.color.colorGray));

        etComment.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                if(etComment.getText().length() > 0) {
                    btnAddReview.setEnabled(true);
                    btnAddReview.setTextColor(view.getResources().getColor(R.color.colorPrimary));
                } else{
                    btnAddReview.setEnabled(false);
                    btnAddReview.setTextColor(view.getResources().getColor(R.color.colorGray));
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }
        });

        btnAddReview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(etComment.getText())) {
                    sendData();
                }else {
                    etComment.setError(getString(R.string.required));
                }
                dialog.dismiss();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });

        dialog.show();
    }

    // send review data to server
    public void sendData() {
        HashMap<String, String> params = new HashMap<>();

        params.put("bus_id", selected_business.getBus_id());
        params.put("user_id", common.getSession(ApiParams.COMMON_KEY));
        params.put("reviews", etComment.getText().toString()/*.replaceAll("[^a-zA-Z0-9]"," ")*/);
        params.put("rating", String.valueOf(ratingbar.getRating()));

        VJsonRequest vJsonRequest = new VJsonRequest(getActivity(), ApiParams.ADD_BUSINESS_REVIEWS, params,
                new VJsonRequest.VJsonResponce() {
                    @Override
                    public void VResponce(String responce) {

                        Gson gson = new Gson();
                        Type listType = new TypeToken<ReviewsModel>() {
                        }.getType();
                        mReviewsArray.add((ReviewsModel) gson.fromJson(responce, listType));

                        Log.d("jbeeReviews", responce);
                        loadData();

                        if (alertDialog != null) {
                            alertDialog.dismiss();
                            alertDialog = null;
                        }

                    }

                    @Override
                    public void VError(String responce) {
                        //progressBar1.setVisibility(View.GONE);
                    }
                });


    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

}
