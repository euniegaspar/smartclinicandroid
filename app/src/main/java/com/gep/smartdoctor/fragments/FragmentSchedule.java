package com.gep.smartdoctor.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gep.smartdoctor.Config.ApiParams;
import com.gep.smartdoctor.R;
import com.gep.smartdoctor.activities.ActivityMain;
import com.gep.smartdoctor.adapters.ScheduleApptAdapter;
import com.gep.smartdoctor.models.AppointmentModel;
import com.gep.smartdoctor.models.ScheduleItemModel;
import com.gep.smartdoctor.util.CommonClass;
import com.gep.smartdoctor.util.VJsonRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;

public class FragmentSchedule extends Fragment {

    View view;
    CommonClass common;
    public static RecyclerView rvScheduleItem;
    public static ScheduleApptAdapter scheduleApptAdapter;
//    String response;
    public static String responseString;
    public static ArrayList<ScheduleItemModel> scheduleItemModelArray;
    public static ArrayList<String> apptString;


//    private static FragmentSchedule instance;
//    public static FragmentSchedule getInstance() {
//        if (instance == null) instance = new FragmentSchedule();
//        return instance;
//    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_schedule, container, false);
        common = new CommonClass(getActivity());

        apptString = new ArrayList<>();
        scheduleItemModelArray = new ArrayList<>();

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        rvScheduleItem = (RecyclerView) view.findViewById(R.id.rv_sched_item);
        rvScheduleItem.setLayoutManager(layoutManager);
        scheduleApptAdapter = new ScheduleApptAdapter(getActivity(), scheduleItemModelArray /*, responseString, appointmentArray*/);
        rvScheduleItem.setAdapter(scheduleApptAdapter);

        fetchData(getActivity(), common.get_user_id());

        return view;
    }

    public static void fetchData(Activity context, String userId){
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", userId);

        VJsonRequest vJsonRequest = new VJsonRequest(context, ApiParams.MYAPPOINTMENTS_URL, params,
                new VJsonRequest.VJsonResponce() {
                    @Override
                    public void VResponce(String responce) {
                        responseString = responce;
                        scheduleItemModelArray.clear();
                        FragmentAppointment.calendarEvents.clear();

                        Gson gson = new Gson();
                        Type type = new TypeToken<List<AppointmentModel>>(){}.getType();
                        List<AppointmentModel> appt = gson.fromJson(responseString, type);

                        for (AppointmentModel appointmentModel : appt){
                            apptString.add(appointmentModel.getAppointment_date());
                        }

                        //filters duplicate item
                        HashSet<String> hashSet = new HashSet<String>();
                        hashSet.addAll(apptString);
                        apptString.clear();
                        apptString.addAll(hashSet);

                            //adding item
                            for(int i = 0 ; i < apptString.size() ; i++) {
                                ScheduleItemModel scheduleItemModel = new ScheduleItemModel();
                                scheduleItemModel.setScheduleDateID(String.valueOf(i));
                                scheduleItemModel.setScheduleItemTitle(apptString.get(i));
                                scheduleItemModel.setResponseString(responseString);

                                scheduleItemModelArray.add(scheduleItemModel);
                                scheduleApptAdapter.notifyDataSetChanged();

                                //For filtering
                                FragmentAppointment.schedID.add(String.valueOf(i));
                                FragmentAppointment.schedTitle.add(apptString.get(i));
                                FragmentAppointment.schedResponse.add(responseString);
                            }

                        //ARRANGE BY DATE
                        Collections.sort(scheduleItemModelArray, new Comparator<ScheduleItemModel>() {
                            @Override
                            public int compare(ScheduleItemModel o1, ScheduleItemModel o2) {
                                if (o1.getScheduleItemTitle() == null || o2.getScheduleItemTitle() == null)
                                    return 0;
                                return o1.getScheduleItemTitle().compareTo(o2.getScheduleItemTitle());
                            }
                        });

                        Log.d("jbeeAppointment", responce);
                    }

                    @Override
                    public void VError(String responce) {
//                        common.setToastMessage(responce);
                    }
                });
    }
}