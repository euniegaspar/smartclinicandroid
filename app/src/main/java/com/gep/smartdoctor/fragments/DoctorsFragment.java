package com.gep.smartdoctor.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gep.smartdoctor.R;
import com.gep.smartdoctor.Config.ApiParams;
import com.gep.smartdoctor.models.ActiveModels;
import com.gep.smartdoctor.models.BusinessModel;
import com.gep.smartdoctor.models.DoctorModel;
import com.gep.smartdoctor.util.CommonClass;
import com.gep.smartdoctor.util.RecyclerItemClickListener;
import com.gep.smartdoctor.util.VJsonRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import com.gep.smartdoctor.adapters.DoctorAdapter;
import com.gep.smartdoctor.dialogues.DoctorInfoDialog;

/**
 * Created by LENOVO on 7/10/2016.
 */
public class DoctorsFragment extends Fragment {

    private CommonClass common;
    private Activity act;
    private Bundle args;
    private BusinessModel selected_business;
    private Context context;

    public static ArrayList<DoctorModel> mDoctorArray;
    private DoctorAdapter doctorAdapter;
    private RecyclerView recyclerView;

    View emptyLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_doctors, container, false);
        act = getActivity();
        common = new CommonClass(act);
        selected_business = ActiveModels.BUSINESS_MODEL;
        mDoctorArray = new ArrayList<DoctorModel>();

        recyclerView = (RecyclerView) rootView.findViewById(R.id.rv_list);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);

        emptyLayout = (View)rootView.findViewById(R.id.emptyLayout);
        emptyLayout.setVisibility(View.GONE);

        doctorAdapter = new DoctorAdapter(getActivity(), mDoctorArray);
        recyclerView.setAdapter(doctorAdapter);
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {

            @Override
            public void onItemClick(View view, int position) {
                DoctorInfoDialog doctorInfoDialog = new DoctorInfoDialog(getActivity(), mDoctorArray.get(position));
                doctorInfoDialog.show();
            }
        }));

        loadData();
        args = this.getArguments();

        return rootView;
    }

    // get doctor list
    public void loadData() {
        HashMap<String, String> params = new HashMap<>();
        params.put("bus_id", selected_business.getBus_id());

        VJsonRequest vJsonRequest = new VJsonRequest(getActivity(), ApiParams.GET_DOCTORS, params,
                new VJsonRequest.VJsonResponce() {
                    @Override
                    public void VResponce(String responce) {
                        Gson gson = new Gson();
                        Type listType = new TypeToken<List<DoctorModel>>() {
                        }.getType();

                        mDoctorArray.clear();
                        mDoctorArray.addAll((Collection<? extends DoctorModel>) gson.fromJson(responce, listType));
                        doctorAdapter.notifyDataSetChanged();

                        if (mDoctorArray.size() > 0) {
                            doctorAdapter.notifyDataSetChanged();
                            emptyLayout.setVisibility(View.GONE);

                        } else {
                            emptyLayout.setVisibility(View.VISIBLE);
                        }

                    }
                    @Override
                    public void VError(String responce) {
                    }
                });

    }

}
