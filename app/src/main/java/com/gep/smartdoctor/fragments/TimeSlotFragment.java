package com.gep.smartdoctor.fragments;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gep.smartdoctor.Config.ApiParams;
import com.gep.smartdoctor.R;
import com.gep.smartdoctor.activities.ActivityLogin;
import com.gep.smartdoctor.activities.ActivityPersonInfo;
import com.gep.smartdoctor.adapters.TimeSlotAfternoonAdapter;
import com.gep.smartdoctor.adapters.TimeSlotEveningAdapter;
import com.gep.smartdoctor.adapters.TimeSlotMorningAdapter;
import com.gep.smartdoctor.models.ActiveModels;
import com.gep.smartdoctor.models.SlotsModel;
import com.gep.smartdoctor.models.TimeSlotModel;
import com.gep.smartdoctor.util.CommonClass;
import com.gep.smartdoctor.util.RecyclerItemClickListener;
import com.gep.smartdoctor.util.VJsonRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by subhashsanghani on 1/16/17.
 */

public class TimeSlotFragment extends Fragment {

    private static TimeSlotFragment instance;
    private GridLayoutManager lm1, lm2, lm3;
    private ArrayList<SlotsModel> morningTimeSlotArray, afternoonTimeSlotArray, eveningTimeSlotArray;
    private TimeSlotMorningAdapter morningTimeSlotAdapter;
    private TimeSlotAfternoonAdapter afternoonTimeSlotAdapter;
    private TimeSlotEveningAdapter eveningTimeSlotAdapter;
    private TextView btnShowAllMorning, btnShowAllAfternoon, btnShowAllEvening;
    TimeSlotModel timeSlotModel;

    private CommonClass common;
    private RecyclerView rvMorning, rvAfternoon, rvEvening;
    SlotsModel slotsModelMorning, slotsModelAfternoon, slotsModelEvening;
    View rootView;
    TextView tvNoSchedMorning, tvNoSchedAfternoon, tvNoSchedEvening;

    boolean morningShowAll = false, afternoonShowAll = false, eveningShowAll = false;
    public int position;
    public final static int PAGES = 30;

    public static TimeSlotFragment getInstance() {
        if (instance == null) instance = new TimeSlotFragment();
        return instance;
    }

    public static final TimeSlotFragment newInstance(String doctorId, int serviceId, String currentdate, String doctor, String clinic, String clinicLoc/*,
                                                     String weekday, String month, String day, String year*/) {
        TimeSlotFragment f = new TimeSlotFragment();
        Bundle args = new Bundle(1);

        args.putString("doctorId", doctorId);
        args.putInt("serviceId", serviceId);
        args.putString("date", currentdate);
        args.putString("doctor", doctor);
        args.putString("clinic", clinic);
        args.putString("clinicLoc", clinicLoc);
        f.setArguments(args);
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_timeslot, container, false);
        common = new CommonClass(getActivity());

        btnShowAllMorning = (TextView) rootView.findViewById(R.id.btn_morning_showall);
        btnShowAllAfternoon = (TextView) rootView.findViewById(R.id.btn_afternoon_showall);
        btnShowAllEvening = (TextView) rootView.findViewById(R.id.btn_evening_showall);
        tvNoSchedMorning = (TextView) rootView.findViewById(R.id.tv_nomorning_sched);
        tvNoSchedAfternoon = (TextView) rootView.findViewById(R.id.tv_noaftie_sched);
        tvNoSchedEvening = (TextView) rootView.findViewById(R.id.tv_noeve_sched);

        lm1 = new GridLayoutManager(getActivity(), 3, LinearLayoutManager.VERTICAL, false);
        rvMorning = (RecyclerView) rootView.findViewById(R.id.rv_morning_list);
        rvMorning.setLayoutManager(lm1);
        lm2 = new GridLayoutManager(getActivity(), 3, LinearLayoutManager.VERTICAL, false);
        rvAfternoon = (RecyclerView) rootView.findViewById(R.id.rv_afternoon_list);
        rvAfternoon.setLayoutManager(lm2);
        lm3 = new GridLayoutManager(getActivity(), 3, LinearLayoutManager.VERTICAL, false);
        rvEvening = (RecyclerView) rootView.findViewById(R.id.rv_evening_list);
        rvEvening.setLayoutManager(lm3);

        btnShowAllMorning.setVisibility(View.GONE);
        btnShowAllAfternoon.setVisibility(View.GONE);
        btnShowAllEvening.setVisibility(View.GONE);

        String doctorid = getArguments().getString("doctorId");
        String currentdate = getArguments().getString("date");
        loadSlotTask(currentdate, doctorid);
        buttonClicks();

        return rootView;
    }

    // load time slot from api
    public void loadSlotTask(String currentdate, String doctorId) {
        HashMap<String, String> params = new HashMap<>();
        params.put("bus_id", ActiveModels.BUSINESS_MODEL.getBus_id());
        params.put("doct_id", doctorId);
        params.put("date", currentdate);

        // this class for handle request response thread and return response data
        VJsonRequest vJsonRequest = new VJsonRequest(getActivity(), ApiParams.TIME_SLOT_URL, params,
                new VJsonRequest.VJsonResponce() {
                    @Override
                    public void VResponce(String responce) {
                        try {
                            Object json = new JSONTokener(responce).nextValue();
                            if (json instanceof JSONObject) {
                                Gson gson = new Gson();
                                Type listType = new TypeToken<TimeSlotModel>() {
                                }.getType();
                                timeSlotModel = gson.fromJson(responce, listType);

                                try {
                                    initMorningItem();
                                    initAfternoonItem();
                                    initEveningItem();
                                } catch (Exception e){}
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        if(tvNoSchedMorning!=null){
                            try {
                                tvNoSchedMorning.setText(getResources().getString(R.string.no_sched_available));
                                tvNoSchedAfternoon.setText(getResources().getString(R.string.no_sched_available));
                                tvNoSchedEvening.setText(getResources().getString(R.string.no_sched_available));
                            } catch(Exception e){
                            }
                        }
                    }

                    @Override
                    public void VError(String responce) {
                        //progressBar1.setVisibility(View.GONE);
                    }
                });

    }

    private void initMorningItem(){
        morningTimeSlotArray = timeSlotModel.getMorning();
        if(morningTimeSlotArray.size() > 0){
            tvNoSchedMorning.setVisibility(View.GONE);
        }

        if(morningTimeSlotArray.size() > 6){
            btnShowAllMorning.setVisibility(View.VISIBLE);
            morningTimeSlotAdapter = new TimeSlotMorningAdapter(getActivity(), morningTimeSlotArray, 6);
        } else{
            morningTimeSlotAdapter = new TimeSlotMorningAdapter(getActivity(), morningTimeSlotArray);
        }
        rvMorning.setAdapter(morningTimeSlotAdapter);
    }

    private void initAfternoonItem(){
        afternoonTimeSlotArray = timeSlotModel.getAfternoon();
        if(afternoonTimeSlotArray.size() > 0){
            tvNoSchedAfternoon.setVisibility(View.GONE);
        }

        if(afternoonTimeSlotArray.size() > 6){
            btnShowAllAfternoon.setVisibility(View.VISIBLE);
            afternoonTimeSlotAdapter = new TimeSlotAfternoonAdapter(getActivity(), afternoonTimeSlotArray, 6);
        } else{
            afternoonTimeSlotAdapter = new TimeSlotAfternoonAdapter(getActivity(), afternoonTimeSlotArray);
        }
        rvAfternoon.setAdapter(afternoonTimeSlotAdapter);
    }

    private void initEveningItem(){
        eveningTimeSlotArray = timeSlotModel.getEvening();
        if(eveningTimeSlotArray.size() > 0){
            tvNoSchedEvening.setVisibility(View.GONE);
        }

        if(eveningTimeSlotArray.size() > 6){
            btnShowAllEvening.setVisibility(View.VISIBLE);
            eveningTimeSlotAdapter = new TimeSlotEveningAdapter(getActivity(), eveningTimeSlotArray, 6);
        } else{
            eveningTimeSlotAdapter = new TimeSlotEveningAdapter(getActivity(), eveningTimeSlotArray);
        }
        rvEvening.setAdapter(eveningTimeSlotAdapter);
    }

    private void buttonClicks(){
        btnShowAllMorning.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!morningShowAll){
                    morningShowAll = true;
                    btnShowAllMorning.setText(getResources().getString(R.string.show_less));
                    morningTimeSlotAdapter = new TimeSlotMorningAdapter(getActivity(), morningTimeSlotArray);
                    rvMorning.setAdapter(morningTimeSlotAdapter);
                } else{
                    morningShowAll = false;
                    btnShowAllMorning.setText(getResources().getString(R.string.show_all));
                    morningTimeSlotAdapter = new TimeSlotMorningAdapter(getActivity(), morningTimeSlotArray, 6);
                    rvMorning.setAdapter(morningTimeSlotAdapter);
                }
            }
        });

        rvMorning.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                slotsModelMorning = morningTimeSlotArray.get(position);
                SlotsModel chooseMap = morningTimeSlotArray.get(position);
                if (!chooseMap.getIs_booked()) {
                    if (common.is_user_login()) {
                        ActiveModels.SELECTED_SLOT = slotsModelMorning;
                        Intent intent = new Intent(getActivity(), ActivityPersonInfo.class);
                        Bundle b = new Bundle();
                        b.putString("timeslot", slotsModelMorning.getSlot());
                        b.putString("date", getArguments().getString("date"));
                        b.putString("doctor", ActiveModels.DOCTOR_MODEL.getDoct_name());
                        b.putString("ampm", "AM");
                        b.putString("clinic", getArguments().getString("clinic"));
                        b.putString("clinicLoc", getArguments().getString("clinicLoc"));
                        b.putInt("service_id", getArguments().getInt("serviceId"));
                        Log.d("jbeeView", "date = " + getArguments().getString("date"));

                        String date = getArguments().getString("date");
                        String time = slotsModelMorning.getSlot();
                        if(!timeHasPassed( date + " " + time)){
                            common.setToastMessage(getActivity().getResources().getString(R.string.cannot_book));
                        } else {
                            intent.putExtras(b);
                            getActivity().startActivity(intent);
                        }

                    } else {
                        Intent intent = new Intent(getActivity(), ActivityLogin.class);
                        getActivity().startActivity(intent);
                    }
                } else {
                    common.setToastMessage(getActivity().getResources().getString(R.string.already_booked));
                }
            }
        }));


        //AFTERNOON
        btnShowAllAfternoon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!afternoonShowAll){
                    afternoonShowAll = true;
                    btnShowAllAfternoon.setText(getResources().getString(R.string.show_less));
                    afternoonTimeSlotAdapter = new TimeSlotAfternoonAdapter(getActivity(), afternoonTimeSlotArray);
                    rvAfternoon.setAdapter(afternoonTimeSlotAdapter);
                } else{
                    afternoonShowAll = false;
                    btnShowAllAfternoon.setText(getResources().getString(R.string.show_all));
                    afternoonTimeSlotAdapter = new TimeSlotAfternoonAdapter(getActivity(), afternoonTimeSlotArray, 6);
                    rvAfternoon.setAdapter(afternoonTimeSlotAdapter);
                }
            }
        });

        rvAfternoon.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                slotsModelAfternoon = afternoonTimeSlotArray.get(position);
                SlotsModel chooseMap = afternoonTimeSlotArray.get(position);
                if (!chooseMap.getIs_booked()) {
                    if (common.is_user_login()) {
                        ActiveModels.SELECTED_SLOT = slotsModelAfternoon;
                        Intent intent = new Intent(getActivity(), ActivityPersonInfo.class);
                        Bundle b = new Bundle();
                        b.putString("timeslot", slotsModelAfternoon.getSlot());
                        b.putString("date", getArguments().getString("date"));
                        b.putString("doctor", ActiveModels.DOCTOR_MODEL.getDoct_name());
                        b.putString("ampm", "PM");
                        b.putString("clinic", getArguments().getString("clinic"));
                        b.putString("clinicLoc", getArguments().getString("clinicLoc"));
                        b.putInt("service_id", getArguments().getInt("serviceId"));

                        String date = getArguments().getString("date");
                        String time = slotsModelAfternoon.getSlot();
                        if(!timeHasPassed( date + " " + time)){
                            common.setToastMessage("Cannot book time");
                        } else {
                            intent.putExtras(b);
                            getActivity().startActivity(intent);
                        }

                    } else {
                        Intent intent = new Intent(getActivity(), ActivityLogin.class);
                        getActivity().startActivity(intent);
                    }
                } else {
                    common.setToastMessage(getActivity().getResources().getString(R.string.already_booked));
                }
            }
        }));


        //EVENING
        btnShowAllEvening.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!eveningShowAll){
                    eveningShowAll = true;
                    btnShowAllEvening.setText(getResources().getString(R.string.show_less));
                    eveningTimeSlotAdapter = new TimeSlotEveningAdapter(getActivity(), eveningTimeSlotArray);
                    rvEvening.setAdapter(eveningTimeSlotAdapter);
                } else{
                    eveningShowAll = false;
                    btnShowAllEvening.setText(getResources().getString(R.string.show_all));
                    eveningTimeSlotAdapter = new TimeSlotEveningAdapter(getActivity(), eveningTimeSlotArray, 6);
                    rvEvening.setAdapter(eveningTimeSlotAdapter);
                }
            }
        });

        rvEvening.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                slotsModelEvening = eveningTimeSlotArray.get(position);
                SlotsModel chooseMap = eveningTimeSlotArray.get(position);
                if (!chooseMap.getIs_booked()) {
                    if (common.is_user_login()) {
                        ActiveModels.SELECTED_SLOT = slotsModelEvening;
                        Intent intent = new Intent(getActivity(), ActivityPersonInfo.class);
                        Bundle b = new Bundle();
                        b.putString("timeslot", slotsModelEvening.getSlot());
                        b.putString("date", getArguments().getString("date"));
                        b.putString("doctor", ActiveModels.DOCTOR_MODEL.getDoct_name());
                        b.putString("ampm", "PM");
                        b.putString("clinic", getArguments().getString("clinic"));
                        b.putString("clinicLoc", getArguments().getString("clinicLoc"));
                        b.putInt("serviceId", getArguments().getInt("serviceId"));

                        String date = getArguments().getString("date");
                        String time = slotsModelEvening.getSlot();
                        if(!timeHasPassed( date + " " + time)){
                            common.setToastMessage("Cannot book time");
                        } else {
                            intent.putExtras(b);
                            getActivity().startActivity(intent);
                        }

                    } else {
                        Intent intent = new Intent(getActivity(), ActivityLogin.class);
                        getActivity().startActivity(intent);
                    }
                } else {
                    common.setToastMessage(getActivity().getResources().getString(R.string.already_booked));
                }
            }
        }));
    }

    // count total difference current datetime to given datetime
    public boolean timeHasPassed(String timeChosen) {
        String dateToday = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date d1 = null;
        Date d2 = null;

        try {
            d1 = format.parse(dateToday);
            d2 = format.parse(timeChosen);

            //in milliseconds
        } catch (Exception e) {
            e.printStackTrace();
        }

        Log.d("jbeeReviews", "d1 = " + d1.getTime());
        Log.d("jbeeReviews", "d2 = " + d2.getTime());

        long d_one = d1.getTime();
        long d_two = d2.getTime();

        if(d_one < d_two){
            return true;
        } else{
            return false;
        }

    }

}
