package com.gep.smartdoctor.fragments;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.fragment.app.Fragment;

import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.text.Editable;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.gep.smartdoctor.Config.ApiParams;
import com.gep.smartdoctor.Config.ConstValue;
import com.gep.smartdoctor.R;
import com.gep.smartdoctor.activities.ActivityMain;
import com.gep.smartdoctor.activities.ActivityVerifyPhone;
import com.gep.smartdoctor.util.CircleTransform;
import com.gep.smartdoctor.util.CommonClass;
import com.gep.smartdoctor.util.VJsonRequest;
import com.google.android.material.textfield.TextInputLayout;
import com.hbb20.CountryCodePicker;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.squareup.picasso.Picasso;
import com.yalantis.ucrop.UCrop;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.Manifest.permission_group.CAMERA;
import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static android.content.Context.INPUT_METHOD_SERVICE;
import static com.bumptech.glide.load.resource.bitmap.TransformationUtils.rotateImage;

public class FragmentProfile extends Fragment {

    private static final int PERMISSION_REQUEST_CODE = 111;
//    public final int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 1;
//    String CAMERA_PERMISSION = android.Manifest.permission.CAMERA; 
//    String READ_EXTERNAL_STORAGE_PERMISSION = android.Manifest.permission.READ_EXTERNAL_STORAGE; 
//    String WRITE_EXTERNAL_STORAGE_PERMISSION = android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

    View view;
    CommonClass common;
    Uri picUri;
    Bitmap myBitmap;

    private final static int ALL_PERMISSIONS_RESULT = 107;

    RelativeLayout btnDrawer, inputLayout, updateProfLayout;
    ImageView imgProfile;
    Button btnOffline;

    private Button btnUpdate;
    private EditText etFullname, etPhone, etEmail, etCurrentPassword, etNewPassword, etRePassword;
    private TextInputLayout tilFullname, tilPhone, tilEmail, tilCurrentPassword, tilNewPassword, tilRePassword;
    View emptyLayout;
    Button btnRetry;
    String currentPhone = "", currentName = "", currentEmail = ""; //from API variable initialization
    CountryCodePicker ccp;
    TextView tvEmailVerificationLabel;

    private boolean isViewShown = false;
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (getView() != null && isVisibleToUser) {
            isViewShown = true; // fetchdata() contains logic to show data when page is selected mostly asynctask to fill the data
            checkInput();
        } else {
            isViewShown = false;
        } }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState){
        view = inflater.inflate(R.layout.fragment_profile, container, false);

        common = new CommonClass(getActivity());
        updateProfLayout = (RelativeLayout) view.findViewById(R.id.updateprof_layout);
        imgProfile = (ImageView) view.findViewById(R.id.img_profile);
        inputLayout = (RelativeLayout) view.findViewById(R.id.inputlayout);
        btnDrawer = (RelativeLayout) view.findViewById(R.id.btn_drawer);
        btnOffline = (Button) view.findViewById(R.id.btn_update_gray);
        tvEmailVerificationLabel = (TextView) view.findViewById(R.id.error_email_label);

        btnDrawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ActivityMain) getActivity()).drawer.openDrawer(GravityCompat.START);
            }
        });



//        checkInput();
        return view;
    }


    private void spanStrings(){
        //email_error_label
        String labelString = getResources().getString(R.string.profile_email_label) + " " +
                getResources().getString(R.string.email_not_receive);
        tvEmailVerificationLabel.setText(Html.fromHtml(labelString));

        //Span "terms"
        Pattern word = Pattern.compile(getResources().getString(R.string.email_not_receive));
        Matcher match = word.matcher(tvEmailVerificationLabel.getText().toString());
        int startIndexterms = 0;
        int endIndexterms = 0;

        while (match.find()) {
            startIndexterms = match.start();
            endIndexterms = match.end();
        }

        SpannableString ss = new SpannableString(labelString);

        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void updateDrawState(TextPaint ds) {
                ds.setFakeBoldText(false);
                ds.setUnderlineText(true);    // this remove the underline
                ds.setColor(getResources().getColor(R.color.colorPrimary));
            }

            @Override
            public void onClick(View widget) {
                common.setToastMessage("Email verification");
//                if(CommonClass.is_internet_connected(getActivity())){
//                    Uri uri = Uri.parse(ConstValue.TERMS_CONDITIONS_URL); // missing 'http://' will cause crashed
//                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
//                    startActivity(intent);
//                }
            }
        };

        ss.setSpan(clickableSpan, startIndexterms, endIndexterms, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        tvEmailVerificationLabel.setText(ss);
        tvEmailVerificationLabel.setMovementMethod(LinkMovementMethod.getInstance());
    }

    public void checkInput(){
        ccp = (CountryCodePicker) view.findViewById(R.id.ccp);
        ccp.hideNameCode(true);
        btnUpdate = (Button) view.findViewById(R.id.btn_update);
        tilFullname = (TextInputLayout) view.findViewById(R.id.til_fullname);
        tilPhone = (TextInputLayout) view.findViewById(R.id.til_phone);
        tilEmail = (TextInputLayout) view.findViewById(R.id.til_email);
        tilCurrentPassword = (TextInputLayout) view.findViewById(R.id.til_currentpassword);
        tilNewPassword = (TextInputLayout) view.findViewById(R.id.til_newpassword);
        tilRePassword = (TextInputLayout) view.findViewById(R.id.til_repassword);

        etFullname = (EditText) view.findViewById(R.id.edit_fullname);
        etPhone = (EditText) view.findViewById(R.id.edit_phone);
        etEmail = (EditText) view.findViewById(R.id.edit_email);
        etCurrentPassword = (EditText) view.findViewById(R.id.edit_currentpassword);
        etNewPassword = (EditText) view.findViewById(R.id.edit_newpassword);
        etRePassword = (EditText) view.findViewById(R.id.edit_repassword);

        etFullname.setFilters(CommonClass.getFilterWithoutSpecialChars(50));
        etPhone.setFilters(CommonClass.getFilterWithoutSpecialChars(15));
        etCurrentPassword.setFilters(CommonClass.getFilterWithCharLimit(50));
        etNewPassword.setFilters(CommonClass.getFilterWithCharLimit(50));
        etRePassword.setFilters(CommonClass.getFilterWithCharLimit(50));
        spanStrings();

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(CommonClass.is_internet_connected(getActivity())){ ///temporary only
                    if (getActivity().getCurrentFocus() != null) {
                        InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
                        inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                    }

                    register();
                } else{
                    Toast.makeText(getActivity(), R.string.error_no_internet, Toast.LENGTH_SHORT).show();
                }
            }
        });

//        imgProfile.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (Build.VERSION.SDK_INT >= 23) {
//                    if (checkPermission()) {
//                        changeProfilePic();
//                    } else {
//                        requestPermission();
//                    }
//                }
//
//
//            }
//        });

        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", common.get_user_id());

        // this class for handle request response thread and return response data
        VJsonRequest vJsonRequest = new VJsonRequest(getActivity(), ApiParams.USERDATA_URL, params,
                new VJsonRequest.VJsonResponce() {
                    @Override
                    public void VResponce(String responce) {
                        JSONObject userdata = null;
                        try {
                            userdata = new JSONObject(responce);
                            etEmail.setText(userdata.getString("user_email"));
                            etFullname.setText(userdata.getString("user_fullname"));
                            etPhone.setText(userdata.getString("user_phone"));

                            if(userdata.getString("is_email_varified").equalsIgnoreCase("0")){
                                etEmail.setEnabled(true);
                                etEmail.setError("");
                                tvEmailVerificationLabel.setVisibility(View.VISIBLE);
                            } else{
                                etEmail.setError(null);
                                tvEmailVerificationLabel.setVisibility(View.GONE);
                            }

                            currentName = userdata.getString("user_fullname");
                            currentPhone = userdata.getString("user_phone");
                            currentEmail = userdata.getString("user_email");
//                            etFullname.requestFocus();
                            etFullname.setSelection(etFullname.getText().length());

                            ccp.setFullNumber(currentPhone);
                            try {
                                String[] numberPhone = currentPhone.split(String.valueOf(ccp.getSelectedCountryCodeWithPlus().charAt(ccp.getSelectedCountryCodeWithPlus().length() - 1)), 2); //get last char of index area code
                                etPhone.setText(numberPhone[1]);
                            } catch(Exception e){
                                e.printStackTrace();
                            }

                            btnUpdate.setBackground(getResources().getDrawable(R.drawable.rounded_btn_gray));
                            btnUpdate.setEnabled(false);



//                            String imageUrl = ConstValue.BASE_URL + "/index.php/api/get_userdata/" + userdata.getString("user_image");
//                            Picasso.with(getActivity()).load(imageUrl).into(imgProfile);
//
//                            Log.d("jbeeProfile", responce);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void VError(String responce) {
//                        common.setToastMessage(responce);
                    }
                });


        etFullname.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(tilFullname.isErrorEnabled()){
                    tilFullname.setErrorEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!currentName.equals(etFullname.getText().toString())){
                    btnUpdate.setBackground(getResources().getDrawable(R.drawable.rounded_button));
                    btnUpdate.setEnabled(true);
                } else{
                    btnUpdate.setBackground(getResources().getDrawable(R.drawable.rounded_btn_gray));
                    btnUpdate.setEnabled(false);
                }
            }
        });

        etPhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(tilPhone.isErrorEnabled()){
                    tilPhone.setErrorEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!currentPhone.equals(etPhone.getText().toString())){
                    btnUpdate.setBackground(getResources().getDrawable(R.drawable.rounded_button));
                    btnUpdate.setEnabled(true);
                } else{
                    btnUpdate.setBackground(getResources().getDrawable(R.drawable.rounded_btn_gray));
                    btnUpdate.setEnabled(false);
                }
            }
        });

        etEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(tilEmail.isErrorEnabled()){
                    tilEmail.setErrorEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!currentEmail.equals(etEmail.getText().toString())){
                    btnUpdate.setBackground(getResources().getDrawable(R.drawable.rounded_button));
                    btnUpdate.setEnabled(true);
                } else{
                    btnUpdate.setBackground(getResources().getDrawable(R.drawable.rounded_btn_gray));
                    btnUpdate.setEnabled(false);
                }
            }
        });

        etCurrentPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(tilCurrentPassword.isErrorEnabled()){
                    tilCurrentPassword.setErrorEnabled(false);
                }

                if(count == 0){
                    isChangePass = false;
                } else{
                    isChangePass = true;
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!currentpassword.equals(etCurrentPassword.getText().toString())){
                    btnUpdate.setBackground(getResources().getDrawable(R.drawable.rounded_button));
                    btnUpdate.setEnabled(true);
                } else{
                    btnUpdate.setBackground(getResources().getDrawable(R.drawable.rounded_btn_gray));
                    btnUpdate.setEnabled(false);
                }
            }
        });

        etNewPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(tilNewPassword.isErrorEnabled()){
                    tilNewPassword.setErrorEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!newpassword.equals(etNewPassword.getText().toString())){
                    btnUpdate.setBackground(getResources().getDrawable(R.drawable.rounded_button));
                }
            }
        });

        etRePassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(tilRePassword.isErrorEnabled()){
                    tilRePassword.setErrorEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!repassword.equals(etRePassword.getText().toString())){
                    btnUpdate.setBackground(getResources().getDrawable(R.drawable.rounded_button));
                }
            }
        });
    }

    ////////UPDATE////
    boolean isChangePass;
    String fullname = "";
    String phone = "";
    String email = "";
    String currentpassword = "";
    String newpassword = "";
    String repassword = "";
    // attempt update profile with validations
    public void register() {

        fullname = etFullname.getText().toString();
        phone = etPhone.getText().toString();
        if(ccp.getSelectedCountryCodeWithPlus() != null) {
            phone = ccp.getSelectedCountryCodeWithPlus() + etPhone.getText().toString();
        }
        email = etEmail.getText().toString();
        currentpassword = etCurrentPassword.getText().toString();
        newpassword = etNewPassword.getText().toString();
        repassword = etRePassword.getText().toString();
        boolean cancel = false;
        View focusView = null;

        if(!isChangePass){
            if(tilNewPassword.isErrorEnabled()){
                tilNewPassword.setErrorEnabled(false);
            }
            if(tilRePassword.isErrorEnabled()){
                tilRePassword.setErrorEnabled(false);
            }
        }


        if (TextUtils.isEmpty(fullname)) {
            etFullname.getBackground().setColorFilter(getResources().getColor(R.color.colorRed), PorterDuff.Mode.SRC_ATOP);
            tilFullname.setError(getString(R.string.valid_required_fullname));
            focusView = etFullname;
            cancel = true;
        }
        if (TextUtils.isEmpty(phone)) {
            etPhone.getBackground().setColorFilter(getResources().getColor(R.color.colorRed), PorterDuff.Mode.SRC_ATOP);
            tilPhone.setError(getString(R.string.valid_required_phone));
            focusView = etPhone;
            cancel = true;
        } else{
            // Check for a valid phone number
            if (!CommonClass.isValidCellPhone(phone)) {
                etPhone.getBackground().setColorFilter(getResources().getColor(R.color.colorRed), PorterDuff.Mode.SRC_ATOP);
                tilPhone.setError(getString(R.string.invalid_phone_number));
                focusView = etPhone;
                cancel = true;
            }
        }

        if (TextUtils.isEmpty(email)) {
            etEmail.getBackground().setColorFilter(getResources().getColor(R.color.colorRed), PorterDuff.Mode.SRC_ATOP);
            tilEmail.setError(getString(R.string.valid_required_email));
            focusView = etEmail;
            cancel = true;
        } else {
            // Check for a valid email address.
            if (!CommonClass.isValidEmail(email)) {
                etEmail.getBackground().setColorFilter(getResources().getColor(R.color.colorRed), PorterDuff.Mode.SRC_ATOP);
                tilEmail.setError(getString(R.string.valid_email));
                focusView = etEmail;
                cancel = true;
            }
        }


        if(!etCurrentPassword.getText().toString().equals("")){
            isChangePass = true;
        }

        //CHANGING PASSWORD IS NOT REQUIRED
        if(isChangePass){
            if (TextUtils.isEmpty(currentpassword)) {
//            common.setToastMessage(getString(R.string.valid_required_current_password));
                etCurrentPassword.getBackground().setColorFilter(getResources().getColor(R.color.colorRed), PorterDuff.Mode.SRC_ATOP);
                tilCurrentPassword.setError(getString(R.string.valid_required_current_password));
                focusView = etCurrentPassword;
                cancel = true;
            }

            if (TextUtils.isEmpty(newpassword)) {
                etNewPassword.getBackground().setColorFilter(getResources().getColor(R.color.colorRed), PorterDuff.Mode.SRC_ATOP);
                tilNewPassword.setError(getString(R.string.valid_required_new_password));
                focusView = etNewPassword;
                cancel = true;
            } else{
                // Check for a valid email address.
                if (!CommonClass.isValidPassword(newpassword)) {
                    etNewPassword.getBackground().setColorFilter(getResources().getColor(R.color.colorRed), PorterDuff.Mode.SRC_ATOP);
                    tilNewPassword.setError("Password should be 6-12 char alphanumeric, uppercase, lowercase, and special symbol from #?!@$%^&*-.");
                    focusView = etNewPassword;
                    cancel = true;
                }

                if(currentpassword.equals(newpassword)){
                    etNewPassword.getBackground().setColorFilter(getResources().getColor(R.color.colorRed), PorterDuff.Mode.SRC_ATOP);
                    tilNewPassword.setError("Please change your password");
                    focusView = etNewPassword;
                    cancel = true;
                }
            }

            if (!newpassword.equals(repassword)) {
                etRePassword.getBackground().setColorFilter(getResources().getColor(R.color.colorRed), PorterDuff.Mode.SRC_ATOP);
                tilRePassword.setError(getString(R.string.valid_not_match));
                focusView = etRePassword;
                cancel = true;
            }
        }


        if (cancel) {
            if (focusView != null)
                focusView.requestFocus();
        } else {

            if(currentPhone.equals(phone)){
                HashMap<String, String> params = new HashMap<>();
                params.put("user_fullname", fullname);
                params.put("user_phone", phone);
                params.put("user_id", common.get_user_id());

                common.setToastMessage("Please wait...");
                btnUpdate.setEnabled(false);
                btnUpdate.setBackground(getResources().getDrawable(R.drawable.rounded_btn_gray));

                // this class for handle request response thread and return response data
                VJsonRequest vJsonRequest = new VJsonRequest(getActivity(), ApiParams.UPDATEPROFILE_URL, params,
                        new VJsonRequest.VJsonResponce() {
                            @Override
                            public void VResponce(String responce) {

                                common.setSession(ApiParams.USER_FULLNAME, etFullname.getText().toString());
                                common.setSession(ApiParams.USER_PHONE, etPhone.getText().toString());
                                common.setSession(ApiParams.USER_EMAIL, etEmail.getText().toString());

                                common.setToastMessage(responce);
                            }

                            @Override
                            public void VError(String responce) {
                                common.setToastMessage(responce);
                                btnUpdate.setEnabled(true);
                            }
                        });


                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if(isChangePass){
                            changePassword();
                        } else{
                            common.setToastMessage(getActivity().getResources().getString(R.string.profile_update_success));
                            Intent intent = new Intent(getActivity(), ActivityMain.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            getActivity().startActivity(intent);
                            getActivity().finish();
                            getActivity().overridePendingTransition(0,0);
//                            ActivityMain.tabPosition = 1;
//                            ((ActivityMain) getActivity()).switchToHome();
                            //finish();

                            ActivityMain.tabPosition = 1;
                            ((ActivityMain) getActivity()).mViewPager.setCurrentItem(1);
                        }
                    }
                }, 3000);
            } else{
//                if (getActivity().getCurrentFocus() != null) {
//                    InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
//                    inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
//                }

                Intent intent = new Intent(getActivity(), ActivityVerifyPhone.class);
                intent.putExtra("user_fullname", fullname);
                intent.putExtra("user_phone", phone);
                intent.putExtra("c_password", currentpassword);
                intent.putExtra("n_password", newpassword);
                intent.putExtra("r_password", repassword);
                intent.putExtra("isChangePass", "true");
                intent.putExtra("registration", "false");
                startActivity(intent);
            }

        }

    }

    private void changePassword() {
        HashMap<String, String> params2 = new HashMap<>();
        params2.put("user_id", common.get_user_id());
        params2.put("c_password", currentpassword);
        params2.put("n_password", newpassword);
        params2.put("r_password", repassword);

        // this class for handle request response thread and return response data PASSWORD
        VJsonRequest vJsonRequestPassword = new VJsonRequest(getActivity(), ApiParams.CHANGE_PASSWORD_URL, params2,
                new VJsonRequest.VJsonResponce() {
                    @Override
                    public void VResponce(String responce) {
                        common.setToastMessage(responce);
                        ActivityMain.tabPosition = 1;
                        ((ActivityMain) getActivity()).mViewPager.setCurrentItem(1);

                        etCurrentPassword.setText("");
                        etNewPassword.setText("");
                        etRePassword.setText("");
                    }

                    @Override
                    public void VError(String responce) {
                        common.setToastMessage(responce);
                        btnUpdate.setEnabled(true);
                    }
                });
    }


    ///PROFILE PIC///
    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void requestPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            Toast.makeText(getActivity(), " Please allow this permission in App Settings.", Toast.LENGTH_LONG).show();
        } else {
            ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getActivity(), "Permission Granted Successfully! ", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getActivity(), "Permission Denied :( ", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }


    private void changeProfilePic(){
        final Dialog dialog = new Dialog(view.getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_upload);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView btnCancel = (TextView) dialog.findViewById(R.id.btn_cancel);
        TextView btnCamera = (TextView) dialog.findViewById(R.id.tv_camera);
        TextView btnGallery = (TextView) dialog.findViewById(R.id.tv_library);

        btnCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

                if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                    Intent camera_intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(camera_intent, 1);
                } else {
                    //changed here
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(new String[]{Manifest.permission.CAMERA}, PERMISSION_REQUEST_CODE);
                    }
                }


            }
        });

        btnGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent pickPhoto = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(pickPhoto , 2);
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        dialog.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode != RESULT_CANCELED) {
            switch (requestCode) {
                case 1:
                    if (resultCode == RESULT_OK && data != null) {
                        Bundle extras = data.getExtras();
                        Bitmap imageBitmap = (Bitmap) extras.get("data");
//                        imgProfile.setImageBitmap(imageBitmap);

                        loadBitmapByPicasso(getActivity(), imageBitmap, imgProfile);


                    }

                    break;
                case 2:
                    if (resultCode == RESULT_OK && data != null) {
                        Uri selectedImageUri = data.getData();
                        Bitmap bitmap = null;
                        try {
                            bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), selectedImageUri);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        loadBitmapByPicasso(getActivity(), bitmap, imgProfile);
//                        uploadToServer(bitmap);
                    }
                    break;
            }
        }
    }

    private void loadBitmapByPicasso(Context pContext, Bitmap pBitmap, ImageView pImageView) {
        try {
            Uri uri = Uri.fromFile(File.createTempFile("temp_file_name", ".jpg", pContext.getCacheDir()));
            OutputStream outputStream = pContext.getContentResolver().openOutputStream(uri);
            pBitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
            outputStream.close();
            Picasso.with(pContext).load(uri)
                    .placeholder(R.drawable.prof_placeholder)
                    .transform(new CircleTransform())
                    .into(pImageView);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    private void uploadToServer(Bitmap bitmap){
//        fullname = etFullname.getText().toString();
//        phone = etPhone.getText().toString();
//        if(ccp.getSelectedCountryCodeWithPlus() != null) {
//            phone = ccp.getSelectedCountryCodeWithPlus() + etPhone.getText().toString();
//        }
//
//        HashMap<String, String> params = new HashMap<>();
//        params.put("user_id", common.get_user_id());
//        params.put("user_fullname", etFullname.getText().toString());
//        params.put("user_phone", etPhone.getText().toString());
//        params.put("user_image", getStringImage(bitmap));
//
//        // this class for handle request response thread and return response data
//        VJsonRequest vJsonRequest = new VJsonRequest(getActivity(), ApiParams.UPDATEPROFILE_URL, params,
//                new VJsonRequest.VJsonResponce() {
//                    @Override
//                    public void VResponce(String responce) {
//
//                        common.setSession(ApiParams.USER_FULLNAME, etFullname.getText().toString());
//                        common.setSession(ApiParams.USER_PHONE, etPhone.getText().toString());
//                        common.setSession(ApiParams.USER_EMAIL, etEmail.getText().toString());
//
//                        common.setToastMessage(responce);
//                    }
//
//                    @Override
//                    public void VError(String responce) {
//                        common.setToastMessage(responce);
////                        btnUpdate.setEnabled(true);
//                    }
//                });
//    }

    private String getStringImage(Bitmap bitmap) {

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] b = baos.toByteArray();
        final String temp = Base64.encodeToString(b, Base64.DEFAULT);

        return temp;
    }

    @Override
    public void onResume(){
        super.onResume();

        if (!common.is_user_login()){
            updateProfLayout.setBackgroundColor(getResources().getColor(R.color.colorGrayPrimary));
            btnDrawer.setVisibility(View.GONE);
            inputLayout.setVisibility(View.GONE);
            btnOffline.setVisibility(View.VISIBLE);
        } else{
            updateProfLayout.setBackgroundColor(getResources().getColor(R.color.colorTextWhite));
            btnDrawer.setVisibility(View.VISIBLE);
            inputLayout.setVisibility(View.VISIBLE);
            btnOffline.setVisibility(View.GONE);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // save file url in bundle as it will be null on scren orientation
        // changes
        outState.putParcelable("pic_uri", picUri);
    }

    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);

        // get the file url
        picUri = savedInstanceState.getParcelable("pic_uri");
    }
}
