package com.gep.smartdoctor.fragments;

import android.app.Activity;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;

import com.gep.smartdoctor.R;
import com.gep.smartdoctor.activities.ActivityMain;
import com.gep.smartdoctor.adapters.ScheduleApptAdapter;
import com.gep.smartdoctor.dialogues.DialogAppointmentCalendar;
import com.gep.smartdoctor.models.AppointmentModel;
import com.gep.smartdoctor.models.ScheduleItemModel;
import com.gep.smartdoctor.util.CommonClass;
import com.gep.smartdoctor.util.DeactivatedViewPager;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.TimeZone;

public class FragmentAppointment extends Fragment {

    View view;
    int currentTab = 0;
    CommonClass common;
    RelativeLayout btnDrawer, btnFilterCalendar, btnRefreshCalendar;
    RadioGroup radioGroup;
    RadioButton btnSchedule, btnRecord;

    private AppointmentPagerAdapter appointPagerAdapter;
    private DeactivatedViewPager mViewPager;

    public static boolean dateIsChanged = false;
    public static ArrayList<String> schedID, schedTitle, schedResponse;
    public static List<Long> calendarEvents;

    public static Calendar mCalendar;
    public static int c_day, c_month, c_year;

    private boolean isViewShown = false;
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (getView() != null && isVisibleToUser) {
            isViewShown = true; // fetchdata() contains logic to show data when page is selected mostly asynctask to fill the data
            initView();
        } else {
            isViewShown = false;
        } }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState){
        view = inflater.inflate(R.layout.fragment_appointment, container, false);

        calendarEvents = new ArrayList<>();
        schedID = new ArrayList<String>();
        schedTitle = new ArrayList<String>();
        schedResponse = new ArrayList<String>();

        mCalendar = Calendar.getInstance(TimeZone.getDefault());
        c_day = mCalendar.get(Calendar.DAY_OF_MONTH);
        c_month = mCalendar.get(Calendar.MONTH);
        c_year = mCalendar.get(Calendar.YEAR);
        mCalendar.set(c_year, c_month, c_day);

        common = new CommonClass(getActivity());
        btnDrawer = (RelativeLayout) view.findViewById(R.id.btn_drawer);
        btnFilterCalendar = (RelativeLayout) view.findViewById(R.id.btn_filter);
        btnRefreshCalendar = (RelativeLayout) view.findViewById(R.id.btn_refresh);
        radioGroup = (RadioGroup) view.findViewById(R.id.radiogroup);
        btnSchedule = (RadioButton) view.findViewById(R.id.rb_schedule);
        btnRecord = (RadioButton) view.findViewById(R.id.rb_records);
//        if(!common.is_user_login()){
            btnFilterCalendar.setVisibility(View.GONE);
            btnRefreshCalendar.setVisibility(View.GONE);
//        }

//        initView();

        return view;
    }

    public void initView(){
        appointPagerAdapter = new AppointmentPagerAdapter(getChildFragmentManager());
        mViewPager = (DeactivatedViewPager) view.findViewById(R.id.pager);
        mViewPager.setOffscreenPageLimit(1);
        mViewPager.canScrollHorizontally(0);
        mViewPager.setAdapter(appointPagerAdapter);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//                Log.d("appointmentPage", String.valueOf(position));
////
////                if(position == 0){
////                    btnSchedule.performClick();
////                } else{
////                    btnRecord.performClick();
////                }
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        btnDrawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ActivityMain) getActivity()).drawer.openDrawer(Gravity.START);
            }
        });

        btnSchedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnRefreshCalendar.setVisibility(View.VISIBLE);
                btnFilterCalendar.setVisibility(View.VISIBLE);
                btnSchedule.setSelected(true);
                btnRecord.setSelected(false);
                btnSchedule.setTextColor(getResources().getColor(R.color.colorTextWhite));
                btnRecord.setTextColor(getResources().getColor(R.color.colorPrimary));
                currentTab = 0;
                mViewPager.setCurrentItem(0);
            }
        });

        btnRecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                refreshFilter(); ///TEMPORARY ONLY
                btnRefreshCalendar.setVisibility(View.GONE);
                btnFilterCalendar.setVisibility(View.GONE);
                btnSchedule.setSelected(false);
                btnRecord.setSelected(true);
                btnSchedule.setTextColor(getResources().getColor(R.color.colorPrimary));
                btnRecord.setTextColor(getResources().getColor(R.color.colorTextWhite));
                currentTab = 1;
                mViewPager.setCurrentItem(1);
            }
        });

        btnSchedule.callOnClick();

        btnRefreshCalendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refreshFilter();
            }
        });

        btnFilterCalendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogAppointmentCalendar dialogAppointmentCalendar = new DialogAppointmentCalendar(getActivity());
                dialogAppointmentCalendar.show();
            }
        });
    }

    public static void fetchAppointmentList(Activity activity, String date){
        FragmentAppointment.dateIsChanged = true;
        FragmentSchedule.scheduleApptAdapter = new ScheduleApptAdapter(activity, FragmentSchedule.scheduleItemModelArray /*, responseString, appointmentArray*/);
        FragmentSchedule.rvScheduleItem.setAdapter(FragmentSchedule.scheduleApptAdapter);
        FragmentSchedule.scheduleItemModelArray.clear();

        HashSet<String> hashSet = new HashSet<String>();
        hashSet.addAll(schedTitle);
        schedTitle.clear();
        schedTitle.addAll(hashSet);

        for(int i = 0 ; i < schedTitle.size() ; i++) {
            if(schedTitle.get(i).equals(date)){
                ScheduleItemModel scheduleItemModel = new ScheduleItemModel();
                scheduleItemModel.setScheduleDateID(schedID.get(i));
                scheduleItemModel.setScheduleItemTitle(schedTitle.get(i));
                scheduleItemModel.setResponseString(schedResponse.get(i));

                FragmentSchedule.scheduleItemModelArray.add(scheduleItemModel);
                FragmentSchedule.scheduleApptAdapter.notifyDataSetChanged();
            }
        }
    }

    public static void refreshFilter(){
        mCalendar = Calendar.getInstance(TimeZone.getDefault());
        c_day = mCalendar.get(Calendar.DAY_OF_MONTH);
        c_month = mCalendar.get(Calendar.MONTH);
        c_year = mCalendar.get(Calendar.YEAR);
        mCalendar.set(c_year, c_month, c_day);

        if(dateIsChanged){
            FragmentSchedule.scheduleItemModelArray.clear();

            Gson gson = new Gson();
            Type type = new TypeToken<List<AppointmentModel>>(){}.getType();
            List<AppointmentModel> appt = gson.fromJson(FragmentSchedule.responseString, type);

            ArrayList<String> apptString = new ArrayList<String>();
            for (AppointmentModel appointmentModel : appt){
                apptString.add(appointmentModel.getAppointment_date());
            }

            //filters duplicate item
            HashSet<String> hashSet = new HashSet<String>();
            hashSet.addAll(apptString);
            apptString.clear();
            apptString.addAll(hashSet);

            //adding item
            for(int i = 0 ; i < apptString.size() ; i++) {
                ScheduleItemModel scheduleItemModel = new ScheduleItemModel();
                scheduleItemModel.setScheduleDateID(String.valueOf(i));
                scheduleItemModel.setScheduleItemTitle(apptString.get(i));
                scheduleItemModel.setResponseString(FragmentSchedule.responseString);

                FragmentSchedule.scheduleItemModelArray.add(scheduleItemModel);
                FragmentSchedule.scheduleApptAdapter.notifyDataSetChanged();
            }

            //ARRANGE BY DATE
            Collections.sort(FragmentSchedule.scheduleItemModelArray, new Comparator<ScheduleItemModel>() {
                @Override
                public int compare(ScheduleItemModel o1, ScheduleItemModel o2) {
                    if (o1.getScheduleItemTitle() == null || o2.getScheduleItemTitle() == null)
                        return 0;
                    return o1.getScheduleItemTitle().compareTo(o2.getScheduleItemTitle());
                }
            });

            dateIsChanged = false;
        }
    }


    @Override
    public void onResume() {
        super.onResume();

        Log.d("testAppointment","testAppointment onResume");
        initView();
    }

    // bind fragment in viewpager
    public class AppointmentPagerAdapter extends FragmentStatePagerAdapter {
        public AppointmentPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            Fragment fragment = null;
            if (i == 0) {
                fragment = new FragmentSchedule(); //LIST OF CLINICS !!
            } else if (i == 1) {
                fragment = new FragmentRecordsHistory();
            }

            return fragment;
        }

        @Override
        public int getCount() {
            return 2;
        }
    }
}
