package com.gep.smartdoctor.fragments;

import android.app.Activity;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.gep.smartdoctor.R;
import com.gep.smartdoctor.Config.ApiParams;
import com.gep.smartdoctor.models.ActiveModels;
import com.gep.smartdoctor.models.BusinessModel;
import com.gep.smartdoctor.util.CommonClass;
import com.gep.smartdoctor.util.VJsonRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.gep.smartdoctor.adapters.ClinicListAdapter;

public class FragmentClinicList extends Fragment {

    private ArrayList<BusinessModel> postItems;

    private CommonClass common;

    public int current_page;
    public boolean loadingMore;
    public boolean stopLoadingData;
    public boolean is_first_time;
    public int number_of_item;

    private RecyclerView businessRecyclerView;
    private ClinicListAdapter adapter;
    private ProgressBar progressBar;
    private SwipeRefreshLayout swipeRefreshLayout;

    private Activity act;
    View rootView;
//    View emptyLayout;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_clinic_list, container, false);
        act = getActivity();
        common = new CommonClass(act);
        postItems = new ArrayList<BusinessModel>();

//        emptyLayout = (View)rootView.findViewById(R.id.emptyLayout);
//        emptyLayout.setVisibility(View.GONE);

        number_of_item = 10;
        current_page = 0;
        loadingMore = true;
        stopLoadingData = true;
        is_first_time = true;

        bindView(rootView);
        loadGetResult();
        initializeRefreshListener();

        return rootView;
    }

    // load xml element in view
    public void bindView(View rootView) {
        swipeRefreshLayout = rootView.findViewById(R.id.swiperefresh_layout);
        businessRecyclerView = rootView.findViewById(R.id.rv_list);
        final LinearLayoutManager layoutManager
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        businessRecyclerView.setLayoutManager(layoutManager);

        adapter = new ClinicListAdapter(getActivity(), postItems);
        businessRecyclerView.setHasFixedSize(true);
        businessRecyclerView.setAdapter(adapter);
        progressBar = rootView.findViewById(R.id.progressBar1);

        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimary));

        businessRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) //check for scroll down
                {
                    int pastVisiblesItems, visibleItemCount, totalItemCount;
                    visibleItemCount = layoutManager.getChildCount();
                    totalItemCount = layoutManager.getItemCount();
                    pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();

                    if (!(loadingMore)) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            if (stopLoadingData == false) {
                                // FETCH THE NEXT BATCH OF FEEDS
                                is_first_time = false;
                                loadGetResult();
                            }
                            //Do pagination.. i.e. fetch new data
                        }
                    }
                }
                super.onScrolled(recyclerView, dx, dy);
            }
        });
    }

    private void initializeRefreshListener() {
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // This method gets called when user pull for refresh,
                // You can make your API call here,
                // We are using adding a delay for the moment
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if(swipeRefreshLayout.isRefreshing()) {
                            loadGetResult();
                            swipeRefreshLayout.setRefreshing(false);
                        }
                    }
                }, 2000);
            }
        });
    }

    // load business data from business api
    public void loadGetResult() {
        current_page = 0;
        loadingMore = true;
        stopLoadingData = true;
        is_first_time = true;

        progressBar.setVisibility(View.VISIBLE);
        HashMap<String, String> params = new HashMap<>();
        if (getArguments() != null && getArguments().containsKey("search")) {
            params.put("search", getArguments().getString("search"));
        }

        //if (type !=null && !type.equalsIgnoreCase("0"))
        //    params.put("type","page_type_id");

        if (getArguments().containsKey("cat_id")) {
            params.put("cat_id", getArguments().getString("cat_id"));
        }
        if (getArguments().containsKey("locality_id")) {
            params.put("locality_id", getArguments().getString("locality_id"));

            if(getArguments().getString("locality_id").equals("")){
                params.put("locality_id", "empty");

                if(getArguments().getString("search").equals(""))
                params.put("search", "empty");
            }
        }
        int radius = 60;
        if (common.containKeyInSession("radius")) {
            radius = common.getSessionInt("radius");
            if (radius <= 0) {
                radius = 60;
            }
        }

        if (getArguments().containsKey("lat")) {
            params.put("lat", getArguments().getString("lat"));

        }
        if (getArguments().containsKey("lon")) {
            params.put("lon", getArguments().getString("lon"));
        }

        params.put("rad", String.valueOf(radius));
        params.put("offcet", String.valueOf(current_page));
        params.put("number_row", String.valueOf(number_of_item));

//        Log.i("jbeeSearch", "loadGetResult: " + params);

        // this class for handle request response thread and return response data
        VJsonRequest vJsonRequest = new VJsonRequest(getActivity(), ApiParams.BUSINESS_LIST, params,
                new VJsonRequest.VJsonResponce() {
                    @Override
                    public void VResponce(String responce) {

                        Gson gson = new Gson();
                        Type listType = new TypeToken<List<BusinessModel>>() {
                        }.getType();
                        ArrayList<BusinessModel> arraylist = gson.fromJson(responce, listType);

                        if (arraylist.size() > 0) {
                            if (is_first_time) {
                                postItems.clear();
                            }
                            if (arraylist.size() < number_of_item) {
                                stopLoadingData = true;
                                loadingMore = true;
                            } else {
                                stopLoadingData = false;
                                loadingMore = false;
                            }
                            postItems.addAll(arraylist);
                            ActiveModels.LIST_BUSINESS_MODEL = postItems;
                            progressBar.setVisibility(View.GONE);
                            if (postItems != null) {
                                current_page = current_page + number_of_item;
                                adapter.notifyDataSetChanged();

                            }

                        } else {
                            progressBar.setVisibility(View.GONE);
                        }
                    }

                    @Override
                    public void VError(String responce) {
                        progressBar.setVisibility(View.GONE);
                    }
                });
    }

    @Override
    public void onResume() {
        super.onResume();
        if (adapter != null) {
            adapter.updatelist();
            adapter.notifyDataSetChanged();
        }
    }

}
